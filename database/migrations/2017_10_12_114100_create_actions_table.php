<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->default(0);
            $table->integer('loader_id')->default(0);
            $table->string('action_name');
            $table->string('actions_status')->comment = "0=waiting; 1=working;
             2=waiting to accept; 3=break; 4=break; 5=broken; 6=call_manager; 7=manager_material; 8=manager_product; 9=workspace_broken";
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('actions');
    }
}
