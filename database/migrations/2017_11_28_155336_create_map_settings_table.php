<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMapSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('map_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('lang', 10, 7)->default(55.7191000);
            $table->decimal('long', 10, 7)->default(21.1254000);
            $table->decimal('bound_north', 10, 7)->default(55.7193700);
            $table->decimal('bound_south', 10, 7)->default(55.7188700);
            $table->decimal('bound_east', 10, 7)->default(21.1261000);
            $table->decimal('bound_west', 10, 7)->default(21.1248000);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('map_settings');
    }
}
