<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');

            $table->string('order_number');

            $table->integer('workspace_id')->unsigned();
            $table->foreign('workspace_id')->references('id')->on('workspaces')->onDelete('cascade');

            $table->integer('product_id');
            $table->integer('sort');
            $table->string('dimensions');
            $table->string('quantity_made')->default(0);
            $table->string('target_quantity');
            $table->string('heat_no');
            $table->integer('status')->default(0)->comment('0=Not active, 1=Active, 2=Paused, 3=Finished');
            $table->text('order_notes');
            $table->timestamps();
            $table->timestamp('deadline');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
