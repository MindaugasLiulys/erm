<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupervisorCallsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supervisor_calls', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('workspace_id');
            $table->integer('loader_id');
            $table->integer('status')->default(0)->comment('0=>not called, 1=>called');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supervisor_calls');
    }
}
