<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoaderToWarehouseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loader_to_warehouse', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('worker_id')->default(0);
            $table->integer('ramp_id')->default(0);

            $table->integer('loader_id')->unsigned();
            $table->foreign('loader_id')->references('id')->on('loaders')->onDelete('cascade');  
            
            $table->integer('material_id')->default(0);
            $table->integer('order_id')->default(0);

            $table->integer('workspace_id');

            $table->integer('warehouse_from')->default(0);
            $table->integer('warehouse_id')->default(0);
            $table->string('volume');
            $table->integer('status')->comment = "0=waiting; 1=doing; 2=done";
            $table->integer('type')->default(0)->comment = "0=loaded_product; 1=unloaded_material; 2=unloaded_product; 3=loaded_material; 4=warehouse_warehouse";
            $table->timestamp('start_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loader_to_warehouse');
    }
}
