<?php

use Illuminate\Database\Seeder;

class WorkspacesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        DB::table('workspaces')->insert([
            [
                'name'   => 'WSN1',
                'color'  => '#999999',
            ],
            [
                'name'   => 'WSN2',
                'color'  => '#999999',
            ],
            [
                'name'   => 'WSN3',
                'color'  => '#999999',
            ]
        ]);

        $workers = \App\Models\Worker::all();

        foreach ($workers as $worker) {
            for ($i = 0; $i <= 5; $i++) {
                \App\Models\Task::create([
                        'worker_id' => $worker->id,
                        'text' => $faker->text(200),
                        'status' => 1
                ]);
            }
        }

        foreach ($workers as $worker) {
            if($worker->id > 3 && $worker->id <= 6){
                DB::table('worker_to_workspace')->insert([
                    'worker_id' => $worker->id,
                    'workspace_id' => 1,
                ]);
            }
        }

        foreach ($workers as $worker) {
            if($worker->id > 6 && $worker->id <= 9){
                DB::table('worker_to_workspace')->insert([
                    'worker_id' => $worker->id,
                    'workspace_id' => 2,
                ]);
            }
        }

        foreach ($workers as $worker) {
            if($worker->id > 9){
                DB::table('worker_to_workspace')->insert([
                    'worker_id' => $worker->id,
                    'workspace_id' => 3,
                ]);
            }
        }
    }
}
