<?php

use Illuminate\Database\Seeder;
use App\Models\Loader_to_warehouse;
use Carbon\Carbon;

class LoaderToWarehouseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $workspaces = \App\Models\Workspace::all();
        foreach ($workspaces as $workspace) {
            for ($i = 0; $i < 20; $i++) {
                \App\Models\Order::create([
                    'order_number' => $faker->unique()->numberBetween(100, 2000),
                    'workspace_id' => $workspace->id,
                    'product_id' => \App\Models\Product::inRandomOrder()->first()->id,
                    'dimensions' => $faker->numberBetween(5, 25) . 'x' . $faker->numberBetween(10, 50),
                    'quantity_made' => 0,
                    'target_quantity' => $faker->numberBetween(50, 250),
                    'heat_no' => $faker->numberBetween(1, 5),
                    'deadline' =>  Carbon::now()->addDays(50)->format('Y-m-d H:i:s'),
                    'status' => 0,
                    'order_notes' => $faker->text(150),
                ]);
            }
        }

        $first = true;
        for ($i = 0; $i < 90; $i++) {
            $workspaces = \App\Models\Workspace::all();

            foreach ($workspaces as $workspace) {
                $worker = $workspace->workers->first();
                $warehouse = \App\Models\Warehouse::find(rand(1, 3));
                $order = $workspace->orders->where('status', 1)->first();
                $order = isset($order) ? $order :
                    $workspace->orders->where('status', 0)->first();

                if (($order->target_quantity - $order->quantity_made) <= 30) {
                    $loader_volume = $order->target_quantity - $order->quantity_made;
                    $order->update([
                        'status' => 3,
                        'quantity_made' => $order->target_quantity,
                    ]);
                }else {
                    $loader_volume = ($i / 3 == 0) ? rand(10, 20) : rand(15, 30);
                    $order->update([
                        'status' => 1,
                        'quantity_made' => $order->quantity_made + $loader_volume,
                    ]);
                }
                print_r('praejo 1 '. PHP_EOL);

                $production = new \App\Models\Workspace_production();
                $production->worker_id = $worker->id;
                $production->workspace_id = $worker->workspaces->first()->id;
                $production->order_id = $order->id;
                $production->day_start = $first ? 0 : $workspace->workspace_productions->last()->day_end;
                $production->day_end = $order->orderFinished ? 0 : 10;
                $production->check = ($i == 89) ? false : true;
                $production->unloaded = $loader_volume;
                $production->created_at = Carbon::now()->subDays(90 - $i)->format('Y-m-d H:i:s');
                $production->updated_at = $production->created_at;
                $production->save();

                print_r('praejo 2 '. PHP_EOL);

                $loaderTo = new Loader_to_warehouse();
                $loaderTo->worker_id = $worker->id;
                $loaderTo->loader_id = rand(1, 3);
                $loaderTo->workspace_id = $worker->workspaces->first()->id;
                $loaderTo->warehouse_id = $warehouse->id;
                $loaderTo->volume = $loader_volume;
                $loaderTo->order_id = $order->id;
                $loaderTo->status = 2;
                $loaderTo->start_at = Carbon::now()->subDays(90 - $i)->format('Y-m-d H:i:s');
                $loaderTo->created_at = Carbon::now()->subDays(90 - $i)->format('Y-m-d H:i:s');
                $loaderTo->updated_at = Carbon::now()->subDays(90 - $i)->format('Y-m-d H:i:s');
                $loaderTo->save();

                print_r('praejo 3 '. PHP_EOL);

                $productFromWarehouse = new Loader_to_warehouse();
                $productFromWarehouse->loader_id = rand(1, 3);
                $productFromWarehouse->order_id = $order->id;
                $productFromWarehouse->ramp_id = rand(1, 3);
                $productFromWarehouse->warehouse_id = $warehouse->id;
                $productFromWarehouse->volume = ($i / 3 == 0) ? rand(5, 10) : rand(5, 15);
                $productFromWarehouse->status = 2;
                $productFromWarehouse->type = 2;
                $productFromWarehouse->start_at = Carbon::now()->subDays(90 - $i)->format('Y-m-d H:i:s');
                $productFromWarehouse->created_at = Carbon::now()->subDays(90 - $i)->format('Y-m-d H:i:s');
                $productFromWarehouse->updated_at = Carbon::now()->subDays(90 - $i)->format('Y-m-d H:i:s');
                $productFromWarehouse->save();

                print_r('praejo 4 '. PHP_EOL);

                $warehouse->update(['used' => ($warehouse->used + $loaderTo->volume - $productFromWarehouse->volume)]);

                if ($workspace->id == 3){
                    $first = false;
                }

                $prodWarehouse = \App\Models\OrderWarehouse::firstOrCreate([
                    'warehouse_id' => $warehouse->id,
                    'order_id' => $order->id,
                ]);
                $prodWarehouse->quantity = $prodWarehouse->quantity + $loaderTo->volume - $productFromWarehouse->volume;
                $prodWarehouse->save();

                $material = \App\Models\Product\Material::find(rand(1, 3));

                $loaderToMaterial = new Loader_to_warehouse();
                $loaderToMaterial->worker_id = $worker->id;
                $loaderToMaterial->loader_id = rand(1, 3);
                $loaderToMaterial->material_id = $material->id;
                $loaderToMaterial->workspace_id = $worker->workspaces->first()->id;
                $loaderToMaterial->warehouse_id = $material->warehouse->id;
                $loaderToMaterial->volume = rand(15, 30);
                $loaderToMaterial->status = 2;
                $loaderToMaterial->type = 1;
                $loaderToMaterial->start_at = Carbon::now()->subDays(90 - $i)->format('Y-m-d H:i:s');
                $loaderToMaterial->created_at = Carbon::now()->subDays(90 - $i)->format('Y-m-d H:i:s');
                $loaderToMaterial->updated_at = Carbon::now()->subDays(90 - $i)->format('Y-m-d H:i:s');
                $loaderToMaterial->save();

                $materialToWarehouse = new Loader_to_warehouse();
                $materialToWarehouse->loader_id = rand(1, 3);
                $materialToWarehouse->ramp_id = rand(1, 3);
                $materialToWarehouse->material_id = $material->id;
                $materialToWarehouse->warehouse_id = $material->warehouse->id;
                $materialToWarehouse->volume = rand(5, 30);
                $materialToWarehouse->status = 2;
                $materialToWarehouse->type = 3;
                $materialToWarehouse->start_at = Carbon::now()->subDays(90 - $i)->format('Y-m-d H:i:s');
                $materialToWarehouse->created_at = Carbon::now()->subDays(90 - $i)->format('Y-m-d H:i:s');
                $materialToWarehouse->updated_at = Carbon::now()->subDays(90 - $i)->format('Y-m-d H:i:s');
                $materialToWarehouse->save();

                foreach (\App\Models\Product\Material::where('id', $loaderToMaterial->material_id)->cursor() as $material) {
                    $material->quantity = $material->quantity - $loaderToMaterial->volume + $materialToWarehouse->volume;
                    $material->save();
                }

            }

        }
    }
}
