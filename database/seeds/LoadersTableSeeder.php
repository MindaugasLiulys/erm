<?php

use Illuminate\Database\Seeder;

class LoadersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $i = 1;
        $names = ['FLN1', 'FLN2', 'FLN3'];

        DB::table('map_settings')->insert([
            'lang' => 55.7191000,
            'long' => 21.1254000,
            'bound_north' => 55.7194300,
            'bound_south' => 55.7187700,
            'bound_east' => 21.1265000,
            'bound_west' => 21.1243000,
        ]);

        foreach ($names as $name) {

            $loader = \App\Models\Loader::create([
                'name' => $name,
                'status' => 0,
                'color'  => '#999999',
            ]);

            DB::table('worker_to_loader')->insert([
                'loader_id' => $loader->id,
                'worker_id' => $i,
            ]);

            ++$i;
        }

    }
}
