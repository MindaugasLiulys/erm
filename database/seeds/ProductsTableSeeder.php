<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            [
                'name'          => 'Metal sheets',
                'created_at'    => Carbon::now()->format('Y-m-d H:i:s')
            ],[
                'name'          => 'Metal strips',
                'created_at'    => Carbon::now()->format('Y-m-d H:i:s')
            ],[
                'name'          => 'Metal pipes',
                'created_at'    => Carbon::now()->format('Y-m-d H:i:s')
            ],
        ]);

        DB::table('materials')->insert([
            [
                'name'          => 'Metal',
                'warehouse_id'  => '1',
                'quantity'      => '2500',
                'created_at'    => Carbon::now()->format('Y-m-d H:i:s')
            ],[
                'name'          => 'Aluminium',
                'warehouse_id'  => '2',
                'quantity'      => '2000',
                'created_at'    => Carbon::now()->format('Y-m-d H:i:s')
            ],[
                'name'          => 'Steel',
                'warehouse_id'  => '3',
                'quantity'      => '3000',
                'created_at'    => Carbon::now()->format('Y-m-d H:i:s')
            ],
        ]);

        $materials = \App\Models\Product\Material::all();

        $products = \App\Models\Product::all();

        foreach ($products as $product) {
            $product->materials()->sync($materials);
//            DB::table('product_workspace')->insert([
//                [
//                    'product_id'      => $product->id,
//                    'workspace_id'    => $product->id,
//                    'created_at'    => Carbon::now()->format('Y-m-d H:i:s'),
//                    'updated_at'    => Carbon::now()->format('Y-m-d H:i:s')
//                ]
//            ]);
        }
    }
}
