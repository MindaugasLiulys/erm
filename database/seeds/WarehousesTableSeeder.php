<?php

use Illuminate\Database\Seeder;

class WarehousesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('warehouses')->insert([
            [
                'name'   => 'WN1',
                'space'  => '2000',
                'used'   => '0',
                'reserved'   => '0',
                'color'  => '#999999',
            ],
            [
                'name'   => 'WN2',
                'space'  => '2500',
                'used'   => '0',
                'reserved'   => '0',
                'color'  => '#999999',
            ],
            [
                'name'   => 'WN3',
                'space'  => '2250',
                'used'   => '0',
                'reserved'   => '0',
                'color'  => '#999999',
            ]
        ]);
    }
}
