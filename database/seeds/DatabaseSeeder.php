<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(WarehousesTableSeeder::class);
        $this->call(PositionsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(LoadersTableSeeder::class);
        $this->call(WorkspacesTableSeeder::class);
        $this->call(ActionsTableSeeder::class);
        $this->call(ProductsTableSeeder::class);
        $this->call(LoaderToWarehouseTableSeeder::class);
        $this->call(RampTableSeeder::class);
        $this->call(BreakdownsTableSeeder::class);
        $this->call(SettingsTableSeeder::class);
    }
}
