<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $address = array('line1' => 'street 30-88', 'line2' => 'Klaipeda, Lietuva 91200');
        $emails = array('info@erm.dev', 'support@erm.dev');

        DB::table('settings')->insert([
            [
                'company_name'    => 'ERM',
                'company_address' => json_encode($address),
                'company_emails'   => json_encode($emails),
                'company_phone'   => '+370 8600 000',
                'company_website' => '',
                'company_fax'     => '',
                'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s')
            ]
        ]);
    }
}
