<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ActionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('actions')->insert([
            [
                'user_id'       => 2,
                'loader_id'     => 1,
                'action_name'   => 'Waiting',
                'actions_status'=> 0,
                'created_at'    => Carbon::now()->subMinute(2)->format('Y-m-d H:i:s')
            ],
            [
                'user_id'       => 2,
                'loader_id'     => 2,
                'action_name'   => 'Waiting',
                'actions_status'=> 0,
                'created_at'    => Carbon::now()->subMinute(1)->format('Y-m-d H:i:s')
            ],
            [
                'user_id'       => 2,
                'loader_id'     => 3,
                'action_name'   => 'Waiting',
                'actions_status'=> 0,
                'created_at'    => Carbon::now()->format('Y-m-d H:i:s')
            ]
        ]);
    }
}