<?php

use Illuminate\Database\Seeder;

class RampTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ramps')->insert([
            [
                'name'   => 'RN1',
                'status' => '1',
            ],
            [
                'name'   => 'RN2',
                'status' => '1',
            ],
            [
                'name'   => 'RN3',
                'status' => '1',
            ],
            [
                'name'   => 'RN4',
                'status' => '1',
            ]
        ]);
    }
}
