<?php

use App\Models\Position;
use App\Models\User;
use App\Models\Worker;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * 123456
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        DB::table('users')->insert([
            'username' => 'admin',
            'password' => bcrypt('123456'),
            'group' => 1,
        ]);

        $user = new User();
        $user->username = 'loader';
        $user->password = bcrypt('123456');
        $user->group = 0;
        $user->save();

        $postion = new Position();
        $postion->name = 'Supervisor';
        $postion->active = 1;
        $postion->save();

        $names = [
            '0' => ['Jonas', 'Jonauskas'],
            '1' => ['Tadas', 'Blinda'],
            '2' => ['Petras', 'Petrauskas'],
            '3' => ['Žilvinas', 'Misius'],
            '4' => ['Gediminas', 'Lingys'],
            '5' => ['Arvydas', 'Matulaitis'],
            '6' => ['Tomas', 'Anužis'],
            '7' => ['Jonas', 'Abrutis'],
            '8' => ['Giedrius', 'Zujus'],
            '9' => ['Lukas', 'Viršilas'],
            '10' => ['Antanas', 'Bružas'],
        ];

        Worker::insert(
            [
                'user_id' => $user->id,
                'name' => 'Tadas',
                'surname' => 'Urba',
                'position_id' => $postion->id,
                'password' => 123456,
            ]
        );

        for ($i = 0; $i <= 10; $i++) {
            $user = new User();
            $user->username = strtolower(substr(iconv('UTF-8', 'ISO-8859-1//TRANSLIT//IGNORE', $names[$i][1]), 0, 3) . 
                substr(iconv('UTF-8', 'ISO-8859-1//TRANSLIT//IGNORE', $names[$i][0]), 0, 3));
            $user->password = bcrypt('123456');
            $user->group = 0;
            $user->save();

            $postion = Position::inRandomOrder()->first();

            Worker::insert(
                [
                    'user_id' => $user->id,
                    'name' => $names[$i][0],
                    'surname' => $names[$i][1],
                    'position_id' => $postion->id,
                    'password' => 123456,
                ]
            );
        }

        $user = new User();
        $user->username = 'worker';
        $user->password = bcrypt('123456');
        $user->group = 0;
        $user->save();

        $postion = new Position();
        $postion->name = 'Worker';
        $postion->active = 1;
        $postion->save();

        Worker::insert(
            [
                'user_id' => $user->id,
                'name' => 'Vytenis',
                'surname' => 'Andriukaitis',
                'position_id' => $postion->id,
                'password' => 123456,
            ]
        );

    }
}
