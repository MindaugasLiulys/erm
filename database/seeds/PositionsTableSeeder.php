<?php

use Illuminate\Database\Seeder;

class PositionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('positions')->insert([
            [
                'name'   => 'Forklift driver',
                'active' => '1',
            ],
            [
                'name'   => 'Engineer',
                'active' => '1',
            ],
            [
                'name'   => 'Mechanic',
                'active' => '1',
            ],
            [
                'name'   => 'Worker',
                'active' => '1',
            ],
            [
                'name'   => 'Team leader',
                'active' => '1',
            ]
        ]);
    }
}
