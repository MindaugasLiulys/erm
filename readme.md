
## Step 1

### With GIT
Clone git repository

With Git SSH
```
git clone gindra@bitbucket.org:MindaugasLiulys/erm.git
```

Or with HTTPS
```
git clone https://github.com/FlorientR/laravel-gentelella.git
```

Go to the project folder 
```
cd erm
```

Update composer 
```
composer update
```

## Step 2
Copy ```.env.example``` file to ```.env```

For Unix
```
cp .env.example .env
```
For Windows
```
copy .env.example .env
```

Next, run this follow commands

```
php artisan key:generate
```

```
php artisan migrate --seed
```
!! YOU NEED TO INSTALL NODE.JS FOR USE NPM (SOCKET.IO) !! 

1. SSH into your Homestead VM or your machine
2. run node -v
```
You will see something like "v0.10.33"
```
3. run redis-cli ping
```
You should see "PONG"
```

## Install packages
```
npm install express ioredis socket.io --save
```

Your package.json file will look like
```

{
  "private": true,
  "devDependencies": {
    "gulp": "^3.8.8",
    "laravel-elixir": "*"
  },
  "dependencies": {
    "express": "^4.12.4",
    "ioredis": "^1.4.0",
    "redis": "^0.12.1",
    "socket.io": "^1.3.5"
  }
}
```
## Setting up Redis

As mentioned above Redis should already be installed but you just need to edit your .env file to tell Laravel to use the correct BROADCAST_DRIVER. Add the following line.

```
BROADCAST_DRIVER=redis
```
## Starting the Servers

1. Open two command line tabs and SSH into Homestead

cd into the root directory of your project
In the first tab, run node socket.js

You should see "Listening on Port 3000"
In the second tab run redis-server --port 3001

You should see a lot of output




