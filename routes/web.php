<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Authentication Routes...
$this->get('login', 'Auth\LoginController@showLoginForm')->name('login');
$this->post('login', 'Auth\LoginController@login');
$this->post('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('lang/{lang}', ['as'=>'lang.switch', 'uses'=>'LanguageController@switchLang']);

// Registration Routes...
//$this->get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
//$this->post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
//$this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
//$this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
//$this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
//$this->post('password/reset', 'Auth\ResetPasswordController@reset');

//Route::get('protected', ['middleware' => ['auth', 'admin'], function() {
//    return "this page requires that you be logged in and an Admin";
//}]);

Route::group(['prefix' => 'admin', 'middleware' => 'admin'], function () {
    Route::get('/', ['as' => 'admin_panel', 'uses' => 'Admin\AdminController@index']);

    //    Workers
    Route::get('/workers', ['as' => 'workers', 'uses' => 'Admin\WorkersController@index']);
    Route::get('/workers/create', ['as' => 'create-worker', 'uses' => 'Admin\WorkersController@create']);
    Route::post('/workers/create', ['as' => 'create-worker', 'uses' => 'Admin\WorkersController@store']);
    Route::get('/worker/edit/{id}', ['as' => 'edit-worker', 'uses' => 'Admin\WorkersController@edit']);
    Route::post('/worker/edit/{id}', ['as' => 'edit-worker-post', 'uses' => 'Admin\WorkersController@save']);
    Route::post('/worker/delete/{id}', ['as' => 'delete-worker-post', 'uses' => 'Admin\WorkersController@drop']);
    Route::get('/workers/print/{id}', ['as' => 'workers-print', 'uses' => 'Admin\WorkersController@generatePdf']);
    Route::get('/workers/print-all', ['as' => 'workers-print-all', 'uses' => 'Admin\WorkersController@generatePdfAll']);

//    PDF view
//        Route::get('/workers/{id}/pdfhtml',  ['as' => 'workers', 'uses' => 'Admin\WorkersController@pdfhtml']);
//        Route::get('/workers/print',  ['as' => 'workers-a', 'uses' => 'Admin\WorkersController@pdfhtml']);

    //    Positions
    Route::get('/positions', ['as' => 'positions', 'uses' => 'Admin\PositionsController@index']);
    Route::get('/positions/create', ['as' => 'create-position', 'uses' => 'Admin\PositionsController@create']);
    Route::post('/positions/create', ['as' => 'create-position', 'uses' => 'Admin\PositionsController@store']);
    Route::get('/positions/edit/{id}', ['as' => 'edit-position', 'uses' => 'Admin\PositionsController@edit']);
    Route::post('/positions/edit/{id}', ['as' => 'edit-position-post', 'uses' => 'Admin\PositionsController@save']);
    Route::post('/position/delete/{id}', ['as' => 'delete-position-post', 'uses' => 'Admin\PositionsController@drop']);

    //    Workspaces
    Route::get('/workspaces', ['as' => 'workspaces', 'uses' => 'Admin\WorkspacesController@index']);
    Route::get('/workspaces/create', ['as' => 'create-workspace', 'uses' => 'Admin\WorkspacesController@create']);
    Route::post('/workspaces/create', ['as' => 'create-workspace', 'uses' => 'Admin\WorkspacesController@store']);
    Route::get('/workspaces/edit/{id}', ['as' => 'edit-workspace', 'uses' => 'Admin\WorkspacesController@edit']);
    Route::post('/workspaces/edit/{id}', ['as' => 'edit-workspace-post', 'uses' => 'Admin\WorkspacesController@update']);
    Route::post('/workspaces/add/{id}/{worker}', ['as' => 'add-worker-post', 'uses' => 'Admin\WorkspacesController@addWorkerToWorkspace']);
    Route::post('/workspaces/edit/{id}', ['as' => 'edit-workspace-post', 'uses' => 'Admin\WorkspacesController@update']);
    Route::post('/workspaces/remove/{id}', ['as' => 'remove-worker-post', 'uses' => 'Admin\WorkspacesController@removeWorkerFromWorkspace']);
    Route::post('/workspaces/delete/{id}', ['as' => 'delete-workspace', 'uses' => 'Admin\WorkspacesController@drop']);
    Route::get('/workspaces/worker-stats/{id}', ['as' => 'worker-stat', 'uses' => 'Admin\WorkspacesController@workerStat']);
    Route::get('/workspaces/worker-stats/{id}/print-all/{from}/{to}', ['as' => 'worker-stats-print-all-date', 'uses' => 'Admin\WorkspacesController@generatePdfDate']);

    //  Loaders
    Route::get('/loaders', ['as' => 'loaders', 'uses' => 'Admin\LoadersController@index']);
    Route::get('/loaders/create', ['as' => 'create-loader', 'uses' => 'Admin\LoadersController@create']);
    Route::post('/loaders/create', ['as' => 'create-loader-post', 'uses' => 'Admin\LoadersController@store']);
    Route::post('/loaders/delete/{id}', ['as' => 'delete-loader', 'uses' => 'Admin\LoadersController@drop']);
    Route::get('/loaders/edit/{id}', ['as' => 'edit-loader', 'uses' => 'Admin\LoadersController@edit']);
    Route::post('/loaders/edit/{id}', ['as' => 'edit-loader-post', 'uses' => 'Admin\LoadersController@update']);
    Route::post('/loaders/add/{id}/{worker}',  ['as' => 'add-worker-to-loader-post', 'uses' => 'Admin\LoadersController@addWorkerToLoader']);
    Route::post('/loaders/remove/{id}',  ['as' => 'remove-worker-from-loader-post', 'uses' => 'Admin\LoadersController@removeWorkerFromLoader']);

    //  Warehouses
    Route::get('/warehouses', ['as' => 'warehouses', 'uses' => 'Admin\WarehousesController@index']);
    Route::get('/warehouses/create', ['as' => 'create-warehouse', 'uses' => 'Admin\WarehousesController@create']);
    Route::post('/warehouses/create', ['as' => 'create-warehouse-post', 'uses' => 'Admin\WarehousesController@store']);
    Route::post('/warehouses/delete/{id}', ['as' => 'delete-warehouse', 'uses' => 'Admin\WarehousesController@drop']);
    Route::get('/warehouses/edit/{id}', ['as' => 'edit-warehouse', 'uses' => 'Admin\WarehousesController@edit']);
    Route::post('/warehouses/edit/{id}', ['as' => 'edit-warehouse-post', 'uses' => 'Admin\WarehousesController@update']);
    Route::post('/warehouses/transfer', ['as' => 'warehouse-transfer', 'uses' => 'Admin\WarehousesController@transfer']);
        //Ramps
        Route::get('/warehouses/ramp/create', ['as' => 'create-ramp', 'uses' => 'Admin\WarehousesController@createRamp']);
        Route::post('/warehouses/ramp/create', ['as' => 'create-ramp-post', 'uses' => 'Admin\WarehousesController@storeRamp']);
        Route::post('/warehouses/ramp/delete/{id}', ['as' => 'delete-ramp', 'uses' => 'Admin\WarehousesController@dropRamp']);
        Route::get('/warehouses/ramp/edit/{id}', ['as' => 'edit-ramp', 'uses' => 'Admin\WarehousesController@editRamp']);
        Route::post('/warehouses/ramp/edit/{id}', ['as' => 'edit-ramp-post', 'uses' => 'Admin\WarehousesController@updateRamp']);

    //  Factory

    Route::group(['prefix' => 'factory'], function () {
        Route::get('/', ['as' => 'factory', 'uses' => 'Admin\FactoryController@index']);
        Route::post('call_order', ['as' => 'call_order', 'uses' => 'Admin\FactoryController@callOrder']);
        Route::post('call_material', ['as' => 'call_material', 'uses' => 'Admin\FactoryController@callMaterial']);
        Route::post('call_additional', ['as' => 'call_additional', 'uses' => 'Admin\FactoryController@callAdditional']);
        Route::post('get_available_warehouses', ['as' => 'get_available_warehouses', 'uses' => 'Admin\FactoryController@getAvailableWarehouses']);
        Route::get('call_manager_finish', ['as' => 'call_manager_finish', 'uses' => 'Admin\FactoryController@callManagerFinish']);
    });

    //Products
    Route::get('/products', ['as' => 'products', 'uses' => 'Admin\ProductsController@index']);
    Route::get('/products/create', ['as' => 'products-create', 'uses' => 'Admin\ProductsController@create']);
    Route::post('/products/create', ['as' => 'products-create-new', 'uses' => 'Admin\ProductsController@store']);
    Route::get('/products/edit/{id}', ['as' => 'products-edit', 'uses' => 'Admin\ProductsController@edit']);
    Route::post('/products/edit/{id}', ['as' => 'products-store', 'uses' => 'Admin\ProductsController@save']);
    Route::post('/products/delete/{id}', ['as' => 'delete-product', 'uses' => 'Admin\ProductsController@drop']);
    
    //Materials
    Route::post('/materials/delete/{id}', ['as' => 'delete-material', 'uses' => 'Admin\MaterialsController@destroy']);
    Route::resource('materials', 'Admin\MaterialsController', ['as' => 'admin']);

    //  Statistics
    Route::get('/statistics', ['as' => 'statistics', 'uses' => 'Admin\StatisticsController@index']);
    Route::get('/statistics/workspace/{id}', ['as' => 'workspace-statistics', 'uses' => 'Admin\StatisticsController@workspaceStats']);
    Route::get('/statistics/loader/{id}', ['as' => 'loader-statistics', 'uses' => 'Admin\StatisticsController@loaderStats']);
    Route::get('/statistics/warehouse/{id}', ['as' => 'warehouse-statistics', 'uses' => 'Admin\StatisticsController@warehouseStats']);
    Route::get('/statistics/worker/{id}', ['as' => 'worker-statistics', 'uses' => 'Admin\StatisticsController@workerStats']);

    //Map
    Route::resource('map', 'MapController');

    //  Breakdowns
    Route::get('/breakdowns', ['as' => 'breakdowns', 'uses' => 'Admin\BreakdownsController@index']);
    Route::get('/breakdowns/create', ['as' => 'breakdowns-create', 'uses' => 'Admin\BreakdownsController@create']);
    Route::post('/breakdowns/create', ['as' => 'breakdowns-create-new', 'uses' => 'Admin\BreakdownsController@store']);

    //  Tasks
    Route::get('/tasks', ['as' => 'tasks', 'uses' => 'Admin\TasksController@index']);
    Route::post('/tasks/create', ['as' => 'tasks-create', 'uses' => 'Admin\TasksController@createTask']);

    //  Orders
    Route::get('/orders', ['as' => 'orders', 'uses' => 'Admin\OrdersController@index']);
    Route::post('/orders', ['as' => 'orders-post', 'uses' => 'Admin\OrdersController@sortOrders']);
    Route::post('/orders/create', ['as' => 'order-create', 'uses' => 'Admin\OrdersController@createOrder']);
    Route::post('/orders/edit/{id}', ['as' => 'order-edit', 'uses' => 'Admin\OrdersController@editOrder']);
    Route::post('/orders/delete/{id}', ['as' => 'order-delete', 'uses' => 'Admin\OrdersController@dropOrder']);
    Route::get('/orders/order-pdf/{id}', ['as' => 'order-pdf', 'uses' => 'Admin\OrdersController@generatePdf']);

    //  Settings
    Route::get('/settings', ['as' => 'settings', 'uses' => 'Admin\SettingsController@index']);
    Route::post('/settings', ['as' => 'settings-update', 'uses' => 'Admin\SettingsController@update']);

});

Route::group(['prefix' => 'worker', 'as'=>'worker.', 'middleware' => 'worker'], function () {
    Route::get('/', ['as' => 'worker-home', 'uses' => 'Worker\WorkerHomeController@index']);
    Route::get('/call-the-truck/', ['as' => 'callTheTruck', 'uses' => 'Worker\WorkerHomeController@callTheTruck']);
    //  Workspaces
    Route::get('/workspaces', ['as' => 'worker-workspaces', 'uses' => 'Worker\WorkspacesController@index']);
    Route::post('/workspaces/{id}', ['as' => 'fill-loader', 'uses' => 'Worker\WorkspacesController@fillLoader']);
    Route::post('/workspaces/save/{id}', ['as' => 'save-quantity', 'uses' => 'Worker\WorkspacesController@saveQuantity']);
});


Route::get('/logout', 'Auth\LoginController@logout');
Route::get('/', 'HomeController@index');

Route::get('board','BoardController@index')->name('board');
Route::get('fire', function () {
    // this fires the event
    event(new App\Events\Board());
    return "event fired";
});
Route::get('info',function(){
    return phpinfo();
});
