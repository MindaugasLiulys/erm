<?php

Route::post('login', 'API\Auth\LoginController@login');
Route::post('refresh', 'API\Auth\LoginController@refresh');

Route::middleware('auth:api')->group(function(){
    Route::get('logout', 'API\Auth\LoginController@logout');
    Route::get('logout_worker', 'API\Auth\LoginController@logoutWorker');
    Route::get('get_user', 'API\UserController@get');
    Route::get('get_tasks', 'API\UserController@getTasks');
    Route::get('get_tasks_count', 'API\UserController@getTasksCount');
    Route::post('finish_task', 'API\UserController@finishTask');
    Route::get('call_manager', 'API\UserController@callManager');
    Route::get('call_manager_finish', 'API\UserController@callManagerFinish');
    Route::get('call_mechanic', 'API\UserController@callMechanic');

    Route::group(['prefix' => 'loader'], function () {
        Route::get('get_loader', 'API\LoaderController@getLoader');
        
        Route::get('get_all_jobs', 'API\LoaderController@getAllJobs');
        Route::get('get_jobs_done', 'API\LoaderController@getJobsDone');
        Route::get('get_jobs_done_week', 'API\LoaderController@getJobsDoneWeek');
        
        Route::post('start_job', 'API\LoaderController@startJob');
        Route::post('end_job', 'API\LoaderController@endJob');
        Route::get('get_latest_job', 'API\LoaderController@getLatestJob');
        Route::post('set_loader_status', 'API\LoaderController@setLoaderStatus');
        Route::post('start_job_later', 'API\LoaderController@startJobLater');
        
        Route::post('change_location', 'API\LoaderController@changeLocation');
        Route::post('loader_fixed', 'API\LoaderController@loaderFixed');
    });

    Route::group(['prefix' => 'worker'], function (){
        Route::get('get_latest_jobs', 'API\WorkerController@getLatestJobs');
        Route::get('get_worker_order', 'API\WorkerController@getWorkerOrder');
        Route::get('get_workspace', 'API\WorkerController@getWorkspace');
        Route::post('accept_balance', 'API\WorkerController@acceptBalance');
        Route::post('save_balance', 'API\WorkerController@saveBalance');
        Route::post('call_loader', 'API\OrderController@callLoader');
        Route::post('call_loader_material', 'API\WorkerController@callLoaderForMaterial');
        Route::post('get_loaders_warehouses', 'API\WorkerController@getLoadersWarehouses');
        Route::post('get_loaders_materials', 'API\WorkerController@getLoadersMaterials');

        Route::get('get_orders', 'API\OrderController@getOrders');
        Route::post('set_workspace_status', 'API\WorkerController@setWorkspaceStatus');
        
        Route::post('workspace_fixed', 'API\WorkerController@workspaceFixed');
    });

});
