<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $table = 'settings';

    protected $fillable = [
        'company_name',
        'company_address',
        'company_emails',
        'company_phone',
        'company_website',
        'company_fax',
    ];
}
