<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
	use Notifiable, HasRoles, HasApiTokens;
    use \HighIdeas\UsersOnline\Traits\UsersOnlineTrait;

    protected $table = 'users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function worker()
    {
        return $this->hasOne('App\Models\Worker','user_id');
    }

    public function workerData()
    {
        return $this->hasOne(Worker::class,'user_id')->select('id', 'user_id', 'name', 'surname', 'status');
    }

    public function isAdmin()
    {
        return $this->group == 1;
    }

    public function isWorker()
    {
        return $this->group == 0;
    } 
    
    public function findForPassport($identifier) {
        return $this->orWhere('username', $identifier)->first();
    }

    public function action()
    {
        return $this->hasOne('App\Models\Action', 'user_id')->orderBy('id', 'desc');
    }

}
