<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderWarehouse extends Model
{
    protected $table = 'order_warehouses';

    protected $fillable = [
        'order_id',
        'warehouse_id',
        'quantity',
    ];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class);
    }
}
