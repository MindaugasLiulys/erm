<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Breakdown extends Model
{

    protected $table = 'breakdowns';

    protected $fillable = [
        'type',
        'text',
        'time',
        'breakdown_id',
        'status',
    ];

    public function workspace()
    {
        return $this->belongsTo('App\Models\Workspace', 'breakdown_id');
    }

    public function loader()
    {
        return $this->belongsTo('App\Models\Loader', 'breakdown_id');
    }

    public function breakdownObject()
    {
        if ($this->type == 0) {
            return $this->workspace();
        }

        return $this->loader();
    }

    public function scopeToWorkspace($query)
    {
        return $query->where('type', 0);
    }

    public function scopeToLoader($query)
    {
        return $query->where('type', 1);
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function scopeFinished($query)
    {
        return $query->where('status', 0);
    }
}