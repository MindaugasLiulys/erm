<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Supervisor_call extends Model
{
    protected $fillable = [
        'loader_id',
        'workspace_id',
        'status'
    ];

    function workspace(){
        return $this->belongsTo('App\Models\Workspace', 'workspace_id');
    }

    function loader(){
        return $this->belongsTo('App\Models\Loader', 'loader_id');
    }

    public function object()
    {
        if ($this->type == 0) {
            return $this->workspace();
        }

        return $this->loader();
    }

    public function workspaces()
    {
        return $this->belongsToMany(Workspace::class);
    }

    function scopeCalled($query){
        return $query->where('status', 0);
    }

    function scopeFinished($query){
        return $query->where('status', 1);
    }

    function scopeToWorkspace($query){
        return $query->where('workspace_id', '!=', 0);
    }

    function scopeToLoader($query){
        return $query->where('loader_id', '!=', 0);
    }
}
