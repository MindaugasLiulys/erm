<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $table = 'tasks';

    protected $fillable = [
        'workspace_id',
        'worker_id',
        'loader_id',
        'text',
        'status'
    ];

    function worker()
    {
        return $this->belongsTo('App\Models\Worker', 'worker_id');
    }

    function workspace()
    {
        return $this->belongsTo('App\Models\Workspace', 'workspace_id');
    }

    function loader()
    {
        return $this->belongsTo('App\Models\Loader', 'loader_id');
    }

    function scopeToWorkspace($query)
    {
        return $query->where('workspace_id', '!=', 0);
    }

    function scopeToWorker($query)
    {
        return $query->where('worker_id', '!=', 0);
    }

    function scopeToLoader($query)
    {
        return $query->where('loader_id', '!=', 0);
    }

    function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    function scopeFinished($query)
    {
        return $query->where('status', 0);
    }

}
