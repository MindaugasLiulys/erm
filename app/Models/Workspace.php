<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Workspace extends Model
{

    protected $table = 'workspaces';

    protected $fillable = [
        'name',
        'color',
        'status',
    ];

    protected $appends = ['current_produced_product_name'];

    public function workers()
    {
        return $this->belongsToMany(Worker::class, 'worker_to_workspace', 'workspace_id', 'worker_id');
    }

    public function supervisor()
    {
        return $this->hasMany(Supervisor_call::class);
    }

    public function workspace_productions()
    {
        return $this->hasMany(Workspace_production::class);
    }
    
    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function loaderJobs()
    {
        return $this->hasMany(Loader_to_warehouse::class);
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function scopeBroken($query)
    {
        return $query->where('status', 2);
    }

    public function getCurrentProducedProductNameAttribute()
    {
        return $this->workspace_productions->last()->order->product->name;
    }

}
