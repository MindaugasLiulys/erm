<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MapSettings extends Model
{
    protected $table = 'map_settings';
    
    protected $fillable = [
        'lang',  
        'long',  
        'bound_north',  
        'bound_south',  
        'bound_east',  
        'bound_west',  
    ];
}
