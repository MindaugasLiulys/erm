<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Worker extends Model
{

    protected $table = 'workers';

    protected $fillable = [
        'status'
    ];
    
    public function scopeActive($query)
    {
        return $query->where('status', true);
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function position()
    {
        return $this->belongsTo('App\Models\Position', 'position_id');
    }

    public function workspaces(){
        return $this->belongsToMany(Workspace::class, 'worker_to_workspace', 'worker_id', 'workspace_id');
    }

    public function loaderTo(){
        return $this->hasMany('App\Models\Worker_to_loader');
    }

    public function loader(){
        return $this->belongsToMany(Loader::class, 'worker_to_loader', 'worker_id', 'loader_id');
    }
}
