<?php

namespace App\Models;

use App\Models\Product\Material;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Loader_to_warehouse extends Model
{
    protected $table = 'loader_to_warehouse';

    protected $fillable = [
        'worker_id',
        'loader_id',
        'workspace_id',
        'ramp_id',
        'warehouse_id',  
        'warehouse_from',
        'material_id',
        'order_id',
        'volume',
        'status',  
        'type',  
        'start_at',
    ];

    protected $appends = ['workspace_name', 'warehouse_name',  'warehouse_from_name', 'material_name', 'product_name', 'ramp_name', 'order_number'];
    
    public function loader()
    {
        return $this->belongsTo(Loader::class);
    }

    public function worker()
    {
        return $this->belongsTo(Worker::class);
    }
    
    public function workspace()
    {
        return $this->belongsTo(Workspace::class);
    }
    
    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class);
    }

    public function warehouseFrom()
    {
        return $this->belongsTo(Warehouse::class, 'warehouse_from');
    }
    
    public function material()
    {
        return $this->belongsTo(Material::class);
    }
    
    public function ramp()
    {
        return $this->belongsTo(Ramp::class);
    }
    
    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function scopeWorking($query)
    {
        return $query->where('status', 1);
    }
    
    public function scopeActive($query)
    {
        return $query->where('status', '<>', 2);
    }   
    
    public function scopeWaiting($query)
    {
        return $query->where('status', false);
    }   
    
    public function scopeFinished($query)
    {
        return $query->where('status', 2)->warehouseToWarehouseNot();
    }
    
    public function scopeBringMaterial($query)
    {
        return $query->where('type', 1);
    }
    
    public function scopeLoadMaterial($query)
    {
        return $query->where('type', 3);
    }
    
    public function scopeLoadWarehouse($query)
    {
        return $query->where('type', 0);
    }
    
    public function scopeUnloadWarehouse($query)
    {
        return $query->where('type', 2);
    }
    
    public function scopeWarehouseToWarehouse($query)
    {
        return $query->where('type', 4);
    }

    public function scopeWarehouseToWarehouseNot($query)
    {
        return $query->where('type', '!=', 4);
    }

    public function scopeWorkspaceJobs($query)
    {
        return $query->where('type', '<', 3);
    }

    public function scopeLater($query)
    {
        return $query->where('start_at', '>', Carbon::now());
    }

    public function getStartAtTime()
    {
        return date('H\h i\m\i\n', strtotime($this->start_at));
    }

    public function scopeJobsDoneToday($query)
    {
        return $query->finished()->whereDate('updated_at', Carbon::today()->toDateString());
    }

    public function scopeJobsInAWeek($query)
    {
        return $query->finished()->whereDate('created_at', '>=', Carbon::now()->subDay(7)->toDateString());
    }
    
    public function scopeLatestJobs($query)
    {
        return $query->whereDate('created_at', '>=', Carbon::now()->subDay(7)->toDateString());
    }

    public function getWorkspaceNameAttribute()
    {
        if (!$this->workspace) {
            return "";
        }
        return $this->workspace->name;
    }

    public function getWarehouseNameAttribute()
    {
        if (!$this->warehouse) {
            return "";
        }
        return $this->warehouse->name;
    }

    public function getWarehouseFromNameAttribute()
    {
        if (!$this->warehouseFrom) {
            return "";
        }
        return $this->warehouseFrom->name;
    }

    public function getRampNameAttribute()
    {
        if (!$this->ramp) {
            return "";
        }
        return $this->ramp->name;
    }

    public function getMaterialNameAttribute()
    {
        if (!$this->material) {
            return "";
        }
        return $this->material->name;
    }

    public function getProductNameAttribute()
    {
        if (!$this->order) {
            return "";
        }
        return $this->order->product->name;
    }

    public function getOrderNumberAttribute()
    {
        if (!$this->order) {
            return "";
        }
        return $this->order->order_number;
    }
}
