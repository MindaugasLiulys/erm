<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Worker_to_workspace extends Model
{
    protected $table = 'worker_to_workspace';

    public function workspace(){
        return $this->belongsTo('App\Models\Workspace', 'workspace_id');
    }

    public function worker(){
        return $this->belongsTo('App\Models\Worker', 'worker_id');
    }
}
