<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Action extends Model
{
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function loader()
    {
        return $this->belongsTo('App\Models\Loader', 'loader_id');
    }
    
    public function scopeLoaders($query){
        return $query->where('loader_id', '!=', 0);
    }
}
