<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Loader extends Model
{

    protected $table = 'loaders';

    protected $fillable = [
          'name',
          'status',
          'color',
          'lang',
          'long',
    ];

    public function action()
    {
        return $this->hasOne(Action::class, 'loader_id')->orderBy('id', 'desc');
    }

    public function workers(){
        return $this->belongsToMany(Worker::class, 'worker_to_loader', 'loader_id', 'worker_id');
    }
    
    public function job()
    {
        return $this->hasMany(Loader_to_warehouse::class, 'loader_id')->waiting();
    }

    public function supervisor()
    {
        return $this->hasMany(Supervisor_call::class);
    }
    
    public function jobLater()
    {
        return $this->hasMany(Loader_to_warehouse::class, 'loader_id')->later();
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function scopeBroken($query)
    {
        return $query->where('status', 3);
    }

    public function scopeBusy($query)
    {
        return $query->where('status', 2);
    }
}
