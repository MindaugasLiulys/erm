<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';

    protected $fillable = [
        'order_number',
        'workspace_id',
        'product_id',
        'dimensions',
        'quantity_made',
        'target_quantity',
        'heat_no',
        'deadline',
        'status',
        'order_notes',
        'sort'
    ];

    protected $dates = ['created_at'];

    function workspace()
    {
        return $this->belongsTo(Workspace::class);
    }

    function workspace_production()
    {
        return $this->hasMany(Workspace_production::class);
    }

    function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    function scopeNotActive($query)
    {
        return $query->where('status', 0)->orderBy('created_at')->orderBy('sort', 'desc');
    }

    function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    function scopePaused($query)
    {
        return $query->where('status', 2);
    }

    function scopeFinished($query)
    {
        return $query->where('status', 3);
    }
    
    function scopeNotFinished($query)
    {
        return $query->where('status', '<', 3);
    }
    
    function getOrderFinishedAttribute()
    {
        if ($this->quantity_made == $this->target_quantity) {
            return true;
        }
        
        return false;
    }

}
