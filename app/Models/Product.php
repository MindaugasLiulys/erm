<?php

namespace App\Models;

use App\Models\Product\Material;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
        
    public function materials()
    {
        return$this->belongsToMany(Material::class, 'material_product', 'product_id', 'material_id');
    }
}
