<?php

namespace App\Models\Product;

use App\Models\Loader_to_warehouse;
use App\Models\Product;
use App\Models\Warehouse;
use Illuminate\Database\Eloquent\Model;

class Material extends Model
{
    protected $table = 'materials';
    
    protected $fillable = [
        'name',
        'warehouse_id',
        'quantity'
    ];
    
    public function product()
    {
        return $this->belongsToMany(Product::class);
    }
    
    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class);
    }
    
    public function loaderJobs()
    {
        return $this->hasMany(Loader_to_warehouse::class);
    }
}
