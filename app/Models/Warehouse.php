<?php

namespace App\Models;

use App\Models\Product\Material;
use Illuminate\Database\Eloquent\Model;

class Warehouse extends Model
{
    protected $table = 'warehouses';

    protected $fillable = [
        'name',
        'space',
        'reserved',
        'used',
        'color',
    ];

    public function materials()
    {
        return $this->hasMany(Material::class);
    }

    public function orders()
    {
        return $this->hasMany(OrderWarehouse::class);
    }
}
