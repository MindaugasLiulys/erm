<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Workspace_production extends Model
{
    protected $table = 'workspace_production';

    protected $fillable = [
        'worker_id',
        'workspace_id',
        'order_id',
        'day_start',
        'day_end',
        'unloaded',
        'check',
    ];

    protected $casts = [
        'check' => 'boolean',
    ];

    protected $appends = ['workspace_name', 'workspace_status'];

    public function worker(){
        return $this->belongsTo('App\Models\Worker','worker_id');
    }

    public function workspace(){
        return $this->belongsTo(Workspace::class,'workspace_id');
    }

    public function order(){
        return $this->belongsTo(Order::class);
    }

    public function getWorkspaceNameAttribute()
    {
        return $this->workspace()->first()->name;
    }

    public function getWorkspaceStatusAttribute()
    {
        return $this->workspace()->first()->status;
    }

}
