<?php

function getPercent($total, $used)
{
    $percent = $used * 100 / $total;
    $percent = number_format((float)$percent, 1, '.', '');

    return $percent;
}

function percentDifference($firstNum, $secondNum)
{
    if ($firstNum != 0 && $secondNum != 0) {
        if ($firstNum < $secondNum) {
            $percent = '-' . $secondNum * 100 / $firstNum + 100;
        } else {
            $percent = $firstNum * 100 / $secondNum - 100;
        }

        $percent = number_format((float)$percent, 0, '.', '');
    } else {
        $percent = trans('site.Not enough data');
    }

    return $percent;
}

function randomColor()
{
    $n = rand(0, 2);

    $colorElement1 = rand(0, 255);
    $colorElement2 = rand(0, 255);
    $colorElement3 = rand(0, 255);

    switch ($n) {
        case 0:
            $colorElement1 = 255;
            break;
        case 1:
            $colorElement2 = 255;
            break;
        case 2:
            $colorElement3 = 255;
            break;
    }

//    $color = $colorElement1 . ',' . $colorElement2 . ',' . $colorElement3 . ',0.4';
//    $color = 'rgba(' . $color . ')';

    $color = sprintf("#%02x%02x%02x", $colorElement1, $colorElement2, $colorElement3);
    return $color;

}

function getAction($action_name)
{
    $actions = [
        'waiting' => [
            'message' => trans('site.Waiting'),
            'status' => 0,
            'span' => 'green',
            'i' => '',
            'class' => 'btn btn-success btn-xs',
        ],
        'accept' => [
            'message' => trans('site.Waiting to accept'),
            'status' => 2,
            'span' => 'orange',
            'i' => 'fa fa-cog fa-spin fa-fw',
            'class' => 'btn btn-warning btn-xs',
        ],
        'working' => [
            'message' => trans('site.working'),
            'status' => 1,
            'span' => 'orange',
            'i' => '',
            'class' => 'btn btn-danger btn-xs',
        ],
        'break' => [
            'message' => trans('site.Break'),
            'status' => 3,
            'span' => 'green',
            'i' => 'fa fa-coffee',
            'class' => 'btn btn-success btn-xs',
        ],
        'busy' => [
            'message' => trans('site.loader_busy'),
            'status' => 4,
            'span' => 'orange',
            'i' => 'fa fa-cog fa-spin fa-fw',
            'class' => 'btn btn-danger btn-xs',
        ],
        'broken' => [
            'message' => trans('site.loader_broken'),
            'status' => 5,
            'span' => 'red',
            'i' => 'fa fa-chain-broken',
            'class' => 'btn btn-danger btn-xs',
        ],
        'call_manager' => [
            'message' => trans('site.Supervisor called'),
            'status' => 6,
            'span' => 'red',
            'i' => 'fa fa-chain-broken',
            'class' => 'btn btn-danger btn-xs',
        ],
        'warehouse_warehouse' => [
            'message' => trans('site.warehouse_warehouse'),
            'status' => 7,
            'span' => 'orange',
            'i' => 'fa fa-cog fa-spin fa-fw',
            'class' => 'btn btn-warning btn-xs',
        ],
        'manager_product' => [
            'message' => trans('site.load_product'),
            'status' => 8,
            'span' => 'orange',
            'i' => 'fa fa-cog fa-spin fa-fw',
            'class' => 'btn btn-warning btn-xs',
        ],
        'workspace_broken' => [
            'message' => trans('site.workspace_broken'),
            'status' => 9,
            'span' => 'red',
            'i' => 'fa fa-chain-broken',
            'class' => 'btn btn-danger btn-xs',
        ],
    ];

    return $actions[$action_name];
}

function getActionByStatus($action_status)
{
    $actions = [
        '0' => [
            'message' => trans('site.Waiting'),
            'status' => 0,
            'span' => 'green',
            'i' => '',
            'class' => 'btn btn-success btn-xs',
        ],
        '2' => [
            'message' => trans('site.Waiting to accept'),
            'status' => 2,
            'span' => 'orange',
            'i' => 'fa fa-cog fa-spin fa-fw',
            'class' => 'btn btn-warning btn-xs',
        ],
        '1' => [
            'message' => trans('site.working'),
            'status' => 1,
            'span' => 'orange',
            'i' => '',
            'class' => 'btn btn-danger btn-xs',
        ],
        '3' => [
            'message' => trans('site.Break'),
            'status' => 3,
            'span' => 'green',
            'i' => 'fa fa-coffee',
            'class' => 'btn btn-success btn-xs',
        ],
        '4' => [
            'message' => trans('site.loader_busy'),
            'status' => 4,
            'span' => 'orange',
            'i' => 'fa fa-cog fa-spin fa-fw',
            'class' => 'btn btn-danger btn-xs',
        ],
        '5' => [
            'message' => trans('site.loader_broken'),
            'status' => 5,
            'span' => 'red',
            'i' => 'fa fa-chain-broken',
            'class' => 'btn btn-danger btn-xs',
        ],
        '6' => [
            'message' => trans('site.Supervisor called'),
            'status' => 6,
            'span' => 'red',
            'i' => 'fa fa-chain-broken',
            'class' => 'btn btn-danger btn-xs',
        ],
        '7' => [
            'message' => trans('site.warehouse_warehouse'),
            'status' => 7,
            'span' => 'orange',
            'i' => 'fa fa-cog fa-spin fa-fw',
            'class' => 'btn btn-warning btn-xs',
        ],
        '8' => [
            'message' => trans('site.load_product'),
            'status' => 8,
            'span' => 'orange',
            'i' => 'fa fa-cog fa-spin fa-fw',
            'class' => 'btn btn-warning btn-xs',
        ],
        '9' => [
            'message' => trans('site.workspace_broken'),
            'status' => 9,
            'span' => 'red',
            'i' => 'fa fa-chain-broken',
            'class' => 'btn btn-danger btn-xs',
        ],
    ];

    return $actions[$action_status];
}

function productionMonth($month)
{
    $monthStart = \Carbon\Carbon::now()->startOfMonth()->subMonth($month);
    $monthEnd = \Carbon\Carbon::now()->endOfMonth()->subMonth($month);

    $MonthsProduction = \App\Models\Workspace_production::whereRaw("date(updated_at) >= '" . $monthStart . "' and date(updated_at) <= '" . $monthEnd . "'")->get();

    $monthsProd = 0;
    foreach ($MonthsProduction as $production) {
        $monthsProd += $production->unloaded - $production->day_start + $production->day_end;
    }

    return $monthsProd;
}

function productionWeek($week, $id, $type)
{

    $weekStart = \Carbon\Carbon::now()->subWeek($week)->startOfWeek();
    $weekEnd = \Carbon\Carbon::now()->subWeek($week)->endOfWeek();
    $weeksProd = 0;

    switch ($type) {
        //  Workspace statistics
        case 0:
            $weeksProduction = \App\Models\Workspace_production::whereRaw("date(updated_at) >= '" . $weekStart . "' and date(updated_at) <= '" . $weekEnd . "' and workspace_id='" . $id . "'")->get();
            foreach ($weeksProduction as $production) {
                $weeksProd += $production->unloaded - $production->day_start + $production->day_end;
            }
            break;
        // Loader statistics
        case 1:
            $weeksProduction = \App\Models\Loader_to_warehouse::whereBetween("updated_at", [$weekStart, $weekEnd])
                ->where('loader_id', $id)
                ->finished()->bringMaterial()->get();
            foreach ($weeksProduction as $production) {
                $weeksProd += $production->volume;
            }
            break;
        //  Worker statistics
        case 2:
            $weeksProduction = \App\Models\Workspace_production::whereRaw("date(updated_at) >= '" . $weekStart . "' and date(updated_at) <= '" . $weekEnd . "' and worker_id='" . $id . "'")->get();
            foreach ($weeksProduction as $production) {
                $weeksProd += $production->unloaded - $production->day_start + $production->day_end;
            }
            break;
        //  Production From workspace To Warehouse statistics
        case 3:
            $weeksProduction = \App\Models\Loader_to_warehouse::whereBetween("updated_at", [$weekStart, $weekEnd])
                ->where('warehouse_id', $id)
                ->finished()->loadWarehouse()->get();
            foreach ($weeksProduction as $production) {
                $weeksProd += $production->volume;
            }
            break;
        //  Material From Warehouse To Workspace statistics
        case 4:
            $weeksProduction = \App\Models\Loader_to_warehouse::whereBetween("updated_at", [$weekStart, $weekEnd])
                ->whereIn('material_id', \App\Models\Warehouse::find($id)->materials->pluck('id')->toArray())
                ->finished()->get();

            foreach ($weeksProduction as $production) {
                $weeksProd += $production->volume;
            }
            break;
        //  Productions From Warehouse To Ramp statistics
        case 5:
            $weeksProduction = \App\Models\Loader_to_warehouse::whereBetween("updated_at", [$weekStart, $weekEnd])
                ->where('warehouse_id', $id)
                ->finished()->unloadWarehouse()->get();
            foreach ($weeksProduction as $production) {
                $weeksProd += $production->volume;
            }
            break;
        //  Materials From Ramp To Warehouse statistics
        case 6:
            $weeksProduction = \App\Models\Loader_to_warehouse::whereBetween("updated_at", [$weekStart, $weekEnd])
                ->whereIn('material_id', \App\Models\Warehouse::find($id)->materials->pluck('id')->toArray())
                ->finished()->loadMaterial()->get();
            foreach ($weeksProduction as $production) {
                $weeksProd += $production->volume;
            }
            break;
        // Loader Production From Warehouse statistics
        case 7:
            $weeksProduction = \App\Models\Loader_to_warehouse::whereBetween("updated_at", [$weekStart, $weekEnd])
                ->where('loader_id', $id)
                ->finished()->unloadWarehouse()->get();
            foreach ($weeksProduction as $production) {
                $weeksProd += $production->volume;
            }
            break;
        // Loader Materials From Warehouse To Workspace statistics
        case 8:
            $weeksProduction = \App\Models\Loader_to_warehouse::whereBetween("updated_at", [$weekStart, $weekEnd])
                ->where('loader_id', $id)
                ->finished()->loadWarehouse()->get();
            foreach ($weeksProduction as $production) {
                $weeksProd += $production->volume;
            }
            break;
        // Loader Materials From Ramp To Warehouse statistics
        case 9:
            $weeksProduction = \App\Models\Loader_to_warehouse::whereBetween("updated_at", [$weekStart, $weekEnd])
                ->where('loader_id', $id)
                ->finished()->loadMaterial()->get();
            foreach ($weeksProduction as $production) {
                $weeksProd += $production->volume;
            }
            break;
    }

    return $weeksProd;
}

function productionDay($day, $workspace)
{
    $dayStart = \Carbon\Carbon::now()->subDay($day)->format('Y-m-d');
    $daysProduction = \App\Models\Workspace_production::whereRaw("date(updated_at) = '" . $dayStart . "'")->where('workspace_id', $workspace)->get();
    $daysProd = 0;

    foreach ($daysProduction as $production) {
        $daysProd += $production->unloaded - $production->day_start + $production->day_end;
    }

    return $daysProd;
}

function workspaceMonthsProd($month, $id, $type)
{
    $monthStart = \Carbon\Carbon::now()->startOfMonth()->subMonth($month);
    $monthEnd = \Carbon\Carbon::now()->endOfMonth()->subMonth($month);
    $monthsProd = 0;

    switch ($type) {
        //  Workspace statistics
        case 0:
            $MonthsProduction = \App\Models\Workspace_production::whereRaw("date(updated_at) >= '" . $monthStart . "' and date(updated_at) <= '" . $monthEnd . "' and workspace_id = '" . $id . "'")->get();
            foreach ($MonthsProduction as $production) {
                $monthsProd += $production->unloaded - $production->day_start + $production->day_end;
            }
            break;
        // Loader statistics
        case 1:
            $MonthsProduction = \App\Models\Loader_to_warehouse::loadWarehouse()->whereRaw("date(updated_at) >= '" . $monthStart . "' and date(updated_at) <= '" . $monthEnd . "' and loader_id = '" . $id . "'")->get();
            foreach ($MonthsProduction as $production) {
                $monthsProd += $production->volume;
            }
            break;
        //  Worker statistics
        case 2:
            $MonthsProduction = \App\Models\Workspace_production::whereRaw("date(updated_at) >= '" . $monthStart . "' and date(updated_at) <= '" . $monthEnd . "' and worker_id = '" . $id . "'")->get();
            foreach ($MonthsProduction as $production) {
                $monthsProd += $production->unloaded - $production->day_start + $production->day_end;
            }
            break;
        //  Worker statistics
        case 3:
            $MonthsProduction = \App\Models\Loader_to_warehouse::whereRaw("date(updated_at) >= '" . $monthStart . "' and date(updated_at) <= '" . $monthEnd . "' and warehouse_id = '" . $id . "'")->finished()->loadWarehouse()->get();
            foreach ($MonthsProduction as $production) {
                $monthsProd += $production->volume;
            }
            break;
    }

    return $monthsProd;
}

function getWeek($week)
{
    $weekNumber = \Carbon\Carbon::now()->subWeek($week)->format('W');

    return $weekNumber;
}

function getMonth($number)
{
    $month = \Carbon\Carbon::now()->subMonth($number)->format('Y-m');

    return $month;
}

function getDay($number)
{
    $month = \Carbon\Carbon::now()->subDay($number)->format('Y-m-d');

    return $month;
}

function weekDayData($weekDay, $workspaceId)
{
    $dayVol = 0;
    foreach ($weekDay as $day) {
        if ($day->workspace_id == $workspaceId) {
            $dayVol += $day->unloaded - $day->day_start + $day->day_end;
        }
    }

    return $dayVol;
}

function weekDayDataLoaded($weekDay, $LoaderId)
{
    $dayVol = 0;
    foreach ($weekDay as $day) {
        if ($day->loader_id == $LoaderId && $day->type == 0) {
            $dayVol += $day->volume;
        }
    }

    return $dayVol;
}

function weekDayDataUnloaded($weekDay, $LoaderId)
{
    $dayVol = 0;
    foreach ($weekDay as $day) {
        if ($day->loader_id == $LoaderId && $day->type == 1) {
            $dayVol += $day->volume;
        }
    }

    return $dayVol;
}

function workspaceStat($period, $subjectId, $type)
{
    $lodedValue = 0;
    foreach ($period as $produced) {
        switch ($type) {
            //  Workspace statistics
            case 0:
                if ($produced->workspace_id == $subjectId) {
                    $lodedValue += $produced->unloaded - $produced->day_start + $produced->day_end;
                }
                break;
            //Loaded production
            case 1:
                if ($produced->loader_id == $subjectId) {
                    $lodedValue += $produced->volume;
                }
                break;
        }
    }

    return $lodedValue;
}

function convert_plural_tes($word, $number)
{
    if(\Illuminate\Support\Facades\App::getLocale() == 'lt'){
        switch ($number){
            case (substr($number, -1) == 1):
                $pluralized = $word . 'tė';
                break;
            case ($number > 1 && $number < 10):
                $pluralized = $word . 'tės';
                break;
            case ($number >= 10 && $number <= 20):
                $pluralized = $word . 'čių';
                break;
            default:
                $pluralized = $word . 'tės';
        }
    }else{
        switch ($number){
            case ($number < 1):
                $pluralized = $word . 'te';
                break;
            default:
                $pluralized = $word . 'tes';
        }
    }

    return $pluralized;
}

function monthsBreakdown($month)
{
    $monthStart = \Carbon\Carbon::now()->startOfMonth()->subMonth($month);
    $monthEnd = \Carbon\Carbon::now()->endOfMonth()->subMonth($month);
    $monthsBreak = 0;

    $monthsBreakdown = \App\Models\Breakdown::whereBetween("created_at", [$monthStart, $monthEnd])->finished()->get();
    foreach ($monthsBreakdown as $production) {
        $monthsBreak ++;
    }

    return $monthsBreak;
}

function convertDateFormat($date)
{
    $newDate = new \Carbon\Carbon($date);

    return $newDate;
}

function displayStatusIcon($status){
    switch ($status){
        case 0:
            $icon = '<i class="fa fa-circle-o" style="color: #E74C3C;" aria-hidden="true"></i>';
            break;
        case 1:
            $icon = '<i class="fa fa-circle" style="color: #1ABB9C;" aria-hidden="true"></i>';
            break;
        case 2:
            $icon = '<i class="fa fa-clock-o" style="color: #cebd00;" aria-hidden="true"></i>';
            break;
        case 3:
            $icon = '<i class="fa fa-check-circle-o" style="color: #1ABB9C;" aria-hidden="true"></i>';
            break;
        default :
            $icon = '<i class="fa fa-circle" style="color: #34495E;" aria-hidden="true"></i>';
    }

    echo $icon;
    return null;
}