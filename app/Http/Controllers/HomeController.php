<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\App;
use App\Models\Worker;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if (Auth::user()->isAdmin()) {
            return redirect()->action('Admin\AdminController@index');
        }

        return redirect()->action('Worker\WorkerHomeController@index');
    }

    public function setLanguage($lang)
    {

        App::setLocale($lang);

        return redirect()->back();
    }
}
