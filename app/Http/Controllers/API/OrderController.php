<?php

namespace App\Http\Controllers\API;

use App\Events\Board;
use App\Events\Factory;
use App\Models\Loader_to_warehouse;
use App\Models\Workspace_production;
use App\Traits\EventDataCreationTrait;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    use EventDataCreationTrait;

    public $successStatus = 200;

    public function getOrders()
    {
        $workspace = Auth::guard('api')->user()->worker->workspaces->first();

        $orders = $workspace->orders()->notFinished()->orderBy('status', 'desc')->orderBy('sort', 'ASC')->get();

        return response()->json(['order' => $orders], 200, [], JSON_NUMERIC_CHECK);
    }

    public function callLoader()
    {
        $fillLoader = Loader_to_warehouse::create([
            'worker_id' => Auth::guard('api')->user()->worker->id,
            'loader_id' => request('loader_id'),
            'workspace_id' => Auth::guard('api')->user()->worker->workspaces->first()->id,
            'warehouse_id' => request('warehouse_id'),
            'volume' => request('volume'),
            'order_id' => request('order_id'),
            'status' => 0,
            'material_id' => 0,
            'type' => 0,
        ]);

        $user = Auth::guard('api')->user();

        $data = $this->createDataArrayForCallLoader($user, $fillLoader);

        event(new Board($data));
        event(new Factory($data));

        $workspace = Auth::guard('api')->user()->worker->workspaces->first();
        $workspace_production = $workspace->workspace_productions->last();
        $workspace_production->update(['unloaded' => $workspace_production->unloaded + request('volume')]);

        $order = $workspace->orders()->active()->first();

        $order->update(['quantity_made' => $order->quantity_made + request('volume')]);

        if ($order->orderFinished) {
            $order->update(['status' => 3]);

            $order = $workspace->orders()->notFinished()->orderBy('status', 'desc')->orderBy('sort', 'ASC')->first();
            $order->update(['status' => 1]);

            $workspace_production->update([
                'day_end' => 0,
                'check' => true
            ]);

            $workspace_production = Workspace_production::create([
                'worker_id' => Auth::guard('api')->user()->worker->id,
                'workspace_id' => $workspace->id,
                'order_id' => $order->id,
                'day_start' => 0,
                'day_end' => 0,
                'unloaded' => 0,
                'check' => true,
            ]);
        }

        return response()->json([
            'order' =>  array($order),
            'workspace' =>  array($workspace_production),
        ], 200, [], JSON_NUMERIC_CHECK);
    }
    
}
