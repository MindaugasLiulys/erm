<?php

namespace App\Http\Controllers\API;

use App\Events\Board;
use App\Events\Factory;
use App\Http\Controllers\Controller;
use App\Models\Action;
use App\Models\Breakdown;
use App\Models\Supervisor_call;
use App\Models\Task;
use App\Models\Worker_to_workspace;
use App\Models\Workspace;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function get()
    {
        $loader = Auth::guard('api')->user()->worker->loader->first();

        return response()->json(['data' => array($loader)], 200, [], JSON_NUMERIC_CHECK);
    }

    public function callManager()
    {
        app()->setLocale('en');
        $loader = Auth::guard('api')->user()->worker->loader->first();
        $workspace = Auth::guard('api')->user()->worker->workspaces->first();
        $user = Auth::guard('api')->user();

        Supervisor_call::create([
            'workspace_id' => $loader === null ? $workspace->id : 0,
            'loader_id' => $loader === null ? 0 : $loader->id,
            'status' => 0
        ]);

        $action = new Action();
        $action->user_id = $user->id;
        $action->loader_id = $loader === null ? 0 : $loader->id;
        $action->action_name = trans('site.Supervisor called');
        $action->actions_status = 6;
        $action->save();

        if ($loader !== null) {
            $data = [
                'supervisorStatus' => 2,
                'loader_id' => $loader->id,
                'event_id' => $action->id,
                'action_name' => $action->action_name,
                'actions_status' => getActionByStatus(6)['status'],
                'workplace' => "",
                'loader_name' => $loader->name,
                'name' => $user->worker->name,
                'surname' => $user->worker->surname,
                'time' => $action->created_at->format('H:i:s Y-m-d '),
                'class' => getAction('call_manager')['class'],
                'iconClass' => getAction('call_manager')['i'],
                'loader_class' => getAction('call_manager')['span'],
            ];
        } else {
            $data = [
                'supervisorStatus' => 1,
                'workspace_id' => $workspace->id,
                'event_id' => $action->id,
                'action_name' => $action->action_name,
                'actions_status' => getActionByStatus(6)['status'],
                'workplace' => $workspace->name,
                'loader_name' => trans('site.Not selected'),
                'name' => $user->worker->name,
                'surname' => $user->worker->surname,
                'time' => $action->created_at->format('H:i:s Y-m-d '),
                'class' => getAction('call_manager')['class'],
                'iconClass' => getAction('call_manager')['i'],
                'loader_class' => getAction('call_manager')['span'],
            ];
        }

        event(new Factory($data));
        event(new Board($data));

        $dataResponse = [
            'success' => true
        ];

        return response()->json(['data' => array($dataResponse)], 200, [], JSON_NUMERIC_CHECK);
    }

    public function callManagerFinish()
    {
        $loader = Auth::guard('api')->user()->worker->loader->first();
        $workspace = Auth::guard('api')->user()->worker->workspaces->first();

        if ($loader === null) {
            Supervisor_call::called()->toWorkspace()
                ->where('workspace_id', $workspace->id)->update([
                    'status' => 1
                ]);
        } else {
            Supervisor_call::called()->toLoader()
                ->where('loader_id', $loader->id)->update([
                    'status' => 1
                ]);
        }

        $dataResponse = [
            'success' => true
        ];

        return response()->json(['data' => array($dataResponse)], 200, [], JSON_NUMERIC_CHECK);
    }

    public function callMechanic()
    {
        app()->setLocale('en');
        $loader = Auth::guard('api')->user()->worker->loader->first();
        $user = Auth::guard('api')->user();

        $action = new Action();
        $action->user_id = $user->id;
        $action->loader_id = $loader !== null ? $loader->id : 0;
        $action->action_name = trans('site.Mechanic called');
        $action->actions_status = $loader !== null ? 5 : 9;
        $action->save();

        Breakdown::create([
            'type' => $loader !== null ? 1 : 0,
            'text' => $loader !== null ?
                getAction('broken')['message'] : getAction('workspace_broken')['message'],
            'time' => 0,
            'status' => 1,
            'breakdown_id' => $loader !== null ?
                $loader->id : $user->worker->workspaces->first()->id,
        ]);

        if ($loader !== null) {
            $loader->update(['status' => 3]);
            $data = [
                'supervisorStatus' => 2,
                'loader_id' => $loader->id,
                'event_id' => $action->id,
                'action_name' => $action->action_name,
                'actions_status' => getActionByStatus(5)['status'],
                'workplace' => "",
                'loader_name' => $loader->name,
                'name' => $user->worker->name,
                'surname' => $user->worker->surname,
                'time' => $action->created_at->format('H:i:s Y-m-d '),
                'class' => getAction('broken')['class'],
                'iconClass' => getAction('broken')['i'],
                'loader_class' => getAction('broken')['span'],
                'brakedown' => true
            ];
        } else {
            $user->worker->workspaces->first()->update([
                'status' => 2
            ]);
            $data = [
                'supervisorStatus' => 1,
                'workspace_id' => $user->worker->workspaces->first()->id,
                'workspace' => $user->worker->workspaces->first()->name,
                'event_id' => $action->id,
                'action_name' => $action->action_name,
                'actions_status' => getActionByStatus(9)['status'],
                'workplace' => $user->worker->workspaces->first()->name,
                'loader_name' => trans('site.Not selected'),
                'name' => $user->worker->name,
                'surname' => $user->worker->surname,
                'time' => $action->created_at->format('H:i:s Y-m-d '),
                'class' => getAction('workspace_broken')['class'],
                'iconClass' => getAction('workspace_broken')['i'],
                'loader_class' => getAction('workspace_broken')['span'],
                'brakedown' => true
            ];
        }

        event(new Factory($data));
        event(new Board($data));

        $dataResponse = [
            'success' => true
        ];

        return response()->json(['data' => array($dataResponse)], 200, [], JSON_NUMERIC_CHECK);
    }

    public function getTasksCount()
    {
        $loader = Auth::guard('api')->user()->worker->loader->first();
        if ($loader === null) {
            $tasks = Task::active()->toWorker()->where('worker_id', Auth::guard('api')->user()->worker->id)->get();
        } else {
            $tasks = Task::active()->toLoader()->where('loader_id', $loader->id)->get();
        }

        return response()->json(['task' => array(['total' => count($tasks)])], 200, [], JSON_NUMERIC_CHECK);
    }

    public function getTasks()
    {
        $loader = Auth::guard('api')->user()->worker->loader->first();
        if ($loader === null) {
            $tasks = Task::active()->toWorker()->where('worker_id', Auth::guard('api')->user()->worker->id)->get();
        } else {
            $tasks = Task::active()->toLoader()->where('loader_id', $loader->id)->get();
        }

        return response()->json(['task' => $tasks], 200, [], JSON_NUMERIC_CHECK);
    }

    public function finishTask()
    {
        $task = Task::find(request('id'))->update(['status' => 0]);

        return response()->json(['task' => $task], 200, [], JSON_NUMERIC_CHECK);
    }
}