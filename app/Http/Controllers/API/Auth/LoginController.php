<?php

namespace App\Http\Controllers\API\Auth;

use App\Events\Login;
use App\Models\Loader;
use App\Models\Worker;
use App\Models\Workspace_production;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\Client;

class LoginController extends Controller
{
    use IssueTokenTrait;

    private $client;

    public function __construct() {
        $this->client = Client::find(2);
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'username' => 'required',
            'password' => 'required|min:6'
        ]);
        
        return $this->issueToken($request, 'password');
    }

    public function refresh(Request $request)
    {
        $this->validate($request, [
            'refresh_token' => 'required',
        ]);

        return $this->issueToken($request, 'refresh_token');
    }

    public function logout()
    {
        $accessToken = Auth::guard('api')->user()->token();

        $loader_id = Auth::guard('api')->user()->worker->loader->first()->id;
        $loader = Loader::find($loader_id);
        $loader->status = 0;
        $loader->save();

        $worker = Auth::guard('api')->user()->workerData;
        $loader_id = Auth::guard('api')->user()->worker->loader->first()->id;
        $worker['loader_id'] = $loader_id;

        $data = [
            'workerUserId'        => Auth::guard('api')->user()->id,
            'status'              => 'logout',
            'loaderId'            => $loader_id
        ];

        DB::table('oauth_refresh_tokens')
            ->where('access_token_id', $accessToken->id)
            ->update(['revoked' => true]);

        $accessToken->revoke();

        event(new Login($data));

        return response()->json(['data' => array($worker)], 200, [], JSON_NUMERIC_CHECK);
    }

    public function logoutWorker()
    {
        $accessToken = Auth::guard('api')->user()->token();
        $worker = Auth::guard('api')->user()->workerData;

        $quantities = Workspace_production::where('worker_id', Auth::guard('api')->user()->worker->id)->get()->last();
        $quantities->check = 0;
        $quantities->save();
        
        $data = [
            'workerUserId'        => Auth::guard('api')->user()->id,
            'status'              => 'logout'
        ];

        Auth::guard('api')->user()->worker->update([
            'status' => 0
        ]);

        DB::table('oauth_refresh_tokens')
            ->where('access_token_id', $accessToken->id)
            ->update(['revoked' => true]);

        $accessToken->revoke();

        event(new Login($data));

        return response()->json(['data' => array($worker)], 200, [], JSON_NUMERIC_CHECK);
    }

    public function managerJob()
    {
        
    }
}
