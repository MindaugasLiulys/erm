<?php

namespace App\Http\Controllers\API;

use App\Events\Board;
use App\Events\Factory;
use App\Events\Login;
use App\Http\Controllers\Controller;
use App\Models\Breakdown;
use App\Models\Loader;
use App\Models\Loader_to_warehouse;
use App\Models\Order;
use App\Models\Product;
use App\Models\Product\Material;
use App\Models\Warehouse;
use App\Models\Workspace_production;
use App\Traits\EventDataCreationTrait;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class WorkerController extends Controller
{
    use EventDataCreationTrait;

    public $successStatus = 200;

    public function getWorkerOrder()
    {
        $worker = Auth::guard('api')->user()->workerData;
        $workspace = Auth::guard('api')->user()->worker->workspaces->first();
        $data = [
            'workerUserId' => Auth::guard('api')->user()->id,
            'status' => 'login'
        ];
        $worker['workspace_id'] = $workspace->id;
        $worker['workspace'] = $workspace->name;

        Auth::guard('api')->user()->worker->update([
            'status' => 1
        ]);

        $order = $workspace->orders()->active()->first();

        if (!isset($order)) {
            $order = $workspace->orders()->notActive()->first();
            if (isset($order)) {
                $order->update(['status' => 1]);
            }
        }

        event(new Login($data));

        return response()->json([
            'worker' => array($worker),
            'order' => array($order)
        ], 200, [], JSON_NUMERIC_CHECK);
    }

    public function getWorkspace()
    {
        $workspace = Auth::guard('api')->user()->worker->workspaces->first();
        $workspace_production = $workspace->workspace_productions->last();

        if ($workspace_production->created_at->isToday()) {
            $workspace_production->update([
                'check' => true
            ]);
        } else {
            $workspace_production->update([
                'check' => false
            ]);
        }

        return response()->json(['data' => array($workspace_production)], 200, [], JSON_NUMERIC_CHECK);
    }

    public function acceptBalance()
    {
        $workspace = Auth::guard('api')->user()->worker->workspaces->first();
        $worker = Auth::guard('api')->user()->worker;
        $order = Order::find(request('order_id'));
        $accept = request('accept');

        $work_prod = Workspace_production::find(request('id'));
        $work_prod->update([
            'check' => $accept == 1 ? true : false,
            'day_end' => request('balance'),
        ]);

        if ($accept == 1) {
            $work_prod = Workspace_production::create([
                'worker_id' => $worker->id,
                'workspace_id' => $workspace->id,
                'order_id' => $order->id,
                'day_start' => request('balance'),
                'day_end' => request('balance'),
                'unloaded' => 0,
                'check' => true,
            ]);
        }

        return response()->json(['data' => array($work_prod)], 200, [], JSON_NUMERIC_CHECK);
    }

    public function saveBalance()
    {
        $workspace = Auth::guard('api')->user()->worker->workspaces->first();
        $quantities = Workspace_production::where('worker_id', Auth::guard('api')->user()->worker->id)->orderBy('updated_at', 'desc')->first();

        $quantities->day_end = request('day_end');
        $quantities->save();

        $todayDay = Carbon::now()->dayOfWeek;

        $data = [
            'graphStatus' => 1,
            'workspaceId' => $workspace->id,
            'produced' => $quantities->unloaded + $quantities->day_end - $quantities->day_start,
            'todayDay' => $todayDay,
        ];

        event(new Factory($data));

        return response()->json(['data' => array($data)], 200, [], JSON_NUMERIC_CHECK);
    }

    public function getLoadersWarehouses()
    {
        app()->setLocale(request('lang'));
        $loaders = Loader::all();
        $warehouses = Warehouse::all();

        $data = array();
        $warehousesData = array();

        foreach ($loaders as $loader) {
            $worker = $loader->workers->first();
            array_push($data, [
                'name' => $worker->name,
                'surname' => $worker->surname,
                'loader_id' => $loader->id,
                'loader' => $loader->name,
                'status' => $loader->action->actions_status ? 0 : $loader->status,
                'action_name' => $loader->status ? getActionByStatus($loader->action->actions_status)['message']
                    : trans('site.not_active'),
            ]);
        }

        foreach ($warehouses as $warehouse) {
            array_push($warehousesData, [
                'id' => $warehouse->id,
                'name' => $warehouse->name,
                'space' => $warehouse->space,
                'reserved' => $warehouse->reserved,
                'used' => $warehouse->used,
            ]);
        }
        return response()->json([
            'loaders' => $data,
            'warehouses' => $warehousesData
        ], 200, [], JSON_NUMERIC_CHECK);
    }

    public function getLoadersMaterials()
    {
        app()->setLocale(request('lang'));
        $loaders = Loader::all();
        $workspace_production = Auth::guard('api')->user()->worker->workspaces->first()->workspace_productions->last();
        $order = Order::find($workspace_production->order_id);
        $materials = $order->product->materials;

        $data = array();
        $materialsData = array();

        foreach ($loaders as $loader) {
            $worker = $loader->workers->first();
            array_push($data, [
                'name' => $worker->name,
                'surname' => $worker->surname,
                'loader_id' => $loader->id,
                'loader' => $loader->name,
                'status' => $loader->action->actions_status ? 0 : $loader->status,
                'action_name' => $loader->status ? getActionByStatus($loader->action->actions_status)['message']
                    : trans('site.not_active'),
            ]);
        }

        foreach ($materials as $material) {
            array_push($materialsData, [
                'id' => $material->id,
                'name' => $material->name
            ]);
        }
        return response()->json([
            'loaders' => $data,
            'materials' => $materialsData
        ], 200, [], JSON_NUMERIC_CHECK);
    }

    public function callLoaderForMaterial()
    {
        $material = Material::find(request('material_id'));

        $fillLoader = Loader_to_warehouse::create([
            'worker_id' => Auth::guard('api')->user()->worker->id,
            'loader_id' => request('loader_id'),
            'workspace_id' => Auth::guard('api')->user()->worker->workspaces->first()->id,
            'warehouse_id' => Material::find(request('material_id'))->warehouse->id,
            'material_id' => request('material_id'),
            'volume' => request('volume'),
            'status' => 0,
            'type' => 1,
        ]);

        $user = Auth::guard('api')->user();

        $data = $this->createDataArrayForCallMaterial($user, $fillLoader, $material);

        event(new Board($data));
        event(new Factory($data));

        return response()->json(['data' => array($fillLoader)], 200, [], JSON_NUMERIC_CHECK);
    }

    public function getLatestJobs()
    {
        $workspace = Auth::guard('api')->user()->worker->workspaces->first();
        $jobs = Loader_to_warehouse::latestJobs()->workspaceJobs()->where('workspace_id', $workspace->id)->orderBy('created_at', 'desc')->get();

        return response()->json(['data' => $jobs], 200, [], JSON_NUMERIC_CHECK);
    }

    public function setWorkspaceStatus()
    {
        app()->setLocale("en");
        $workspace = Auth::guard('api')->user()->worker->workspaces->first();
        $workspace->status = request('status');
        $workspace->save();

        $data = $this->createDataArrayForBreakWorkspace(Auth::guard('api')->user(), $workspace);

        event(new Board($data));
        event(new Factory($data));

        $responseData = ['status' => request('status')];

        return response()->json(['data' => array($responseData)], 200, [], $this->successStatus);
    }

    public function workspaceFixed()
    {
        app()->setLocale("en");
        $workspace = Auth::guard('api')->user()->worker->workspaces->first();
        $workspace->status = request('status') ? 2 : 1;
        $workspace->save();

//        $data = $this->createDataArrayForLoaderFixed(Auth::guard('api')->user(), $workspace,
//            request('status') == 1 ? getAction('workspace_broken') : getAction('waiting'));
//
//        event(new Board($data));
//        event(new Factory($data));

        Breakdown::active()->toWorkspace()
            ->where('breakdown_id', $workspace->id)
            ->first()->update([
                'text' => request('message'),
                'time' => request('time'),
                'status' => request('status') ? 1 : 0
            ]);

        $responseData = ['status' => request('status')];

        return response()->json(['data' => array($responseData)], 200, [], $this->successStatus);
    }

}
