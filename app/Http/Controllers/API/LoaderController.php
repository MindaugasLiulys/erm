<?php

namespace App\Http\Controllers\API;

use App\Events\Board;
use App\Events\Factory;
use App\Events\Login;
use App\Events\Map;
use App\Models\Breakdown;
use App\Models\Loader;
use App\Models\Loader_to_warehouse;
use App\Http\Controllers\Controller;
use App\Models\OrderWarehouse;
use App\Models\Product\Material;
use App\Models\ProductWarehouse;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Traits\EventDataCreationTrait;

class LoaderController extends Controller
{
    use EventDataCreationTrait;

    public $successStatus = 200;

    public function startJob()
    {
        app()->setLocale("en");
        $job = Loader_to_warehouse::find(request('id'));
        if (is_null($job) || $job->status == 2) {
            return response()->json(false, $this->successStatus);
        }

        $job->update([
            'status' => 1,
            'start_at' => Carbon::createFromFormat('Y-m-d H:i:s', Carbon::now())->toDateTimeString(),
        ]);

        $data = $this->createDataArrayForStartJob(Auth::guard('api')->user(), $job);

        event(new Board($data));
        event(new Factory($data));

        return response()->json(['data' => array(Loader_to_warehouse::find(request('id')))], 200, [], $this->successStatus);
    }

    public function endJob()
    {
        app()->setLocale("en");
        $job = Loader_to_warehouse::find(request('id'));
        if (is_null($job) || $job->status == 2) {
            return response()->json(false, $this->successStatus);
        }
        $job->update([
            'status' => 2
        ]);

        $data = $this->createDataArrayForEndJob(Auth::guard('api')->user(), $job);

        if ($job->type == 0) {
            $orderWarehouse = OrderWarehouse::where('order_id', $job->order_id)->where('warehouse_id', $job->warehouse_id)->first();
            if (count($orderWarehouse) > 0) {
                $orderWarehouse->update(['quantity' => $orderWarehouse->quantity + $job->volume]);
            } else {
                OrderWarehouse::create([
                    'order_id' => $job->order_id,
                    'warehouse_id' => $job->warehouse_id,
                    'quantity' => $job->volume,
                ]);
            }
        } elseif ($job->type == 1) {
            foreach (Material::where('id', $job->material_id)->cursor() as $material) {
                $material->quantity = $material->quantity - $job->volume;
                $material->save();
            }
        } elseif ($job->type == 2) {
            $orderWarehouse = OrderWarehouse::where('order_id', $job->order_id)->where('warehouse_id', $job->warehouse_id)->first();
            if ($orderWarehouse->quantity == $job->volume) {
                $orderWarehouse->delete();
            } else {
                $orderWarehouse->update(['quantity' => $orderWarehouse->quantity - $job->volume]);
            }
        } elseif ($job->type == 3) {
            foreach (Material::where('id', $job->material_id)->cursor() as $material) {
                $material->quantity = $material->quantity + $job->volume;
                $material->save();
            }
        } elseif ($job->type == 4) {
            $orderWarehouse = OrderWarehouse::where('order_id', $job->order_id)->where('warehouse_id', $job->warehouse_id)->first();
            $orderWarehouse->update(['quantity' => $orderWarehouse->quantity + $job->volume]);
        }

        event(new Board($data));
        event(new Factory($data));

        return response()->json(['data' => array(Loader_to_warehouse::find(request('id')))], 200, [], $this->successStatus);
    }

    public function getLoader()
    {
        $worker = Auth::guard('api')->user()->workerData;
        $loader = Auth::guard('api')->user()->worker->loader->first();
        $data = [
            'workerUserId' => Auth::guard('api')->user()->id,
            'status' => 'login',
            'loaderId' => $loader->id,
        ];
        $worker['loader_id'] = $loader->id;
        $worker['loader'] = $loader->name;
        $worker['status'] = $loader->status;

        event(new Login($data));

        return response()->json(['data' => array($worker)], 200, [], JSON_NUMERIC_CHECK);
    }

    public function getAllJobs()
    {
        $loader_id = Auth::guard('api')->user()->worker->loader->first()->id;
        $jobs = Loader_to_warehouse::active()->where('loader_id', $loader_id)->get();

        return response()->json(['data' => $jobs], 200, [], JSON_NUMERIC_CHECK);
    }

    public function getJobsDone()
    {
        $loader_id = Auth::guard('api')->user()->worker->loader->first()->id;
        $jobs = Loader_to_warehouse::jobsDoneToday()->where('loader_id', $loader_id)->get();

        return response()->json(['data' => $jobs], 200, [], JSON_NUMERIC_CHECK);
    }

    public function getJobsDoneWeek()
    {
        $loader_id = Auth::guard('api')->user()->worker->loader->first()->id;
        $jobs = Loader_to_warehouse::jobsInAWeek()->where('loader_id', $loader_id)->get();

        return response()->json(['data' => $jobs], 200, [], JSON_NUMERIC_CHECK);
    }

    public function getLatestJob()
    {
        $loader_id = Auth::guard('api')->user()->worker->loader->first()->id;
        Loader::find($loader_id)->update(['status' => 1]);

        $job = Loader_to_warehouse::active()->where('loader_id', $loader_id)->get()->first();

        return response()->json(['data' => array($job)], 200, [], JSON_NUMERIC_CHECK);
    }

    public function setLoaderStatus()
    {
        app()->setLocale("en");
        $loader_id = Auth::guard('api')->user()->worker->loader->first()->id;
        $loader = Loader::find($loader_id);
        $loader->status = request('status');
        $loader->save();

        $data = $this->createDataArrayForBreak(Auth::guard('api')->user(), $loader);

        event(new Board($data));
        event(new Factory($data));

        $responseData = ['status' => request('status')];

        return response()->json(['data' => array($responseData)], 200, [], $this->successStatus);
    }

    public function loaderFixed()
    {
        app()->setLocale("en");
        $loader = Auth::guard('api')->user()->worker->loader->first();
        $loader->status = request('status') ? 1 : 3;
        $loader->save();

        $data = $this->createDataArrayForLoaderFixed(Auth::guard('api')->user(), $loader,
            request('status') == 1 ? getAction('broken') : getAction('waiting'));

        event(new Board($data));
        event(new Factory($data));

        Breakdown::active()->toLoader()
            ->where('breakdown_id', $loader->id)
            ->first()->update([
                'text' => request('message'),
                'time' => request('time'),
                'status' => request('status') ? 0 : 1
            ]);

        $responseData = ['status' => request('status')];

        return response()->json(['data' => array($responseData)], 200, [], $this->successStatus);
    }

    public function startJobLater()
    {
        app()->setLocale("en");
        $job = Loader_to_warehouse::find(request('id'));
        if (is_null($job) || $job->status == 2) {
            return response()->json(false, $this->successStatus);
        }

        $job->update([
            'status' => 1,
            'start_at' => request('start_at')
        ]);

        $data = $this->createDataArrayForStartJobLater(Auth::guard('api')->user(), $job);

        event(new Board($data));
        event(new Factory($data));

        return response()->json(['data' => array($job)], 200, [], $this->successStatus);
    }

    public function changeLocation()
    {
        $loader_id = Auth::guard('api')->user()->worker->loader->first()->id;
        $loader = Loader::find($loader_id);
        $loader->lang = request('lang');
        $loader->long = request('long');
        $loader->save();

        $data['loaders'] = Loader::active()->get()->toJson();

        event(new Map($data));

        $responseData = ['status' => $loader->status];

        return response()->json(['data' => array($responseData)], 200, [], $this->successStatus);
    }
}
