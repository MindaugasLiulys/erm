<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use Illuminate\Support\Facades\Input;

class ProductsController extends Controller
{
    public function index()
    {
        $products = Product::get();
        $number = 1;

        return view('admin.products.index', compact('products', 'number'));
    }

    public function create()
    {
        $allMaterials = Product\Material::all();
        return view('admin.products.create', compact('allMaterials'));
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $product = new Product();
        $product->name = request('name');
        $product->save();

        $materials = !empty($data['materials']) ? $data['materials'] : [];

        $product->materials()->attach($materials);

        return redirect()->action('Admin\ProductsController@index');
    }

    public function edit($id)
    {
        $product = Product::find($id);
        $allMaterials = Product\Material::all();
        $materials = $product->materials;

        $materialNotAdded = $allMaterials->diff($materials)->toArray();
        return view('admin.products.edit', compact('product', 'materials', 'materialNotAdded'));
    }

    public function save(Request $request, $id)
    {
        $data = $request->all();
        $product = Product::find($id);
        $product->name = request('name');
        $product->save();

        $materials = !empty($data['materials']) ? $data['materials'] : [];
        $product->materials()->sync($materials);

        return redirect()->action('Admin\ProductsController@index');
    }

    public function drop($id)
    {
        $product = Product::find($id);
        $product->delete();

        return redirect()->back();
    }
}