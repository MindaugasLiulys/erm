<?php

namespace App\Http\Controllers\Admin;

use App\Models\Position;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Facades\DB;

class PositionsController extends Controller
{
    public function index(){

        $positions = Position::get();

        return view('admin.positions.index', compact('positions'));
    }

    protected function create(){

        return view('admin.positions.create', compact(''));
    }

    public function store(){

        $position = new Position();
        $active = 0;
        if(Input::get('active') != 0){
            $active = 1;
        }

        $position->name = Input::get('position');
        $position->active = $active;

        $position->save();

        return redirect()->action('Admin\PositionsController@index');
    }

    public function edit($id){

        $position = Position::find($id);

        return view('admin.positions.edit', compact('position'));
    }

    public function save($id){

        $position = Position::find($id);

        $position->name = Input::get('position');
        $position->active = Input::get('active');

        $position->save();

        return redirect()->action('Admin\PositionsController@index');
    }

    public function drop($id){

        $worker = Position::find($id);
        $worker->delete();

        return redirect()->action('Admin\PositionsController@index');
    }
}
