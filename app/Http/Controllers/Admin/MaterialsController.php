<?php

namespace App\Http\Controllers\Admin;

use App\Models\Product\Material;
use App\Models\Warehouse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MaterialsController extends Controller
{
    public function index()
    {
        $materials = Material::all();

        return view('admin.materials.index', compact('materials'));
    }

    public function create()
    {
        $warehouses = Warehouse::get();

        return view('admin.materials.create', compact('warehouses'));
    }

    public function store()
    {
        $material = new Material();
        $material->name = request('name');
        $material->quantity = request('quantity');
        $material->warehouse_id = request('warehouseId');
        $material->save();

        return redirect()->action('Admin\MaterialsController@index');
    }

    public function edit($id)
    {
        $material = Material::find($id);
        $warehouses = Warehouse::get();

        return view('admin.materials.edit', compact('material', 'warehouses'));
    }

    public function update(Request $request, $id)
    {
        $material = Material::find($id);
        $material->name = request('name');
        $material->quantity = request('quantity');
        $material->warehouse_id = request('warehouseId');
        $material->save();

        return redirect()->action('Admin\MaterialsController@index');
    }

    public function destroy($id)
    {
        Material::destroy($id);

        return redirect()->back();
    }
}
