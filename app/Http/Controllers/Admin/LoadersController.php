<?php

namespace App\Http\Controllers\Admin;

use App\Models\Action;
use App\Models\Loader_to_warehouse;
use App\Models\Worker;
use App\Models\Loader;
use App\Models\Worker_to_loader;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class LoadersController extends Controller
{
    public function index(){

        $loaders = Loader::get();
        $workers = Worker::with('loaderTo')->get();

        $availWorkers = Worker::doesntHave('loaderTo')->get();

        $thisMonthStart = Carbon::now()->startOfMonth()->format('Y-m-d');
        $thisMonthEnd = Carbon::now()->endOfMonth()->format('Y-m-d');
        $lastMonthStart = Carbon::now()->startOfMonth()->subMonth()->format('Y-m-d');
        $lastMonthEnd = Carbon::now()->subMonth()->endOfMonth()->format('Y-m-d');

        $todaysLoaded = Loader_to_warehouse::finished()->loadWarehouse()->whereRaw('Date(updated_at) = CURDATE()')->get();
        $yesterdaysLoaded = Loader_to_warehouse::finished()->loadWarehouse()->whereRaw("Date(updated_at) = '" . Carbon::yesterday()->format('Y-m-d') . "'")->get();
        $thisMonthLoaded = Loader_to_warehouse::finished()->loadWarehouse()->whereRaw("Date(updated_at) >= '" . $thisMonthStart . "' and Date(updated_at) <= '" . $thisMonthEnd . "'")->get();
        $lastMonthLoaded = Loader_to_warehouse::finished()->loadWarehouse()->whereRaw("Date(updated_at) >= '" . $lastMonthStart . "' and Date(updated_at) <= '" . $lastMonthEnd . "'")->get();

        $todaysUnloaded = Loader_to_warehouse::finished()->bringMaterial()->whereRaw('Date(updated_at) = CURDATE()')->get();
        $yesterdaysUnloaded = Loader_to_warehouse::finished()->bringMaterial()->whereRaw("Date(updated_at) = '" . Carbon::yesterday()->format('Y-m-d') . "'")->get();
        $thisMonthUnloaded = Loader_to_warehouse::finished()->bringMaterial()->whereRaw("Date(updated_at) >= '" . $thisMonthStart . "' and Date(updated_at) <= '" . $thisMonthEnd . "'")->get();
        $lastMonthUnloaded = Loader_to_warehouse::finished()->bringMaterial()->whereRaw("Date(updated_at) >= '" . $lastMonthStart . "' and Date(updated_at) <= '" . $lastMonthEnd . "'")->get();

        return view('admin.loaders.index', compact('loaders', 'workers', 'availWorkers', 'todaysLoaded', 'yesterdaysLoaded', 'thisMonthLoaded', 'lastMonthLoaded', 'todaysUnloaded', 'yesterdaysUnloaded', 'thisMonthUnloaded', 'lastMonthUnloaded'));
    }

    public function create(){

        return view('admin.loaders.create');
    }

    public function store(){

        $loader = new Loader();
        $loader->name = Input::get('name');
        $loader->color = Input::get('favcolor');
        $loader->save();

        $action = new Action();
        $action->user_id = Auth::user()->id;
        $action->loader_id = $loader->id;
        $action->action_name = 'Waiting';
        $action->actions_status = 0;
        $action->save();

        return redirect()->action('Admin\LoadersController@index');
    }

    public function edit($id){

        $loader = Loader::find($id);

        return view('admin.loaders.edit', compact('loader'));
    }

    public function update($id){

        $loader = Loader::find($id);
        $loader->name = Input::get('name');
        $loader->color = Input::get('favcolor');
        $loader->save();

        return redirect()->action('Admin\LoadersController@index');
    }

    public function drop($id){

        $loader = Loader::find($id);
        $loader->delete();

        return redirect()->action('Admin\LoadersController@index');
    }

    //  $id = workplace id, $worker = worker id
    public function addWorkerToLoader($id, $worker){

        if(is_numeric($worker)){
            $loaderTo = new Worker_to_loader();
            $loaderTo->worker_id = $worker;
            $loaderTo->loader_id = $id;
            $loaderTo->save();
        }

        return redirect()->action('Admin\LoadersController@index');
    }

    public function removeWorkerFromLoader($id){

        $loaderTo = Worker_to_loader::where('worker_id', $id)->first();

        $loaderTo->delete();

        return redirect()->action('Admin\LoadersController@index');
    }
}
