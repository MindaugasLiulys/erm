<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\Product;
use App\Models\Setting;
use App\Models\Workspace;
use Illuminate\Http\Request;
use PDF;

class OrdersController extends Controller
{
    public function index()
    {
        $ordersTable = Order::with('product', 'workspace')->orderBy('created_at', 'DESC')->get();
        $orders = Order::notFinished()->with('product', 'workspace')->orderBy('status', 'desc')->orderBy('sort', 'ASC')->get();
        $finished = Order::finished()->with('product', 'workspace')->orderBy('sort', 'ASC')->orderBy('status', 'asc')->get();
        $orders = $orders->merge($finished);
        $workspaces = Workspace::get();
        $products = Product::get();

        return view('admin.orders.index', compact('orders', 'workspaces', 'products', 'workers', 'ordersTable'));
    }

    public function createOrder(Request $request)
    {
        $workspace = $request->workspace_id;

        if(count(Order::get()) != 0 && count(Order::where('workspace_id', $workspace)->get()) != 0){
            $lastWorkspace = Order::where('workspace_id', $workspace)->orderBy('sort', 'DESC')->first()->sort + 1;
        }else{
            $lastWorkspace = 0;
        }
        Order::create(
            array_merge($request->all(), ['sort' => $lastWorkspace])
        );

        return redirect()->action('Admin\OrdersController@index');
    }

    public function sortOrders(Request $request)
    {
        $workspace = $request->workspace;
        $lastWorkspace = Order::where('workspace_id', $workspace)->orderBy('sort', 'DESC')->first()->sort ;
        foreach(array_reverse($request->item) as $key=>$data){
            $key = --$lastWorkspace;
            $order = Order::find($data);
            $order->sort = $key;
            $order->save();
        }

        return array($workspace, $lastWorkspace);
    }

    public function editOrder($id, Request $request)
    {
        Order::find($id)->update($request->all());

        return redirect()->action('Admin\OrdersController@index');
    }

    public function dropOrder($id)
    {
        Order::find($id)->delete();

        return redirect()->action('Admin\OrdersController@index');
    }

    public function generatePdf($id)
    {
        $order = Order::find($id);
        $settings = Setting::first();
        $settingsAddress = json_decode($settings->company_address, true);
        $emails = json_decode($settings->company_emails, true);
        $pdfTitle = $order->order_number . '_' . trans('site.order') . '.pdf';

        $pdf = PDF::loadView('admin.orders.order-pdf', compact('order', 'settings', 'settingsAddress', 'emails'));
        return $pdf->download($pdfTitle);
//        return view('admin.orders.order-pdf', compact('order', 'settings', 'settingsAddress', 'emails'));
    }
}
