<?php

namespace App\Http\Controllers\Admin;

use App\Models\Product;
use App\Models\Worker;
use App\Models\Worker_to_workspace;
use App\Models\Workspace;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use App\Http\Controllers\Controller;
use App\Models\Workspace_production;
use Carbon\Carbon;
use PDF;

class WorkspacesController extends Controller
{
    public function index()
    {
        $workspaces = Workspace::get();
        $workers = Worker::with('workspaces')->get();

        $availWorkers = Worker::doesntHave('workspaces')->get();

        $todays = Workspace_production::whereRaw('Date(updated_at) = CURDATE()')->get();
        $yesterdays = Workspace_production::whereRaw("Date(updated_at) = '" . Carbon::yesterday()->format('Y-m-d') . "'")->get();
        $thisMonthStart = Carbon::now()->startOfMonth()->format('Y-m-d');
        $thisMonthEnd = Carbon::now()->endOfMonth()->format('Y-m-d');
        $thisMonth = Workspace_production::whereRaw("Date(updated_at) >= '" . $thisMonthStart . "' and Date(updated_at) <= '" . $thisMonthEnd . "'")->get();

        $lastMonthStart = Carbon::now()->startOfMonth()->subMonth()->format('Y-m-d');
        $lastMonthEnd = Carbon::now()->subMonth()->endOfMonth()->format('Y-m-d');
        $lastMonth = Workspace_production::whereRaw("Date(updated_at) >= '" . $lastMonthStart . "' and Date(updated_at) <= '" . $lastMonthEnd . "'")->get();

        $productions = Workspace_production::orderBy('updated_at', 'desc')->with('worker')->get();

        $monday = Workspace_production::whereRaw("Date(updated_at) = '" . Carbon::now()->startOfWeek()->format('Y-m-d') . "'")->get();
        $tuesday = Workspace_production::whereRaw("Date(updated_at) = '" . Carbon::now()->startOfWeek()->addDay(1)->format('Y-m-d') . "'")->get();
        $wednesday = Workspace_production::whereRaw("Date(updated_at) = '" . Carbon::now()->startOfWeek()->addDay(2)->format('Y-m-d') . "'")->get();
        $thursday = Workspace_production::whereRaw("Date(updated_at) = '" . Carbon::now()->startOfWeek()->addDay(3)->format('Y-m-d') . "'")->get();
        $friday = Workspace_production::whereRaw("Date(updated_at) = '" . Carbon::now()->startOfWeek()->addDay(4)->format('Y-m-d') . "'")->get();
        $saturday = Workspace_production::whereRaw("Date(updated_at) = '" . Carbon::now()->startOfWeek()->addDay(5)->format('Y-m-d') . "'")->get();
        $sunday = Workspace_production::whereRaw("Date(updated_at) = '" . Carbon::now()->startOfWeek()->addDay(6)->format('Y-m-d') . "'")->get();

        return view('admin.workspaces.index', compact('workspaces', 'todays', 'yesterdays', 'thisMonth', 'lastMonth', 'workers', 'availWorkers', 'productions', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'));
    }

    public function create()
    {
        return view('admin.workspaces.create');
    }

    public function store(Request $request)
    {
        Workspace::create($request->all());

        return redirect()->action('Admin\WorkspacesController@index');
    }

    public function edit($id)
    {
        $workspace = Workspace::find($id);

        return view('admin.workspaces.edit', compact('workspace'));
    }

    public function update($id, Request $request)
    {
        Workspace::find($id)->update($request->all());

        return redirect()->action('Admin\WorkspacesController@index');
    }

    public function drop($id)
    {
        $workspace = Workspace::find($id);
        $workspace->delete();

        return redirect()->action('Admin\WorkspacesController@index');
    }

    public function addWorkerToWorkspace($id, $worker)
    {
        if(is_numeric($worker)){
            $workplaceTo = new Worker_to_workspace();
            $workplaceTo->worker_id = $worker;
            $workplaceTo->workspace_id = $id;
            $workplaceTo->save();
        }

        return redirect()->action('Admin\WorkspacesController@index');
    }

    public function removeWorkerFromWorkspace($id)
    {
        $workplaceTo = Worker_to_workspace::where('worker_id', $id)->first();

        $workplaceTo->delete();

        return redirect()->action('Admin\WorkspacesController@index');
    }

    public function workerStat($id)
    {
        $worker = Worker::find($id);
        $productions = Workspace_production::with('product')->get();

        $lang = App::getLocale();

        return view('admin.workspaces.workerStatistics', compact('worker', 'productions', 'lang'));
    }

    public function generatePdfDate($id, $from, $to)
    {
        $worker = Worker::find($id);
        $productions = Workspace_production::get();
        $title = $worker->name . $worker->surname . '_statistics_' . $from . '_' . $to . '.pdf';
        $settings = Setting::first();
        $settingsAddress = json_decode($settings->company_address, true);
        $emails = json_decode($settings->company_emails, true);

        $pdf = PDF::loadView('admin.workspaces.workerStatsPdf', compact('worker', 'productions', 'from', 'to', 'settings', 'settingsAddress', 'emails'));

        return $pdf->download($title);
    }

}
