<?php

namespace App\Http\Controllers\Admin;

use App\Models\Loader;
use App\Http\Controllers\Controller;
use App\Models\Loader_to_warehouse;
use App\Models\Warehouse;
use App\Models\Worker;
use App\Models\Worker_to_loader;
use App\Models\Worker_to_workspace;
use App\Models\Workspace;
use App\Models\Workspace_production;
use Carbon\Carbon;

class StatisticsController extends Controller
{
    public function index()
    {
        $workers = Worker_to_workspace::with('worker', 'worker.position', 'workspace')->get()->unique('id');
        $loaders = Loader::get();
        $workspacesAll = Workspace::get();
        $warehouses = Warehouse::get();
        $warehouseUsed = 0;
        $warehouseTotal = 0;
        foreach($warehouses as $warehouse){
            $warehouseUsed += $warehouse->used;
            $warehouseTotal += $warehouse->space;
        }

        $warehouseOccup = getPercent($warehouseTotal, $warehouseUsed);

        $monthStart = "date(updated_at) >= '" . Carbon::now()->startOfMonth() . "'";

        $workersProd = Workspace_production::whereRaw($monthStart)->orderBy('unloaded', 'desc')->paginate(5)->unique('worker_id');

        $loadersProd = Loader_to_warehouse::loadWarehouse()->whereRaw($monthStart)->orderBy('volume', 'desc')->paginate(5)->unique('loader_id');

        $workspaces = Workspace_production::whereRaw($monthStart)->get();

        $items = array();
        foreach($workspaces as $effWorkspace){
            if (array_key_exists($effWorkspace->workspace_id, $items)){
                $items[$effWorkspace->workspace_id]['produced'] += intval($effWorkspace->unloaded - $effWorkspace->day_start + $effWorkspace->day_end);
            }else{
                $items[$effWorkspace->workspace_id]['produced'] = intval($effWorkspace->unloaded - $effWorkspace->day_start + $effWorkspace->day_end);
            }

            $items[$effWorkspace->workspace_id]['name'] = $effWorkspace->workspace->name;
        }

        if(!empty($items)){
            $highest = array_search(max($items),$items);
            $lowest = array_search(min($items),$items);
        }

        if(request()->has('test'))
        {
            $start = Carbon::parse(request('start'))->diffInMonths(Carbon::now());
            $end = Carbon::parse(request('end'))->diffInMonths(Carbon::now());

            $data = array();
            for($i=$start; $i>=$end; $i--){
                $dataItem = array('m' => getMonth($i), 'a' => productionMonth($i));
                array_push($data, $dataItem);
            }

            return $data;
        }

        return view('admin.statistics.index', compact('workers', 'warehouseUsed', 'warehouses', 'workersProd', 'loadersProd', 'highest', 'items', 'lowest', 'loaders', 'workspacesAll', 'workspaces', 'data', 'req', 'warehouseOccup'));
    }

    public function workspaceStats($id)
    {
        $workspace = Workspace::find($id);
        $subject = $workspace;
        $workers = Worker_to_workspace::where('workspace_id', $id)->get();
        $type = 0;
        $currYear = Carbon::now()->format('Y');
        $weekDate = date("Y-m-d", strtotime($currYear . "W" . getWeek(11)));
        $production = Workspace_production::where('workspace_id', $id)->with('worker')->orderBy('updated_at', 'desc')->get();

        if(request()->has('test'))
        {
            $startWeekDate = date("Y-m-d", strtotime($currYear . "W" . request('start')));
            $endWeekDate = date("Y-m-d", strtotime($currYear . "W" . request('end')));

            $start = Carbon::parse($startWeekDate)->diffInWeeks(Carbon::now());
            $end = Carbon::parse($endWeekDate)->diffInWeeks(Carbon::now());

            $data = array();
            for($i=$start; $i>=$end; $i--){
                $dataItem = array('m' => getWeek($i), 'a' => productionWeek($i, $subject->id, $type));
                array_push($data, $dataItem);
            }

            return $data;
        }

        return view('admin.statistics.stats', compact('subject', 'workers', 'type', 'production', 'weekDate', 'data'));
    }

    public function loaderStats($id)
    {
        $loader = Loader::find($id);
        $subject = $loader;
        $workers = Worker_to_loader::where('loader_id', $id)->get();
        $type = 1;
        $type2 = 7;
        $type3 = 8;
        $type4 = 9;
        $currYear = Carbon::now()->format('Y');
        $weekDate = date("Y-m-d", strtotime($currYear . "W" . getWeek(11)));
        $production = Loader_to_warehouse::loadWarehouse()->with('loader', 'worker', 'warehouse')->where('loader_id', $id)->orderBy('updated_at', 'desc')->get();

        if(request()->has('test'))
        {
            $type1 = request('type1');
            $type2 = request('type2');
            $type3 = request('type3');
            $type4 = request('type4');

            $startWeekDate = date("Y-m-d", strtotime($currYear . "W" . request('start')));
            $endWeekDate = date("Y-m-d", strtotime($currYear . "W" . request('end')));

            $start = Carbon::parse($startWeekDate)->diffInWeeks(Carbon::now());
            $end = Carbon::parse($endWeekDate)->diffInWeeks(Carbon::now());

            $data = array();
            for($i=$start; $i>=$end; $i--){
                $dataItem = array('m' => getWeek($i), 'a' => productionWeek($i, $subject->id, $type1), 'b' => productionWeek($i, $subject->id, $type2));
                array_push($data, $dataItem);
            }

            $data2 = array();
            for($i=$start; $i>=$end; $i--){
                $dataItem2 = array('m' => getWeek($i), 'a' => productionWeek($i, $subject->id, $type3), 'b' => productionWeek($i, $subject->id, $type4));
                array_push($data2, $dataItem2);
            }

            $allData = array('data1' => $data, 'data2' => $data2);

            return $allData;

        }

        return view('admin.statistics.stats', compact('subject', 'workers', 'type', 'type2','type3', 'type4', 'production', 'total', 'weekDate', 'tableClass'));
    }

    public function workerStats($id)
    {
        $worker = Worker::find($id);
        $subject = $worker;
        $type = 2;
        $currYear = Carbon::now()->format('Y');
        $weekDate = date("Y-m-d", strtotime($currYear . "W" . getWeek(11)));
        $production = Workspace_production::where('worker_id', $id)->with('worker', 'workspace')->orderBy('updated_at', 'desc')->get();

        if(request()->has('test'))
        {
            $startWeekDate = date("Y-m-d", strtotime($currYear . "W" . request('start')));
            $endWeekDate = date("Y-m-d", strtotime($currYear . "W" . request('end')));

            $start = Carbon::parse($startWeekDate)->diffInWeeks(Carbon::now());
            $end = Carbon::parse($endWeekDate)->diffInWeeks(Carbon::now());

            $data = array();
            for($i=$start; $i>=$end; $i--){
                $dataItem = array('m' => getWeek($i), 'a' => productionWeek($i, $subject->id, $type));
                array_push($data, $dataItem);
            }

            return $data;
        }

        return view('admin.statistics.stats', compact('subject', 'type', 'production', 'weekDate'));
    }

    public function warehouseStats($id)
    {
        $warehouse = Warehouse::find($id);
        $subject = $warehouse;
        $type = 3;
        $type2 = 5;
        $type3 = 4;
        $type4 = 6;
        $currYear = Carbon::now()->format('Y');
        $weekDate = date("Y-m-d", strtotime($currYear . "W" . getWeek(11)));
        $production = Loader_to_warehouse::where('warehouse_id', $id)->with('worker', 'loader', 'workspace', 'warehouse')->finished()->orderBy('updated_at', 'desc')->get();

        if(request()->has('test'))
        {
            $startWeekDate = date("Y-m-d", strtotime($currYear . "W" . request('start')));
            $endWeekDate = date("Y-m-d", strtotime($currYear . "W" . request('end')));
            $type1 = request('type1');
            $type2 = request('type2');
            $type3 = request('type3');
            $type4 = request('type4');

            $start = Carbon::parse($startWeekDate)->diffInWeeks(Carbon::now());
            $end = Carbon::parse($endWeekDate)->diffInWeeks(Carbon::now());

            $data = array();
            for($i=$start; $i>=$end; $i--){
                $dataItem = array('m' => getWeek($i), 'a' => productionWeek($i, $subject->id, $type1), 'b' => productionWeek($i, $subject->id, $type2));
                array_push($data, $dataItem);
            }

            $data2 = array();
            for($i=$start; $i>=$end; $i--){
                $dataItem2 = array('m' => getWeek($i), 'a' => productionWeek($i, $subject->id, $type3), 'b' => productionWeek($i, $subject->id, $type4));
                array_push($data2, $dataItem2);
            }

            $allData = array('data1' => $data, 'data2' => $data2);

            return $allData;
        }
        return view('admin.statistics.stats', compact('subject', 'type', 'type2','type3', 'type4', 'production', 'total', 'weekDate'));
    }
}
