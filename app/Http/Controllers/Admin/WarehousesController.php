<?php

namespace App\Http\Controllers\Admin;

use App\Events\Board;
use App\Events\Factory;
use App\Models\Action;
use App\Traits\EventDataCreationTrait;
use Illuminate\Http\Request;
use App\Models\Loader_to_warehouse;
use App\Models\Product;
use App\Models\OrderWarehouse;
use App\Models\Ramp;
use App\Models\Warehouse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class WarehousesController extends Controller
{
    use EventDataCreationTrait;
    
    public function index()
    {
        $warehouses = Warehouse::get();

        $todays = Loader_to_warehouse::loadWarehouse()->whereRaw('Date(created_at) = CURDATE()')->get();
        $yesterdays = Loader_to_warehouse::loadWarehouse()->whereRaw("Date(created_at) = '" . Carbon::yesterday()->format('Y-m-d') . "'")->get();

        $thisMonthStart = Carbon::now()->startOfMonth()->format('Y-m-d');
        $thisMonthEnd = Carbon::now()->endOfMonth()->format('Y-m-d');
        $thisMonth = Loader_to_warehouse::loadWarehouse()->whereRaw("Date(created_at) >= '" . $thisMonthStart . "' and Date(created_at) <= '" . $thisMonthEnd . "'")->get();

        $lastMonthStart = Carbon::now()->startOfMonth()->subMonth()->format('Y-m-d');
        $lastMonthEnd = Carbon::now()->subMonth()->endOfMonth()->format('Y-m-d');
        $lastMonth = Loader_to_warehouse::loadWarehouse()->whereRaw("Date(created_at) >= '" . $lastMonthStart . "' and Date(created_at) <= '" . $lastMonthEnd . "'")->get();

        $ramps = Ramp::get();
        $loaders = Action::loaders()->get()->unique('loader_id');

        $orders = OrderWarehouse::with('order')->get();

        return view('admin.warehouses.index', compact('warehouses', 'todays', 'yesterdays', 'thisMonth', 'lastMonth', 'ramps', 'orders', 'loaders'));
    }

    public function create()
    {

        return view('admin.warehouses.create');
    }

    public function store()
    {

        $warehouse = new Warehouse();
        $warehouse->name = Input::get('name');
        $warehouse->space = Input::get('space');
        $warehouse->used = Input::get('used');
        $warehouse->reserved = Input::get('reserved');
        $warehouse->color = Input::get('favcolor');
        $warehouse->save();

        return redirect()->action('Admin\WarehousesController@index');
    }

    public function edit($id)
    {

        $warehouse = Warehouse::find($id);

        return view('admin.warehouses.edit', compact('warehouse'));
    }

    public function update($id)
    {

        $warehouse = Warehouse::find($id);
        $warehouse->name = Input::get('name');
        $warehouse->space = Input::get('space');
        $warehouse->used = Input::get('used');
        $warehouse->reserved = Input::get('reserved');
        $warehouse->color = Input::get('favcolor');
        $warehouse->save();

        return redirect()->action('Admin\WarehousesController@index');
    }

    public function drop($id)
    {

        $warehouse = Warehouse::find($id);
        $warehouse->delete();

        return redirect()->action('Admin\WarehousesController@index');
    }

    public function transfer(Request $request)
    {
        $orderWarehouseFrom = OrderWarehouse::where([['order_id', request('order_id')], ['warehouse_id', request('warehouse_from')]])->first();
        $this->validate($request, [
            'quantity' => 'required|numeric|max:' . $orderWarehouseFrom->quantity,
        ]);
        
        $user = Auth::user();

        $warehouse_from = Warehouse::find($request['warehouse_from']);
        $warehouse_from->update([
            'used' => $warehouse_from->used - $request['quantity']
        ]);
        
        if ($orderWarehouseFrom->quantity == request('quantity')) {
            $orderWarehouseFrom->delete();
        }else {
            $orderWarehouseFrom->update([
                'quantity' => $warehouse_from->used - $request['quantity']
            ]);
        }

        $warehouse_to = Warehouse::find($request['warehouse_id']);
        $warehouse_to->update([
            'reserved' => $warehouse_to->reserved + $request['quantity'],
        ]);

        $fillLoader = Loader_to_warehouse::create(array_merge([
            'volume' => $request['quantity'],
            'type' => 4,
        ], $request->all()
        ));

        $data = $this->createCallWarehouseToWarehouse($user, $fillLoader);

        event(new Factory($data));
        event(new Board($data));

        return response()->json(['message' => 'success'], 200, [], JSON_NUMERIC_CHECK);
    }

    public function createRamp()
    {
        return view('admin.warehouses.ramps.create');
    }

    public function storeRamp()
    {
        $ramp = new Ramp();
        $ramp->name = Input::get('name');
        $ramp->status = Input::get('status');
        $ramp->save();

        return redirect()->action('Admin\WarehousesController@index');
    }

    public function editRamp($id)
    {
        $ramp = Ramp::find($id);

        return view('admin.warehouses.ramps.edit', compact('ramp'));
    }

    public function updateRamp($id)
    {
        $ramp = Ramp::find($id);
        $ramp->name = Input::get('name');
        $ramp->status = Input::get('status');
        $ramp->save();

        return redirect()->action('Admin\WarehousesController@index');
    }

    public function dropRamp($id)
    {

        $ramp = Ramp::find($id);
        $ramp->delete();

        return redirect()->action('Admin\WarehousesController@index');
    }
}
