<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Setting;

class SettingsController extends Controller
{
    public function index()
    {
        $settings = Setting::first();
        $address = json_decode($settings->company_address, true);
        $emails = json_decode($settings->company_emails, true);

        return view('admin.settings.index', compact('settings', 'address', 'emails'));
    }

    public function update(Request $request)
    {
        $settings = Setting::first();
        $address = json_decode($settings->company_address, true);
        $emails = json_decode($settings->company_emails, true);


        foreach ($address as $key=>$value){
            $item[$key] = $request['company_address_' . $key];
            unset($request['company_address_' . $key]);
        }

        foreach ($emails as $key=>$value){
            $itemEmail[$key] = $request['company_email_' . $key];
            unset($request['company_email_' . $key]);
        }

        $settings->update(array_merge([
                        'company_address' => json_encode($item, true),
                        'company_emails' => json_encode($itemEmail, true)
                        ],
                        $request->all())
                    );

        return redirect()->action('Admin\SettingsController@index');
    }
}
