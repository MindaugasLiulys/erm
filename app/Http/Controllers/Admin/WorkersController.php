<?php

namespace App\Http\Controllers\Admin;

use App\Models\Setting;
use App\Models\Worker;
use App\Models\Position;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Auth\User;
use PDF;

class WorkersController extends Controller
{

    public function index()
    {
        $workers = Worker::with('position')->get();

        return view('admin.workers.index', compact('workers'));
    }

    protected function create()
    {
        $positions = Position::get();

        return view('admin.workers.create', compact('positions'));
    }

    public function store()
    {
        $worker = new Worker();
        $user = new User();

        $user->username = strtolower(substr($this->replace(request('surname')), 0, 3) .
            substr($this->replace(request('name')), 0, 3));
        $user->password = bcrypt(Input::get('password'));

        $user->save();

        $worker->user_id = $user->id;;
        $worker->name = Input::get('name');
        $worker->surname = Input::get('surname');
        $worker->position_id = Input::get('position_id');
        $worker->password = Input::get('password');

        $worker->save();

        return redirect()->action('Admin\WorkersController@index');
    }

    public function generatePdf($id)
    {
        $worker = Worker::find($id);
        $settings = Setting::first();
        $settingsAddress = json_decode($settings->company_address, true);
        $emails = json_decode($settings->company_emails, true);
        $pdfTitle = $worker->name . $worker->surname . '_logins.pdf';

        $pdf = PDF::loadView('admin.workers.worker-pdf', compact('worker', 'settings', 'settingsAddress', 'emails'));
        return $pdf->download($pdfTitle);
//        return view('admin.workers.worker-pdf', compact('worker', 'settings', 'settingsAddress', 'emails'));
    }

    public function generatePdfAll()
    {
        $workers = Worker::get();
        $settings = Setting::first();
        $settingsAddress = json_decode($settings->company_address, true);
        $emails = json_decode($settings->company_emails, true);

        $pdf = PDF::loadView('admin.workers.worker-pdf', compact('workers', 'settings', 'settingsAddress', 'emails'));
        return $pdf->download('all_workers_logins.pdf');
    }

    public function edit($id)
    {
        $worker = Worker::find($id);
        $positions = Position::get();

        return view('admin.workers.edit', compact('worker', 'positions'));
    }

    public function save($id)
    {
        $worker = Worker::find($id);

        $userId =$worker->user_id;
        $user = User::find($userId);

        $user->username = strtolower(substr($this->replace(request('surname')), 0, 3) .
            substr($this->replace(request('name')), 0, 3));
        $user->password = bcrypt(Input::get('password'));
        $user->save();

        $worker->name = Input::get('name');
        $worker->surname = Input::get('surname');
        $worker->position_id = Input::get('position_id');
        $worker->password = Input::get('password');

        $worker->save();

        return redirect()->action('Admin\WorkersController@index');
    }

    public function drop($id)
    {
        $worker = Worker::find($id);

        $user = User::where('id', $worker->user_id)->get()->first();

        $user->delete();
        $worker->delete();

        return redirect()->action('Admin\WorkersController@index');
    }

    public function replace($string)
    {
        $replace = [
            '&lt;' => '', '&gt;' => '', '&#039;' => '', '&amp;' => '',
            '&quot;' => '', 'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'Ae',
            '&Auml;' => 'A', 'Å' => 'A', 'Ā' => 'A', 'Ą' => 'A', 'Ă' => 'A', 'Æ' => 'Ae',
            'Ç' => 'C', 'Ć' => 'C', 'Č' => 'C', 'Ĉ' => 'C', 'Ċ' => 'C', 'Ď' => 'D', 'Đ' => 'D',
            'Ð' => 'D', 'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ē' => 'E',
            'Ę' => 'E', 'Ě' => 'E', 'Ĕ' => 'E', 'Ė' => 'E', 'Ĝ' => 'G', 'Ğ' => 'G',
            'Ġ' => 'G', 'Ģ' => 'G', 'Ĥ' => 'H', 'Ħ' => 'H', 'Ì' => 'I', 'Í' => 'I',
            'Î' => 'I', 'Ï' => 'I', 'Ī' => 'I', 'Ĩ' => 'I', 'Ĭ' => 'I', 'Į' => 'I',
            'İ' => 'I', 'Ĳ' => 'IJ', 'Ĵ' => 'J', 'Ķ' => 'K', 'Ł' => 'K', 'Ľ' => 'K',
            'Ĺ' => 'K', 'Ļ' => 'K', 'Ŀ' => 'K', 'Ñ' => 'N', 'Ń' => 'N', 'Ň' => 'N',
            'Ņ' => 'N', 'Ŋ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O',
            'Ö' => 'Oe', '&Ouml;' => 'Oe', 'Ø' => 'O', 'Ō' => 'O', 'Ő' => 'O', 'Ŏ' => 'O',
            'Œ' => 'OE', 'Ŕ' => 'R', 'Ř' => 'R', 'Ŗ' => 'R', 'Ś' => 'S', 'Š' => 'S',
            'Ş' => 'S', 'Ŝ' => 'S', 'Ș' => 'S', 'Ť' => 'T', 'Ţ' => 'T', 'Ŧ' => 'T',
            'Ț' => 'T', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'Ue', 'Ū' => 'U',
            '&Uuml;' => 'Ue', 'Ů' => 'U', 'Ű' => 'U', 'Ŭ' => 'U', 'Ũ' => 'U', 'Ų' => 'U',
            'Ŵ' => 'W', 'Ý' => 'Y', 'Ŷ' => 'Y', 'Ÿ' => 'Y', 'Ź' => 'Z', 'Ž' => 'Z',
            'Ż' => 'Z', 'Þ' => 'T', 'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a',
            'ä' => 'ae', '&auml;' => 'ae', 'å' => 'a', 'ā' => 'a', 'ą' => 'a', 'ă' => 'a',
            'æ' => 'ae', 'ç' => 'c', 'ć' => 'c', 'č' => 'c', 'ĉ' => 'c', 'ċ' => 'c',
            'ď' => 'd', 'đ' => 'd', 'ð' => 'd', 'è' => 'e', 'é' => 'e', 'ê' => 'e',
            'ë' => 'e', 'ē' => 'e', 'ę' => 'e', 'ě' => 'e', 'ĕ' => 'e', 'ė' => 'e',
            'ƒ' => 'f', 'ĝ' => 'g', 'ğ' => 'g', 'ġ' => 'g', 'ģ' => 'g', 'ĥ' => 'h',
            'ħ' => 'h', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i', 'ī' => 'i',
            'ĩ' => 'i', 'ĭ' => 'i', 'į' => 'i', 'ı' => 'i', 'ĳ' => 'ij', 'ĵ' => 'j',
            'ķ' => 'k', 'ĸ' => 'k', 'ł' => 'l', 'ľ' => 'l', 'ĺ' => 'l', 'ļ' => 'l',
            'ŀ' => 'l', 'ñ' => 'n', 'ń' => 'n', 'ň' => 'n', 'ņ' => 'n', 'ŉ' => 'n',
            'ŋ' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'oe',
            '&ouml;' => 'oe', 'ø' => 'o', 'ō' => 'o', 'ő' => 'o', 'ŏ' => 'o', 'œ' => 'oe',
            'ŕ' => 'r', 'ř' => 'r', 'ŗ' => 'r', 'š' => 's', 'ù' => 'u', 'ú' => 'u',
            'û' => 'u', 'ü' => 'ue', 'ū' => 'u', '&uuml;' => 'ue', 'ů' => 'u', 'ű' => 'u',
            'ŭ' => 'u', 'ũ' => 'u', 'ų' => 'u', 'ŵ' => 'w', 'ý' => 'y', 'ÿ' => 'y',
            'ŷ' => 'y', 'ž' => 'z', 'ż' => 'z', 'ź' => 'z', 'þ' => 't', 'ß' => 'ss',
            'ſ' => 'ss', 'ый' => 'iy', 'А' => 'A', 'Б' => 'B', 'В' => 'V', 'Г' => 'G',
            'Д' => 'D', 'Е' => 'E', 'Ё' => 'YO', 'Ж' => 'ZH', 'З' => 'Z', 'И' => 'I',
            'Й' => 'Y', 'К' => 'K', 'Л' => 'L', 'М' => 'M', 'Н' => 'N', 'О' => 'O',
            'П' => 'P', 'Р' => 'R', 'С' => 'S', 'Т' => 'T', 'У' => 'U', 'Ф' => 'F',
            'Х' => 'H', 'Ц' => 'C', 'Ч' => 'CH', 'Ш' => 'SH', 'Щ' => 'SCH', 'Ъ' => '',
            'Ы' => 'Y', 'Ь' => '', 'Э' => 'E', 'Ю' => 'YU', 'Я' => 'YA', 'а' => 'a',
            'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'yo',
            'ж' => 'zh', 'з' => 'z', 'и' => 'i', 'й' => 'y', 'к' => 'k', 'л' => 'l',
            'м' => 'm', 'н' => 'n', 'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's',
            'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c', 'ч' => 'ch',
            'ш' => 'sh', 'щ' => 'sch', 'ъ' => '', 'ы' => 'y', 'ь' => '', 'э' => 'e',
            'ю' => 'yu', 'я' => 'ya'
        ];

        return str_replace(array_keys($replace), $replace, $string);
    }

//    public function pdfhtml(){
//        $workers = Worker::get();
//        return view('admin.workers.worker-pdf', compact('workers'));
//    }

}
