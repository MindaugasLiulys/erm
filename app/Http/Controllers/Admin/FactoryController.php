<?php

namespace App\Http\Controllers\Admin;

use App\Events\Board;
use App\Events\Factory;
use App\Events\Tasks;
use App\Models\Action;
use App\Models\Loader_to_warehouse;
use App\Models\Order;
use App\Models\Product;
use App\Models\Product\Material;
use App\Models\OrderWarehouse;
use App\Models\Ramp;
use App\Models\Supervisor_call;
use App\Models\Task;
use App\Models\Worker;
use App\Models\Loader;
use App\Models\Warehouse;
use App\Models\Workspace;
use App\Http\Controllers\Controller;
use App\Models\Workspace_production;
use App\Traits\EventDataCreationTrait;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class FactoryController extends Controller
{
    use EventDataCreationTrait;

    public function index()
    {
        $workspaces = Workspace::get();
        $loaders = Loader::all();
        $warehouses = Warehouse::get();
        $workspaceWorkers = Worker::with('workspaces', 'position')->get();
        $loaderWorkers = Worker::with('loaderTo')->get();
        $action = Action::with('user')->orderBy('updated_at', 'desc')->get();
        $serialNo = 1;

        $materials = Material::get();
        $products = Product::get();
        $orders = Order::finished()->get();
        $ramps = Ramp::get();

        $monday = Workspace_production::whereRaw("Date(updated_at) = '" . Carbon::now()->startOfWeek()->format('Y-m-d') . "'")->get();
        $tuesday = Workspace_production::whereRaw("Date(updated_at) = '" . Carbon::now()->startOfWeek()->addDay(1)->format('Y-m-d') . "'")->get();
        $wednesday = Workspace_production::whereRaw("Date(updated_at) = '" . Carbon::now()->startOfWeek()->addDay(2)->format('Y-m-d') . "'")->get();
        $thursday = Workspace_production::whereRaw("Date(updated_at) = '" . Carbon::now()->startOfWeek()->addDay(3)->format('Y-m-d') . "'")->get();
        $friday = Workspace_production::whereRaw("Date(updated_at) = '" . Carbon::now()->startOfWeek()->addDay(4)->format('Y-m-d') . "'")->get();
        $saturday = Workspace_production::whereRaw("Date(updated_at) = '" . Carbon::now()->startOfWeek()->addDay(5)->format('Y-m-d') . "'")->get();
        $sunday = Workspace_production::whereRaw("Date(updated_at) = '" . Carbon::now()->startOfWeek()->addDay(6)->format('Y-m-d') . "'")->get();

        $mondayL = Loader_to_warehouse::whereRaw("Date(updated_at) = '" . Carbon::now()->startOfWeek()->format('Y-m-d') . "'")->get();
        $tuesdayL = Loader_to_warehouse::whereRaw("Date(updated_at) = '" . Carbon::now()->startOfWeek()->addDay(1)->format('Y-m-d') . "'")->get();
        $wednesdayL = Loader_to_warehouse::whereRaw("Date(updated_at) = '" . Carbon::now()->startOfWeek()->addDay(2)->format('Y-m-d') . "'")->get();
        $thursdayL = Loader_to_warehouse::whereRaw("Date(updated_at) = '" . Carbon::now()->startOfWeek()->addDay(3)->format('Y-m-d') . "'")->get();
        $fridayL = Loader_to_warehouse::whereRaw("Date(updated_at) = '" . Carbon::now()->startOfWeek()->addDay(4)->format('Y-m-d') . "'")->get();
        $saturdayL = Loader_to_warehouse::whereRaw("Date(updated_at) = '" . Carbon::now()->startOfWeek()->addDay(5)->format('Y-m-d') . "'")->get();
        $sundayL = Loader_to_warehouse::whereRaw("Date(updated_at) = '" . Carbon::now()->startOfWeek()->addDay(6)->format('Y-m-d') . "'")->get();

        return view('admin.factory.index', compact('workspaces', 'lodedValue', 'action', 'serialNo', 'workspaceWorkers', 'loaderWorkers', 'loaders', 'warehouses', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday', 'mondayL', 'tuesdayL', 'wednesdayL', 'thursdayL', 'fridayL', 'saturdayL', 'sundayL', 'products', 'materials', 'ramps', 'orders'));

    }

    public function callOrder(Request $request)
    {
        $max = OrderWarehouse::where([['order_id', request('order_id')], ['warehouse_id', request('warehouse')]])->first()->quantity;
        $this->validate($request, [
            'quantity' => 'required|numeric|max:' . $max,
            'order_id' => 'required',
            'ramp' => 'required',
            'warehouse' => 'required',
        ]);

        $order = Order::find(request('order_id'));
        $loader = Loader::find(request('loaderId'));
        $ramp = Ramp::find(request('ramp'));
        $quantity = request('quantity');
        $warehouse = Warehouse::find(request('warehouse'));
        $user = Auth::user();

        $fillLoader = Loader_to_warehouse::create([
            'worker_id' => 0,
            'loader_id' => $loader->id,
            'ramp_id' => $ramp->id,
            'warehouse_id' => $warehouse->id,
            'volume' => $quantity,
            'order_id' => $order->id,
            'material_id' => 0,
            'status' => 0,
            'type' => 2
        ]);

        $data = $this->createDataArrayForCallLoaderManager($user, $fillLoader, $warehouse);

        event(new Factory($data));
        event(new Board($data));

        return response()->json(['message' => 'success'], 200, [], JSON_NUMERIC_CHECK);
    }

    public function callMaterial(Request $request)
    {
        $material = Material::find(request('material'));

        $this->validate($request, [
            'quantity' => 'required|numeric|max:' . $material->quantity,
        ]);

        $loader = Loader::find(request('loaderId'));
        $ramp = Ramp::find(request('ramp'));
        $quantity = request('quantity');
        $user = Auth::user();

        $fillLoader = Loader_to_warehouse::create([
            'worker_id' => 0,
            'material_id' => $material->id,
            'warehouse_id' => $material->warehouse->id,
            'loader_id' => $loader->id,
            'ramp_id' => $ramp->id,
            'volume' => $quantity,
            'order_id' => 0,
            'status' => 0,
            'type' => 3
        ]);

        $data = $this->createDataArrayForCallMaterialManager($user, $fillLoader, $material);

        event(new Factory($data));
        event(new Board($data));

        return response()->json(['message' => 'success'], 200, [], JSON_NUMERIC_CHECK);
    }

    public function callAdditional(Request $request)
    {
        $task = Task::create($request->all());

        event(new Tasks($task));

        return response()->json(['message' => 'success'], 200, [], JSON_NUMERIC_CHECK);
    }

    public function getAvailableWarehouses()
    {
        $warehouses = Warehouse::whereIn('id', OrderWarehouse::where('order_id', request('order_id'))->pluck('warehouse_id')->toArray())->get();

        return response()->json(['warehouses' => $warehouses->pluck('id', 'name')->toArray()], 200, [], JSON_NUMERIC_CHECK);
    }

    public function callManagerFinish()
    {
        $workspace = Workspace::find(request('workspace_id'));

        Supervisor_call::called()->toWorkspace()
            ->where('workspace_id', $workspace->id)->update([
                'status' => 1
            ]);

        $dataResponse = [
            'success' => true
        ];

        return response()->json(['data' => array($dataResponse)]);
    }
}
