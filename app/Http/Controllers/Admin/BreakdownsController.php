<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Breakdown;
use App\Models\Loader;
use App\Models\Workspace;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;

class BreakdownsController extends Controller
{

    public function index()
    {
        $breakdowns5 = Breakdown::with('breakdownObject')->take(5)->get();
        $breakdowns = Breakdown::with('breakdownObject')->get();
        $activeNow = Carbon::now();

        $monthsBreakdown = Breakdown::whereMonth('created_at', '=', date('m'))->get();

        return view('admin.breakdowns.index', compact('breakdowns', 'breakdowns5', 'activeNow', 'monthsBreakdown'));
    }

    public function create()
    {
        $workspaces = Workspace::get();
        $loaders = Loader::get();

        return view('admin.breakdowns.create', compact('callObjects', 'workspaces', 'loaders'));
    }

    public function store(Request $request)
    {
        Breakdown::create(
            $request->all()
        );

        return redirect()->action('Admin\BreakdownsController@index');
    }
}
