<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Loader_to_warehouse;
use App\Models\Warehouse;
use App\Models\Worker;
use App\Models\Workspace_production;
use Carbon\Carbon;

class AdminController extends Controller
{
    public function index()
    {
        $workers = Worker::get();

        $warehouses = Warehouse::get();
        $warehouseUsed = 0;
        $warehouseTotal = 0;
        foreach($warehouses as $warehouse){
            $warehouseUsed += $warehouse->used;
            $warehouseTotal += $warehouse->space;
        }

        $warehouseOccup = getPercent($warehouseTotal, $warehouseUsed);

        $dataProd = array();
        $workersProd = Workspace_production::whereDate("created_at", ">", Carbon::now()->startOfMonth())->groupBy('worker_id')->take(3)->get();
        foreach ($workersProd as $prod) {
            $total = Workspace_production::whereDate("created_at", ">", Carbon::now()->startOfMonth())->where('worker_id', $prod->worker_id)->get();
            array_push($dataProd,[
                'produced' => $total->reduce(function ($carry, $item) {
                    return $carry + $item->unloaded - $item->day_start + $item->day_end;
                }),
               'name' => $prod->worker->name,
               'workspace' => $prod->workspace->name,
            ]);
        }
        arsort($dataProd);

        $loadDataProd = array();
        $loadersProd = Loader_to_warehouse::loadWarehouse()->whereDate("created_at", ">", Carbon::now()->startOfMonth())->groupBy('loader_id')->take(3)->get();
        foreach ($loadersProd as $prod) {
            $total = Loader_to_warehouse::whereDate("created_at", ">", Carbon::now()->startOfMonth())->where('loader_id', $prod->loader_id)->where('status', 2)->get();
            array_push($loadDataProd,[
                'unloaded' => $total->reduce(function ($carry, $item) {
                    return $carry + $item->volume;
                }),
                'name' => $prod->loader->name
            ]);
        }
        arsort($loadDataProd);

        $workspaces = Workspace_production::whereDate("created_at", ">", Carbon::now()->startOfMonth())->get();

        $items = array();
        foreach($workspaces as $effWorkspace){
            if (array_key_exists($effWorkspace->workspace_id, $items)){
                $items[$effWorkspace->workspace_id]['produced'] += intval($effWorkspace->unloaded - $effWorkspace->day_start + $effWorkspace->day_end);
            }else{
                $items[$effWorkspace->workspace_id]['produced'] = intval($effWorkspace->unloaded - $effWorkspace->day_start + $effWorkspace->day_end);
            }

        $items[$effWorkspace->workspace_id]['name'] = $effWorkspace->workspace->name;
        }

        if(!empty($items)){
            $highest = array_search(max($items),$items);
            $lowest = array_search(min($items),$items);
        }

        return view('admin.index', compact('workers', 'warehouseUsed', 'warehouses', 'workersProd', 'loadersProd', 'highest', 'items', 'lowest', 'warehouseOccup', 'dataProd', 'loadDataProd'));
    }
}
