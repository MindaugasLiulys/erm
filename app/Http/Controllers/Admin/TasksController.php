<?php

namespace App\Http\Controllers\Admin;

use App\Models\Loader;
use App\Http\Controllers\Controller;
use App\Models\Task;
use App\Models\Worker;
use App\Models\Workspace;
use App\Events\Tasks;
use Illuminate\Http\Request;

class TasksController extends Controller
{
    public function index()
    {
        $workspaces = Workspace::get();
        $loaders = Loader::get();
        $workers = Worker::with('workspaces')->get();
        $tasks = Task::get();

        return view('admin.tasks.index', compact('workspaces', 'workers', 'tasks', 'loaders'));
    }

    public function createTask(Request $request)
    {
        $task = Task::create($request->all());
        
        event(new Tasks($task));
        
        return redirect()->action('Admin\TasksController@index');
    }
}
