<?php
namespace App\Http\Controllers\Worker;

use App\Events\Login;
use App\Models\Action;
use App\Models\Loader;
use App\Models\Loader_to_warehouse;
use App\Models\Warehouse;
use App\Models\Worker;
use App\Events\Board;
use App\Events\Factory;
use App\Http\Controllers\Controller;
use App\Models\Worker_to_workspace;
use App\Models\Workspace;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class WorkerHomeController extends Controller
{
    public $actionName;
    public $actionStatus;

    public function __construct()
    {
        $this->actionName['loaderCalled'] = 'Krautuvas iškviestas';
    }

    public function index()
    {
        $workspace = Worker_to_workspace::where('worker_id', Auth::user()->worker->id)->first();
        $workers = Worker_to_workspace::where('workspace_id', $workspace->workspace_id)->get();
        $type = 0;

        $worker = Auth::user()->worker;
        $user_id = Auth::user()->id;

        $data = [
            'workerUserId'        => $user_id,
            'status'              => 'login'
        ];

        event(new Login($data));

        return view('worker.index', compact('workers', 'worker', 'workspace', 'type'));
    }

    public function jobList()
    {
        $worker = Auth::user()->worker;
        return response()->json(['data' => $worker], 200, [], JSON_NUMERIC_CHECK);
    }
}