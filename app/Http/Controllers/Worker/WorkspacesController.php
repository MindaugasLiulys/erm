<?php

namespace App\Http\Controllers\Worker;

use App\Events\Board;
use App\Events\Factory;
use App\Models\Action;
use App\Models\Workspace_production;
use App\Models\Loader;
use App\Models\Warehouse;
use App\Models\Worker;
use App\Models\Worker_to_workspace;
use App\Models\Workspace;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Loader_to_warehouse;

class WorkspacesController extends Controller
{
    public function index()
    {
        $warehouses = Warehouse::all();
        $workspace = Auth::user()->worker->workspaces->first();
        $workers = $workspace->workers;
        $day_start_production = Workspace_production::where('workspace_id', $workspace->id)->get()
            ->last(function ($value, $key) {
                return $value->created_at < Carbon::today();
            });
        $day_start = isset($day_start_production) ? $day_start_production->day_start : 0;
        $loaders = Loader::get();
        $loaderWorkers = Worker::with('loaderTo')->get();

        $products = $workspace->products()->get();

        return view('worker.workspaces.index', compact('warehouses', 'workers', 'loaders', 'loaderWorkers', 'workspace', 'products', 'day_start'));
    }

    public function saveQuantity($id)
    {
        $todays = Loader_to_warehouse::loadWarehouse()->whereRaw('Date(created_at) = CURDATE()')->where('worker_id', Auth::user()->worker->id)->get();
        $todaySVol = 0;
        foreach ($todays as $today) {
            $todaySVol += $today->volume;
        }

        $currDate = Carbon::now()->format('Y-m-d');
        $lastRecord = Workspace_production::where('worker_id', Auth::user()->worker->id)->orderBy('updated_at', 'desc')->first();

        if ($lastRecord != null && $lastRecord->updated_at->format('Y-m-d') == $currDate) {
            $quantities = Workspace_production::where('worker_id', Auth::user()->worker->id)->find($lastRecord->id);
        } else {
            $quantities = new Workspace_production();
        }

        $quantities->worker_id = Auth::user()->worker->id;
        $quantities->workspace_id = $id;
        $quantities->product_id = Input::get('product_id');
        $quantities->day_start = Input::get('day_start');
        $quantities->day_end = Input::get('day_end');
        $quantities->unloaded = $todaySVol;

        $todayDay = Carbon::now()->dayOfWeek;

        $data = [
            'graphStatus' => 1,
            'workspaceId' => $id,
            'produced' => $quantities->unloaded + $quantities->day_end - $quantities->day_start,
            'todayDay' => $todayDay,
        ];

        event(new Factory($data));

        if (!empty($quantities->product_id) && !empty($quantities->day_start) && !empty($quantities->day_end)) {
            $quantities->save();
            return redirect()->back()->with('message', trans('site.saved successfully'))->with('class', 'success');
        } else {
            return redirect()->back()->with('message', trans('site.missing information'))->with('class', 'danger');
        }

    }

    public function fillLoader($id)
    {
        $warehouse = Warehouse::find($id);

        $warehouse->reserved = $warehouse->reserved + Input::get('volume');
        $warehouse->save();

        $warehouse_free = $warehouse->space - $warehouse->used - $warehouse->reserved;

        $fillLoader = new Loader_to_warehouse();

        $fillLoader->worker_id = Auth::user()->worker->id;
        $fillLoader->loader_id = Input::get('loader_id');
        $fillLoader->workspace_id = Input::get('workspace_id');
        $fillLoader->warehouse_id = $id;
        $fillLoader->volume = Input::get('volume');
        $fillLoader->status = 0;
        $fillLoader->save();

        $user_id = Auth::user()->id;

        $action = new Action();
        $action->user_id = $user_id;
        $action->loader_id = Input::get('loader_id');
        $action->action_name = trans('site.Waiting to accept');
        $action->actions_status = 2;
        $action->save();

        $currWorkspace = Worker_to_workspace::where('worker_id', Auth::user()->worker->id)->first();
        $inWorkspace = Workspace::where('id', $currWorkspace->workspace_id)->first();

        $loader = Loader::find($action->loader_id);

        $data = [
            'warehouse_id' => $fillLoader->warehouse_id,
            'warehouseReserved' => $warehouse->reserved,
            'warehouse_perc' => getPercent($warehouse->space, $warehouse->used),
            'warehouse_free' => $warehouse_free,
            'warehouse_perc_res' => getPercent($warehouse->space, $warehouse->reserved),
            'user_id' => $user_id,
            'loader_id' => $action->loader_id,
            'loader_name' => $loader->name,
            'name' => Auth::user()->worker->name,
            'surname' => Auth::user()->worker->surname,
            'workplace' => $inWorkspace->name,
            'workspace_id' => $inWorkspace->id,
            'action_name' => $action->action_name,
            'actions_status' => trans('site.Waiting to accept'),
            'time' => $action->created_at->format('H:i:s Y-m-d '),
            'event_id' => $action->id,
            'action_status_type' => $action->actions_status,
            'class' => 'btn btn-warning btn-xs',
            'iconClass' => 'fa fa-cog fa-spin fa-fw',
            'loader_class' => 'orange',
            'status' => '0',
        ];

        event(new Board($data));
        event(new Factory($data));

        return redirect()->back();

    }

}