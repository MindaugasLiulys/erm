<?php

namespace App\Http\Controllers\Auth;

use App\Events\Login;
use App\Http\Controllers\Controller;
use App\Http\Middleware\Worker;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
//    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    protected function authenticated(Request $request, $user)
    {
        if (Auth::user()->isAdmin()){
            return redirect()->action('Admin\AdminController@index');
        }elseif (Auth::user()->isWorker()){
            return redirect()->action('Worker\WorkerHomeController@index');
        }else{
            return redirect()->intended('/');
        }
    }

    protected function redirectTo()
    {
        if (Auth::user()->isWorker())
        {
            return '/worker';
        }
        return '/';
    }

    protected function credentials(Request $request)
    {
        $field = filter_var($request->get($this->username()), FILTER_VALIDATE_EMAIL)
            ? $this->username()
            : 'username';

        return [
            $field     => $request->get($this->username()),
            'password' => $request->password,
        ];
    }

    public function logout(Request $request)
    {
        $user_id = Auth::user()->id;
        $loader_id = null;
        if(!empty(Auth::user()->worker->loader[0]->id) != 0){
            $loader_id = Auth::user()->worker->loader->id;
            dd($loader_id);
        }

        $data = [
            'workerUserId'        => $user_id,
            'status'              => 'logout',
            'loaderId'            => $loader_id,
        ];

        event(new Login($data));

        $this->guard()->logout();
        return redirect('/');
    }

}
