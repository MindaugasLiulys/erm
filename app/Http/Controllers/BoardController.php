<?php
namespace App\Http\Controllers;
//npm install express ioredis socket.io --save
//https://laracasts.com/discuss/channels/general-discussion/step-by-step-guide-to-installing-socketio-and-broadcasting-events-with-laravel-51

use App\Models\Action;

class BoardController extends Controller
{
    public function index()
    {
        $action = Action::with('user')->orderBy('created_at', 'desc')->get();

        return view('board', compact('action'));
    }

}