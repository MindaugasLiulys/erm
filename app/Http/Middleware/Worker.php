<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class Worker
{
    public function handle($request, Closure $next)
    {

        if ( Auth::check() && Auth::user()->isWorker())
        {
            return $next($request);
        }

        return Redirect::back();

    }
}
