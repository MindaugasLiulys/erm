<?php

namespace App\Traits;

use App\Events\Board;
use App\Events\Factory;
use App\Models\Action;
use App\Models\Loader;
use App\Models\ProductWarehouse;
use App\Models\Warehouse;
use Illuminate\Support\Facades\Auth;

trait EventDataCreationTrait
{
    public function createDataArrayForStartJob($user, $job)
    {
        if ($job->type == 0) {
            $workspace = $job->workspace;
            $action = $this->createAction($user, $job->loader_id, trans('site.loading_workspace_text',
                ['product' => $workspace->current_produced_product_name, 'workspace' => $workspace->name]),
                getAction('working')['status']);
        } else if ($job->type == 2) {
            $action = $this->createAction($user, $job->loader_id, trans('site.loading_workspace_text',
                ['product' => $job->product_name, 'workspace' => $job->ramp_name]),
                getAction('working')['status']);
        } else if ($job->type == 1) {
            $workspace = $job->workspace;
            $action = $this->createAction($user, $job->loader_id, trans('site.unloading_workspace_text',
                ['material' => $job->material->name, 'workspace' => $workspace->name]),
                getAction('working')['status']);
        } else if ($job->type == 3) {
            $action = $this->createAction($user, $job->loader_id, trans('site.unloading_workspace_text',
                ['material' => $job->material->name, 'workspace' => $job->warehouse->name]),
                getAction('working')['status']);
        } else {
            $action = $this->createAction($user, $job->loader_id, trans('site.warehouse_warehouse_text',
                ['warehouse' => $job->warehouseFrom->name, 'another' => $job->warehouse->name]),
                getAction('working')['status']);
        }

        $data = [
            'job_id' => $job->id,
            'workspace_id' => isset($workspace) ? $workspace->id : 0,
            'user_id' => $user->id,
            'loader_id' => $action->loader_id,
            'name' => $user->worker->name,
            'surname' => $user->worker->surname,
            'action_name' => $action->action_name,
            'actions_status' => $action->action_name,
            'time' => $action->created_at->format('H:i:s Y-m-d'),
            'event_id' => $action->id,
            'action_status_type' => getAction('working')['status'],
            'class' => getAction('working')['class'],
            'iconClass' => getAction('working')['i'],
            'loader_class' => getAction('working')['span'],
        ];

        return $data;
    }

    public function createDataArrayForCallLoader($user, $job)
    {
        $warehouse = Warehouse::find(request('warehouse_id'));

        $warehouse->reserved = $warehouse->reserved + request('volume');
        $warehouse->save();

        $warehouse_free = $warehouse->space - $warehouse->used - $warehouse->reserved;

        $action = $this->createAction($user, $job->loader_id, getAction('accept')['message'],
            getAction('accept')['status']);

        $workspace = Auth::guard('api')->user()->worker->workspaces->first();
        $loader = Loader::find($action->loader_id);

        $data = [
            'warehouse_id' => $job->warehouse_id,
            'warehouseReserved' => $warehouse->reserved,
            'warehouse_perc' => getPercent($warehouse->space, $warehouse->used),
            'warehouse_free' => $warehouse_free,
            'warehouse_perc_res' => getPercent($warehouse->space, $warehouse->reserved),
            'user_id' => $user->id,
            'loader_id' => $action->loader_id,
            'loader_name' => $loader->name,
            'name' => $user->worker->name,
            'surname' => $user->worker->surname,
            'workplace' => $workspace->name,
            'action_name' => getAction('accept')['message'],
            'actions_status' => getAction('accept')['message'],
            'time' => $action->created_at->format('H:i:s Y-m-d '),
            'event_id' => $action->id,
            'action_status_type' => getAction('accept')['status'],
            'class' => getAction('accept')['class'],
            'iconClass' => getAction('accept')['i'],
            'loader_class' => getAction('accept')['span'],
            'status' => '0',
        ];

        return $data;
    }

    public function createDataArrayForStartJobLater($user, $job)
    {
        $action = $this->createAction($user, $job->loader_id, trans('site.working_later',
            ['time' => $job->getStartAtTime()]), 1);

        $data = [
            'job_id' => $job->id,
            'user_id' => $user->id,
            'workspace_id' => isset($workspace) ? $workspace->id : 0,
            'loader_id' => $action->loader_id,
            'name' => $user->worker->name,
            'surname' => $user->worker->surname,
            'action_name' => $action->action_name,
            'actions_status' => $action->action_name,
            'time' => $action->created_at->format('H:i:s Y - m - d '),
            'event_id' => $action->id,
            'action_status_type' => getAction('accept')['status'],
            'class' => getAction('accept')['class'],
            'iconClass' => getAction('accept')['i'],
            'loader_class' => getAction('accept')['span'],
        ];

        return $data;
    }

    public function createDataArrayForEndJob($user, $job)
    {
        $dataWarehouse = array();
        if ($job->type == 0) {
            $warehouse = Warehouse::find($job->warehouse_id);

            $warehouse->reserved = $warehouse->reserved - $job->volume;
            $warehouse->used = $warehouse->used + $job->volume;
            $warehouse->save();

            $warehouse_free = $warehouse->space - $warehouse->used;

            $dataWarehouse = [
                'warehouse_id' => $warehouse->id,
                'warehouseReserved' => $warehouse->reserved,
                'warehouse_perc' => getPercent($warehouse->space, $warehouse->used),
                'warehouse_free' => $warehouse_free,
                'warehouse_used' => $warehouse->used,
                'warehouse_perc_res' => getPercent($warehouse->space, $warehouse->reserved),
            ];
        } else if ($job->type == 2) {
            $warehouse = Warehouse::find($job->warehouse_id);

            $warehouse->used = $warehouse->used - $job->volume;
            $warehouse->save();

            $warehouse_free = $warehouse->space - $warehouse->used;

            $dataWarehouse = [
                'warehouse_id' => $warehouse->id,
                'warehouseReserved' => $warehouse->reserved,
                'warehouse_perc' => getPercent($warehouse->space, $warehouse->used),
                'warehouse_free' => $warehouse_free,
                'warehouse_used' => $warehouse->used,
                'warehouse_perc_res' => getPercent($warehouse->space, $warehouse->reserved),
            ];
        } else if ($job->type == 4) {
            $warehouse = Warehouse::find($job->warehouse_id);

            $warehouse->used = $warehouse->used + $job->volume;
            $warehouse->reserved = $warehouse->reserved - $job->volume;
            $warehouse->save();

            $warehouse_free = $warehouse->space - $warehouse->used;

            $dataWarehouse = [
                'warehouse_id' => $warehouse->id,
                'warehouseReserved' => $warehouse->reserved,
                'warehouse_perc' => getPercent($warehouse->space, $warehouse->used),
                'warehouse_free' => $warehouse_free,
                'warehouse_used' => $warehouse->used,
                'warehouse_perc_res' => getPercent($warehouse->space, $warehouse->reserved),
            ];
        }

        $action = $this->createAction($user, $job->loader_id, getAction('waiting')['message'],
            getAction('waiting')['status']);

        $data = array_merge([
            'job_id' => $job->id,
            'workspace_id' => $job->workspace_id,
            'user_id' => $user->id,
            'loader_id' => $action->loader_id,
            'loader_name' => $job->loader->name,
            'name' => $user->worker->name,
            'surname' => $user->worker->surname,
            'action_name' => getAction('waiting')['message'],
            'actions_status' => getAction('waiting')['message'],
            'time' => $action->created_at->format('H:i:s Y-m-d'),
            'event_id' => $action->id,
            'action_status_type' => getAction('waiting')['status'],
            'class' => getAction('waiting')['class'],
            'iconClass' => getAction('waiting')['i'],
            'loader_class' => getAction('waiting')['span'],
            'status' => 2
        ], $dataWarehouse);

        return $data;
    }

    public function createDataArrayForLoaderFixed($user, $loader, $action_attr)
    {
        $action = $this->createAction($user, $loader->id, $action_attr['message'], $action_attr['status']);

        $data = [
            'user_id' => $user->id,
            'loader_id' => $action->loader_id,
            'loader_name' => $loader->name,
            'name' => $user->worker->name,
            'surname' => $user->worker->surname,
            'action_name' => $action_attr['message'],
            'actions_status' => $action_attr['message'],
            'time' => $action->created_at->format('H:i:s Y-m-d'),
            'event_id' => $action->id,
            'action_status_type' => $action_attr['status'],
            'class' => $action_attr['class'],
            'iconClass' => $action_attr['i'],
            'loader_class' => $action_attr['span'],
            'status' => 2,
        ];

        return $data;
    }

    public function createDataArrayForBreak($user, $loader)
    {
        $action = new Action();
        $action->user_id = $user->id;
        $action->loader_id = $loader->id;

        $action->action_name = $loader->status == 2 ? trans('site.Break') : trans('site.Waiting');
        $action->actions_status = $loader->status == 2 ? 3 : 0;
        $action->save();

        $data = [
            'user_id' => $user->id,
            'loader_id' => $action->loader_id,
            'loader_name' => $loader->name,
            'name' => $user->worker->name,
            'surname' => $user->worker->surname,
            'action_name' => $action->action_name,
            'actions_status' => $action->action_name,
            'time' => $action->created_at->format('H:i:s Y-m-d '),
            'event_id' => $action->id,
            'action_status_type' => $action->actions_status,
            'class' => 'btn btn-success btn-xs',
            'iconClass' => 'fa fa-coffee',
            'loader_class' => 'green'
        ];

        return $data;
    }

    public function createDataArrayForBreakWorkspace($user, $workspace)
    {
        $action = new Action();
        $action->user_id = $user->id;
        $action->loader_id = 0;

        $action->action_name = $workspace->status == 2 ? trans('site.Workspace broke') : trans('site.Workspace fixed');
        $action->actions_status = $workspace->status == 2 ? 9 : 0;
        $action->save();

        $data = [
            'user_id' => $user->id,
            'name' => $user->worker->name,
            'surname' => $user->worker->surname,
            'action_name' => $action->action_name,
            'actions_status' => $action->action_name,
            'time' => $action->created_at->format('H:i:s Y-m-d '),
            'event_id' => $action->id,
            'action_status_type' => $action->actions_status,
            'class' => 'btn btn-success btn-xs',
            'iconClass' => 'fa fa-coffee',
            'loader_class' => 'green'
        ];

        return $data;
    }

    public function createDataArrayForCallMaterial($user, $job, $material)
    {
        $action = $this->createAction($user, $job->loader_id, getAction('accept')['message'],
            getAction('accept')['status']);

        $workspace = Auth::guard('api')->user()->worker->workspaces->first();

        $loader = Loader::find($action->loader_id);

        $data = [
            'product_name' => $material->name,
            'user_id' => $user->id,
            'loader_id' => $action->loader_id,
            'loader_name' => $loader->name,
            'name' => $user->worker->name,
            'surname' => $user->worker->surname,
            'workplace' => $workspace->name,
            'action_name' => getAction('accept')['message'],
            'actions_status' => getAction('accept')['message'],
            'time' => $action->created_at->format('H:i:s Y-m-d '),
            'event_id' => $action->id,
            'action_status_type' => getAction('accept')['status'],
            'class' => getAction('accept')['class'],
            'iconClass' => getAction('accept')['i'],
            'loader_class' => getAction('accept')['span'],
            'status' => '0',
        ];

        return $data;
    }

    public function createDataArrayForCallMaterialManager($user, $job, $material)
    {
        $action = $this->createAction($user, $job->loader_id, getAction('accept')['message'],
            getAction('accept')['status']);

        $loader = Loader::find($action->loader_id);

        $data = [
            'product_name' => $material->name,
            'user_id' => $user->id,
            'loader_id' => $action->loader_id,
            'loader_name' => $loader->name,
            'name' => 'admin',
            'surname' => 'admin',
            'workplace' => $job->ramp->name,
            'action_name' => getAction('accept')['message'],
            'actions_status' => getAction('accept')['message'],
            'time' => $action->created_at->format('H:i:s Y-m-d '),
            'event_id' => $action->id,
            'action_status_type' => getAction('accept')['status'],
            'class' => getAction('accept')['class'],
            'iconClass' => getAction('accept')['i'],
            'loader_class' => getAction('accept')['span'],
            'status' => '0',
        ];

        return $data;
    }

    public function createDataArrayForCallLoaderManager($user, $job, $warehouse)
    {
        $warehouse->reserved = $warehouse->reserved + request('volume');
        $warehouse->save();

        $warehouse_free = $warehouse->space - $warehouse->used - $warehouse->reserved;

        $action = $this->createAction($user, $job->loader_id, getAction('accept')['message'],
            getAction('accept')['status']);

        $loader = Loader::find($action->loader_id);

        $data = [
            'warehouse_id' => $job->warehouse_id,
            'warehouseReserved' => $warehouse->reserved,
            'warehouse_perc' => getPercent($warehouse->space, $warehouse->used),
            'warehouse_free' => $warehouse_free,
            'warehouse_perc_res' => getPercent($warehouse->space, $warehouse->reserved),
            'user_id' => $user->id,
            'loader_id' => $action->loader_id,
            'loader_name' => $loader->name,
            'name' => 'admin',
            'surname' => 'admin',
            'workplace' => $job->ramp_name,
            'action_name' => getAction('accept')['message'],
            'actions_status' => getAction('accept')['message'],
            'time' => $action->created_at->format('H:i:s Y-m-d '),
            'event_id' => $action->id,
            'action_status_type' => getAction('accept')['status'],
            'class' => getAction('accept')['class'],
            'iconClass' => getAction('accept')['i'],
            'loader_class' => getAction('accept')['span'],
            'status' => '0',
        ];

        return $data;
    }

    public function createCallWarehouseToWarehouse($user, $job)
    {
        $action = $this->createAction($user, $job->loader_id, getAction('accept')['message'],
            getAction('accept')['status']);

        $loader = Loader::find($action->loader_id);

        $data = [
            'user_id' => $user->id,
            'loader_id' => $action->loader_id,
            'loader_name' => $loader->name,
            'name' => 'admin',
            'surname' => 'admin',
            'workplace' => $job->ramp_name,
            'action_name' => getAction('accept')['message'],
            'actions_status' => getAction('accept')['message'],
            'time' => $action->created_at->format('H:i:s Y-m-d '),
            'event_id' => $action->id,
            'action_status_type' => getAction('accept')['status'],
            'class' => getAction('accept')['class'],
            'iconClass' => getAction('accept')['i'],
            'loader_class' => getAction('accept')['span'],
            'status' => '0',
        ];

        return $data;
    }

    protected function createAction($user, $loader_id, $action_name, $status)
    {
        $action = new Action();
        $action->user_id = $user->id;
        $action->loader_id = $loader_id;

        $action->action_name = $action_name;
        $action->actions_status = $status;
        $action->save();
        return $action;
    }
}
