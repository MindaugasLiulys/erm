@extends('layout')
@section('main_container')

    <!-- page content -->
    <div role="main">
        <div class="col-md-12" style="background: #ffffff;">
            <div class="x_panel" style="border: none;">
                <div class="x_title">
                    <h2>Board</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <!-- start project list -->
                    <table class="table table-striped projects">
                        <thead>
                            <tr>
                                <th style="width: 5%">{{trans('site.No.')}}</th>
                                <th style="width: 10%">{{trans('site.loader')}}</th>
                                <th style="width: 20%">{{trans('site.workspace')}}</th>
                                <th>{{trans('site.worker')}}</th>
                                {{--<th>{{trans('site.action')}}</th>--}}
                                <th>{{trans('site.status')}}</th>
                                <th style="width: 20%">{{trans('site.time')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($action as $act)
                            <tr>
                                <td class="actionName" id="{{$act->id}}">
                                   <span>{{$act->id}}</span>
                                </td>
                                @if($act->loader_id != 0)
                                    <td class="loader" id="{{$act->loader_id}}">
                                       {{$act->loader->name}}
                                    </td>
                                @else
                                    <td class="loader" id="0">
                                        {{trans('site.Not selected')}}
                                    </td>
                                @endif
                                <td class="workplace">
                                    <a>{{ count($act->user->worker) > 0 ?
                                            isset($act->user->worker->workspaces->first()->name) ?
                                             $act->user->worker->workspaces->first()->name : '' : ''}}</a>
                                    <br>
                                </td>
                                <td class="nameSurname">
                                    @if(isset($act->user->worker->name))
                                        {{$act->user->worker->name}} {{$act->user->worker->surname}}
                                    @else
                                        {{$act->userusername}}
                                    @endif
                                </td>
                                {{--<td class="action_name">--}}
                                  {{--{{$act->action_name}}--}}
                                {{--</td>--}}
                                <td class="status">
                                     <button type="button" class="{{ getActionByStatus($act->actions_status)['class'] }}">
                                              {{ getActionByStatus($act->actions_status)['message'] }}</button>
                                </td>
                                <td class="time">
                                    {{$act->created_at->format('H:i:s Y-m-d ')}}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <!-- end project list -->
                </div>
            </div>
        </div>
    </div>

    <!-- /page content -->
@endsection

@section('footer_scripts')
    @if(env("APP_ENV") == 'local')
        <script src="http://localhost:3000/socket.io/socket.io.js"></script>
    @else
        <script src="http://erm.procreo.eu:3000/socket.io/socket.io.js"></script>
    @endif
    <script>
        if('{{ env('APP_ENV') }}' == 'local') {
            var socket = io('http://localhost:3000');
        }else{
            var socket = io('http://erm.procreo.eu:3000');
        }
        var html_insert = $('.table tbody tr').first().clone();
        socket.on("get-loader:App\\Events\\Board", function (message) {
            html_insert.find('.actionName span').html(message.data.event_id);
            html_insert.find('.action_name').html(message.data.action_name);
            html_insert.find('.actionName').prop('id', message.data.event_id);
            html_insert.find('.workplace').html(message.data.workplace);
            html_insert.find('.loader').html(message.data.loader_name);
            html_insert.find('.nameSurname').html(message.data.name + ' ' + message.data.surname);
            html_insert.find('.status button').removeClass().addClass(message.data.class).html(message.data.actions_status);
            html_insert.find('.time').html(message.data.time);
            $('table tbody').prepend(html_insert);
        });

    </script>
@endsection