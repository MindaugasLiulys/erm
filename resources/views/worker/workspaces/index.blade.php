@extends('layout')

@section('main_container')
    <div class="right_col" role="main">
        <h1>{{trans('site.my workspace')}}</h1><hr>
        <div class="workspaces-body">
            @if($workspace->id != 0)
                <h1>{{$workspace->name}}</h1>
                <h4>{{trans('site.other people in your workspace')}}</h4>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="x_panel tile overflow_hidden">
                        <div class="x_title">
                            <h2 style="font-size: 18px">{{$workspace->name}}</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <table class="scroll" style="width:100%">
                                <tbody>
                                    @if(count($workers) > 1)
                                        @foreach($workers as $worker)
                                            @if(Auth::user()->worker->name != $worker->name)
                                                @if($loop->index == 0)
                                                    <tr>
                                                        <th>{{trans('site.No.')}}</th>
                                                        <th>{{trans('site.Full name')}}</th>
                                                    </tr>
                                                @endif
                                                <tr>
                                                    <td>{{$loop->iteration}}.</td>
                                                    <td>{{$worker->name}} {{$worker->surname}}</td>
                                                </tr>
                                            @endif
                                        @endforeach
                                    @else
                                        <tr class="workspace-tr" style="border-bottom: none;">
                                            <td style="padding: 5px 20px; font-size: 14px;">{{trans('site.no other people in workspace')}}</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="x_panel tile overflow_hidden">
                        <div class="col-md-12 x_title">
                            <div class="pull-left">
                                <h4>{{trans('site.Product')}}:
                                    <?php $productId = 0 ?>
                                    @foreach($products as $product)
                                        <?php $productId = $product->id ?>
                                        {{ $product->name }}
                                    @endforeach
                                </h4>
                            </div>
                            <div class="pull-right">
                                <h4>{{trans('site.Date')}}: {{date('Y-m-d')}}</h4>
                            </div>
                        </div>
                        <div class="x_content">
                            <div class="col-md-4">
                                <h3>{{trans('site.Quantity')}}:</h3>
                            </div>
                            <div class="col-md-8">
                                <form action="{{url('worker/workspaces/save/' . $workspace->id)}}" class="validate-form2"
                                      method="POST">
                                    {!! csrf_field() !!}
                                    <input type="hidden" name="product_id" value="<?= $productId ?>">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">{{trans('site.Day start quantity')}}</label>
                                            <input name="day_start" class="form-control validate-input" value="{{ $day_start }}">
                                            <div class="alert alert-danger"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">{{trans('site.Day end quantity')}}</label>
                                            <input name="day_end" class="form-control validate-input">
                                            <div class="alert alert-danger"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <input type="submit" class="btn btn-success btn-block"
                                               value="{{trans('site.Submit')}}">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    @if(session()->has('message'))
                        <div class="alert alert-{{session()->get('class')}}">
                            {{ (session()->get('message') )}}
                        </div>
                    @endif
                </div>
        </div>
        <hr>
        <div class="workspaces-body">
            <h1>{{trans('site.loaders')}}</h1>
            @if(count($loaders) >= 1)
                @foreach($loaders->chunk(3) as $loaderChunk)
                    <div class="row">
                        @foreach($loaderChunk as $loader)
                            <div class="col-md-4 col-sm-4 col-xs-12 text-center col-loader loader_{{$loader->id}}">
                                <div style="width: 100%" class="workspace-block">
                                    <div class="workspace-title">
                                        <h2><i>{{ $loop->iteration }}. {{$loader->name}}</i></h2>
                                    </div>
                                    <div class="workspace-content">
                                        <table style="width: 100%">
                                            @foreach($loaderWorkers as $worker)
                                                @foreach($worker->loaderTo as $work)
                                                    @if($worker->loader_id == $loader->id)
                                                        <tr class="workspace-tr">
                                                            <td style="padding: 5px 20px; font-size: 14px"> {{ $loop->iteration }} . {{$worker->name}} {{$worker->surname}}</td>
                                                        </tr>
                                                    @endif
                                                @endforeach
                                            @endforeach
                                            @if(count($loaderWorkers) <= 1)
                                                <tr>
                                                    <td style="padding: 5px 20px; font-size: 14px">{{trans('site.no workers in loader')}}</td>
                                                </tr>
                                            @endif
                                        </table>
                                    </div>
                                </div>
                                <div class="bottom-btn-left">
                                    @if($loader->action == null)
                                        <a class="btn btn-success btn-mid loaderCall fill-loader"
                                           data-loader="{{$loader->id}}" data-text="{{trans('site.Loader called')}}"
                                           data-status="2" data-workspace="{{$workspace->id}}"
                                           type="button">{{trans('site.call loader')}}</a>
                                    @else
                                        @if($loader->action->user_id == \Auth::user()->id && $loader->action->actions_status != 0)
                                            <a class="btn btn-warning btn-mid" type="button" disabled="disabled"
                                               data-loader="{{$loader->id}}" data-text="{{trans('site.Loader called')}}"
                                               data-workspace="{{$workspace->id}}">{{trans('site.Loader called')}}</a>
                                        @else
                                            @if($loader->action->actions_status == 0)
                                                <a class="btn btn-success btn-mid loaderCall fill-loader"
                                                   data-loader="{{$loader->id}}" data-workspace="{{$workspace->id}}"
                                                   data-text="{{trans('site.Loader called')}}" data-status="2"
                                                   type="button">{{trans('site.call loader')}}</a>
                                            @else
                                                <a class="btn btn-danger btn-mid" type="button" disabled="disabled"
                                                   data-loader="{{$loader->id}}"
                                                   data-text="{{trans('site.Loader called')}}"
                                                   data-workspace="{{$workspace->id}}">{{trans('site.loader is busy')}}</a>
                                            @endif
                                        @endif
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endforeach
            @else
                <h4>{{trans('site.loaders not found')}}</h4>
            @endif
            @else
                <h4>{{trans('site.you are not assigned to workspace')}}</h4>
            @endif
        </div>
    </div>
    @include('worker.modals.fillLoaderModal')
@endsection

@section('footer_scripts')
    @if(env("APP_ENV") == 'local')
        <script src="http://localhost:3000/socket.io/socket.io.js"></script>
    @else
        <script src="http://erm.procreo.eu:3000/socket.io/socket.io.js"></script>
    @endif
    <script>
        var socket = io('http://localhost:3000');

        if ('{{ env('APP_ENV') }}' != 'local') {
            socket = io('http://erm.procreo.eu:3000');
        }
        socket.on("call-loader:App\\Events\\Factory", function (message) {
            var text = "{{trans('site.loader is busy')}}";
            var call_loader_text = "{{trans('site.call loader')}}";

            if (message.data.action_status_type != 0) {
                $('.loader_' + message.data.loader_id).find('a').removeClass('btn-success fill-loader').addClass('btn-danger').off('click').attr('disabled', true).html(text);
            } else {
                $('.loader_' + message.data.loader_id).find('a').removeClass('btn-danger btn-warning').addClass('btn-success loaderCall fill-loader').on('click').attr('disabled', false).html(call_loader_text);
            }

            if (message.data.job_rejected) {
                alert("{{trans('site.job_rejected')}}");
            }
        });

        $(document).on("submit", '.fill-loader-form', function (e) {
            var url = $(this).attr('action');
            var text = "{{trans('site.Loader called')}}";
            var loader = $(this).find('.loader').val();

            $.ajax({
                type: "POST",
                url: url,
                data: $(".fill-loader-form").serialize(),
                success: function () {
                    $('.bottom-btn-left').find(".loaderCall[data-loader='" + loader + "']").removeClass('btn-success btn-danger fill-loader').addClass('btn-warning').attr('disabled', true).html(text);
                    $('#fillLoaderModal').modal('toggle');
                }
            });
            e.preventDefault();
        });


    </script>
@endsection