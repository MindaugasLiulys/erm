@extends('layout')
@section('main_container')

    <!-- page content -->
    <div class="right_col" role="main">
    <a href="{{route('callTheTruck')}}" type="button" class="btn btn-success btn-lg">{{trans('site.Call the truck')}}</a>
    </div>
    <!-- /page content -->
@endsection