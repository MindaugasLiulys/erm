<div class="modal fillLoaderModal fade" id="fillLoaderModal" tabindex="-1" role="dialog" aria-labelledby="fillLoaderModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h2 class="modal-title text-center">{{trans('site.fill loader')}}</h2>
            </div>
            <form action="{{url('worker/workspaces')}}" method="post" class="text-center validate-form fill-loader-form">
                <div class="modal-body">
                    {!! csrf_field() !!}
                    <input type="hidden" class="loader" name="loader_id">
                    <input type="hidden" class="workspace" name="workspace_id">
                    <input type="hidden" class="status" name="actions_status">
                    <div class="form-group">
                        <label class="control-label" for="volume">{{trans('site.volume')}} (m<sup>3</sup>)</label>
                        <input name="volume" id="volume" class="form-control validate-input">
                        <div class="alert alert-danger"></div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="volume">{{trans('site.select warehouse')}}</label>
                        <select id="volume" name="warehouse_id" class="select-warehouse form-control">
                            @if(count($warehouses) >= 1)
                                @foreach($warehouses as $warehouse)
                                    <option value="{{$warehouse->id}}">{{$warehouse->name}}</option>
                                @endforeach
                            @else
                                <option value="0">{{trans('site.no available warehouses')}}</option>
                            @endif
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('site.cancel')}}</button>
                    @if(count($warehouses) >= 1)
                        <input type="submit" class="btn btn-success loaderCall" value="{{trans('site.call loader')}}">
                    @else
                        <input type="submit" class="btn btn-success" value="{{trans('site.call loader')}}" disabled="disabled">
                    @endif
                </div>
            </form>
        </div>
    </div>
</div>