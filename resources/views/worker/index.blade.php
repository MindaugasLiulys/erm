@extends('layout')
@section('main_container')
    <div class="right_col" role="main">
        <div class="row tile_count">
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count no-border1">
                <span class="count_top"><i class="fa fa-user"></i> {{trans('site.Total workers')}}</span>
                <div class="count">{{count($workers)}}</div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-clock-o"></i> {{trans('site.This months production')}}</span>
                <div class="count">{{workspaceMonthsProd(0, $workspace->workspace_id, $type)}}m<sup>3</sup></div>
                <span class="count_bottom">
                    @if(percentDifference(workspaceMonthsProd(0, $workspace->workspace_id, $type), workspaceMonthsProd(1, $workspace->workspace_id, $type)) > 0)
                        <i class="green"><i class="fa fa-sort-asc"></i>
                    @else
                        <i class="red"><i class="fa fa-sort-desc"></i>
                    @endif
                        {{percentDifference(workspaceMonthsProd(0, $workspace->workspace_id, $type), workspaceMonthsProd(1, $workspace->workspace_id, $type))}}
                        %</i> {{trans('site.From last month')}}
                </span>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="dashboard_graph">
                    <div class="row x_title">
                        <div class="col-md-6">
                            <h3>{{trans('site.Production statistics')}} <small>({{trans('site.12-weeks')}}) | ({{$workspace->workspace->name}})</small>
                            </h3>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div id="area-chart"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer_scripts')
    <script>
        $(document).ready(function() {
            var productionText = "{{trans('site.Months production')}}";
            var data = [
                        { m: "{{getWeek(11)}}", a: "{{productionWeek(11, $workspace->workspace_id, $type)}}"},
                        { m: "{{getWeek(10)}}", a: "{{productionWeek(10, $workspace->workspace_id, $type)}}"},
                        { m: "{{getWeek(9)}}", a: "{{productionWeek(9, $workspace->workspace_id, $type)}}"},
                        { m: "{{getWeek(8)}}", a: "{{productionWeek(8, $workspace->workspace_id, $type)}}"},
                        { m: "{{getWeek(7)}}", a: "{{productionWeek(7, $workspace->workspace_id, $type)}}"},
                        { m: "{{getWeek(6)}}", a: "{{productionWeek(6, $workspace->workspace_id, $type)}}"},
                        { m: "{{getWeek(5)}}", a: "{{productionWeek(5, $workspace->workspace_id, $type)}}"},
                        { m: "{{getWeek(4)}}", a: "{{productionWeek(4, $workspace->workspace_id, $type)}}"},
                        { m: "{{getWeek(3)}}", a: "{{productionWeek(3, $workspace->workspace_id, $type)}}"},
                        { m: "{{getWeek(2)}}", a: "{{productionWeek(2, $workspace->workspace_id, $type)}}"},
                        { m: "{{getWeek(1)}}", a: "{{productionWeek(1, $workspace->workspace_id, $type)}}"},
                        { m: "{{getWeek(0)}}", a: "{{productionWeek(0, $workspace->workspace_id, $type)}}"}
                    ],
                    config = {
                        data: data,
                        parseTime:false,
                        xkey: 'm',
                        ykeys: ['a'],
                        labels: [productionText],
                        fillOpacity: 0.6,
                        hideHover: 'auto',
                        behaveLikeLine: true,
                        resize: true,
                        pointFillColors:['#ffffff'],
                        pointStrokeColors: ['black'],
                        lineColors:["rgba(38, 185, 154, 0.38)"]
                    };
            config.element = 'area-chart';
            Morris.Area(config);
        })
    </script>
@endsection