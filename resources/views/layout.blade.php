<!DOCTYPE html>
<html lang="{{App::getLocale()}}">

<head>
    @include('includes.head')
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        @if(\Request::route()->getName() !== 'board')
            @if(\Auth::user()->isAdmin())
                @include('includes.admin.sidebar')
            @elseif(\Auth::user()->isWorker())
                @include('includes.worker.sidebar')
            @else
                @include('includes.worker.sidebar')
            @endif
            @include('includes.topbar')
        @endif

        @yield('main_container')

        @if(\Request::route()->getName() !== 'board')
            @include('includes.footer')
        @endif

    </div>
</div>

<!-- jQuery -->
<script src="{{ asset("js/jquery.min.js") }}" type="text/javascript"></script>

<!-- Bootstrap -->
<script src="{{ asset("js/app.js") }}"></script>
<script src="{{ asset("js/datatables.js") }}"></script>
<script src="{{ asset("js/bootstrap.min.js") }}"></script>

<script src="{{ asset("js/Chart.min.js") }}"></script>
<script src="{{ asset("js/raphael.min.js") }}"></script>
<script src="{{ asset("js/morris.min.js") }}"></script>
<script src="{{ asset("js/moment.js") }}"></script>
<script src="{{ asset("js/daterangepicker.js") }}"></script>
<script src="{{ asset("js/bootstrap-datetimepicker.min.js") }}"></script>
<script src="{{ asset("js/jquery.sparkline.min.js") }}"></script>
<!-- PNotify -->
<script src="{{ asset("js/pnotify/pnotify.js") }}"></script>
<script src="{{ asset("js/pnotify/pnotify.buttons.js") }}"></script>
<script src="{{ asset("js/pnotify/pnotify.nonblock.js") }}"></script>

<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"
        integrity="sha256-0YPKAwZP7Mp3ALMRVB2i8GXeEndvCq3eSl/WsAl1Ryk="
        crossorigin="anonymous"></script>

<script src="{{ asset("js/custom.js") }}"></script>
@yield('footer_scripts')

</body>
</html>