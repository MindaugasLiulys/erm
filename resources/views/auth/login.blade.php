<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>ERM | Login</title>
    
    <!-- Bootstrap -->
    <link href="{{ asset("css/bootstrap.min.css") }}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset("css/font-awesome.min.css") }}" rel="stylesheet">
    <!-- Custom Theme Style -->
	<link href="{{ asset("css/bootstrap.min.css") }}" rel="stylesheet">
	<link href="{{ asset("css/custom.css") }}" rel="stylesheet">
	<link href="{{ asset("css/animate.css") }}" rel="stylesheet">
	<link href="{{ asset("fonts/css/font-awesome.css") }}" rel="stylesheet">


	<script src="{{ asset("js/jquery.min.js") }}"></script>
	<script src="{{ asset("js/bootstrap.min.js") }}"></script>
	<script src="{{ asset("js/app.js") }}"></script>
	<script src="{{ asset("js/custom.js") }}"></script>

</head>

<body class="login">
<div>
    <div class="login_wrapper">
        <div class="animate form login_form">
            <section class="login_content">
				{!! BootForm::open(['url' => url('/login'), 'method' => 'post']) !!}
				<h1>Login Form</h1>
				{!! BootForm::text('email', 'Username', old('email'), ['placeholder' => 'Username', 'afterInput' => '<span>test</span>'] ) !!}
				{!! BootForm::password('password', 'Password', ['placeholder' => 'Password']) !!}
				<div>
					{!! BootForm::submit('Log in', ['class' => 'btn btn-default submit']) !!}
				</div>
                    
				<div class="clearfix"></div>
                    
				<div class="separator">
					<div class="clearfix"></div>
					<br />
                        
					<div>
						<h1><i class="fa fa-cubes"></i> ERM</h1>
						<p>©2017 All Rights Reserved.</p>
					</div>
				</div>
				{!! BootForm::close() !!}
            </section>
        </div>
    </div>
</div>
</body>
</html>