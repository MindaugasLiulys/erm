<footer>
    <div class="pull-left">
        <input action="action" class="btn btn-info" onclick="window.history.go(-1); return false;" type="button" value="{{trans('site.Back')}}" />
    </div>
    <div class="pull-right">
       <a href="https://procreo.eu">Procreo</a>
    </div>
    <div class="clearfix"></div>
</footer>