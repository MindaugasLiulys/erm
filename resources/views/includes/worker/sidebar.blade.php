<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
            <a href="{{ url('/') }}" class="site_title"><i class="fa fa-paw"></i> <span>ERM</span></a>
        </div>
        
        <div class="clearfix"></div>
        
        <!-- menu profile quick info -->
        <div class="profile">
            <div class="profile_info">
                <span>{{trans('site.Welcome')}},</span>
                {{--<h2>{{$worker->name}} {{$worker->surname}}</h2>--}}
                <h2>{{\Auth::user()->worker->name}} {{\Auth::user()->worker->surname}} ({{\Auth::user()->username}})</h2>
                <span>{{trans('site.worker')}}
                    @if(!empty(Auth::user()->worker->position->name))
                        ({{Auth::user()->worker->position->name}})
                    @else
                        ({{trans('site.position not assigned')}})
                    @endif
                </span>
            </div>
        </div>
        <!-- /menu profile quick info -->
        
        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <ul class="nav side-menu">
                    <li>
                        <a href="{{url('/')}}">
                            <i class="fa fa-tachometer"></i>
                            {{trans('site.Dashboard')}}
                        </a>
                    </li>
                    <li>
                        <a href="{{url('worker/workspaces')}}">
                            <i class="fa fa-building"></i>
                            {{trans('site.my workspace')}}
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /sidebar menu -->
    </div>
</div>