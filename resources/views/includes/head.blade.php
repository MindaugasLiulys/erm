<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<!-- Meta, title, CSS, favicons, etc. -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>ERM </title>

<!-- Bootstrap -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<!-- Font Awesome -->
<link href="{{ asset("css/font-awesome.min.css") }}" rel="stylesheet">
<!-- Custom Theme Style -->
<link href="{{ asset("css/custom.css") }}" rel="stylesheet">
<link href="{{ asset("css/animate.css") }}" rel="stylesheet">
<link href="{{ asset("fonts/css/font-awesome.css") }}" rel="stylesheet">
<!-- Datatables start-->
<link href="{{ asset("css/dataTables.bootstrap.css") }}" rel="stylesheet">
<link href="{{ asset("css/jquery.dataTables.css") }}" rel="stylesheet">
<!-- Datatables end -->
<!-- PNotify -->
<link href="{{ asset("css/pnotify/pnotify.css") }}" rel="stylesheet">
<link href="{{ asset("css/pnotify/pnotify.buttons.css") }}" rel="stylesheet">
<link href="{{ asset("css/pnotify/pnotify.nonblock.css") }}" rel="stylesheet">

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<!-- Daterangepicker -->
<link href="{{ asset("css/daterangepicker.css") }}" rel="stylesheet">
<!-- Datetimepicker -->
<link href="{{ asset("css/bootstrap-datetimepicker.css") }}" rel="stylesheet">
