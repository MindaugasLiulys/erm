<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
            <a href="{{route('admin_panel')}}" class="site_title"><i class="fa fa-cubes" aria-hidden="true"></i><span>ERM</span></a>
        </div>
        
        <div class="clearfix"></div>
        
        <!-- menu profile quick info -->
        <div class="profile">
            <div class="profile_info">
                <span>{{trans('site.Welcome')}},</span>
            </div>
        </div>
        <!-- /menu profile quick info -->
        
        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <h3>{{trans('Admin')}}</h3>
                <ul class="nav side-menu">
                    <li>
                        <a href="{{route('admin_panel')}}">
                            <i class="fa fa-tachometer"></i>
                            {{trans('site.Dashboard')}}
                        </a>
                    </li>
                    <li>
                        <a href="{{route('statistics')}}">
                            <i class="fa fa-bar-chart"></i>
                            {{trans('site.Statistics')}}
                        </a>
                    </li>
                    <li>
                        <a href="{{route('workers')}}">
                            <i class="fa fa-user"></i>
                            {{trans('site.workers')}}
                        </a>
                    </li>
                    <li>
                        <a href="{{route('factory')}}">
                            <i class="fa fa-cogs"></i>
                            {{trans('site.factory')}}
                        </a>
                    </li>
                    <li>
                        <a href="{{route('workspaces')}}">
                            <i class="fa fa-building"></i>
                            {{trans('site.workspaces')}}
                        </a>
                    </li>
                    <li>
                        <a href="{{route('loaders')}}">
                            <i class="fa fa-truck"></i>
                            {{trans('site.loaders')}}
                        </a>
                    </li>
                    <li>
                        <a href="{{route('warehouses')}}">
                            <i class="fa fa-codepen"></i>
                            {{trans('site.warehouses')}}
                        </a>
                    </li>
                    <li>
                        <a href="{{route('positions')}}">
                            <i class="fa fa-folder-open"></i>
                            {{trans('site.positions')}}
                        </a>
                    </li>
                    <li>
                        <a href="{{route('products')}}">
                            <i class="fa fa-puzzle-piece"></i>
                            {{trans('site.products')}}
                        </a>
                    </li>
                    <li>
                        <a href="{{route('admin.materials.index')}}">
                            <i class="fa fa-cogs"></i>
                            {{trans('site.materials')}}
                        </a>
                    </li>
                    <li>
                        <a href="{{route('map.index')}}">
                            <i class="fa fa-map-marker"></i>
                            {{trans('site.map')}}
                        </a>
                    </li>
                    <li>
                        <a href="{{route('orders')}}">
                            <i class="fa fa-list-alt" aria-hidden="true"></i>
                            {{trans('site.Orders')}}
                        </a>
                    </li>
                    <li>
                        <a href="{{route('tasks')}}">
                            <i class="fa fa-calendar" aria-hidden="true"></i>
                            {{trans('site.tasks')}}
                        </a>
                    </li>
                    <li>
                        <a href="{{route('breakdowns')}}">
                            <i class="fa fa-exclamation-triangle"></i>
                            {{trans('site.breakdowns')}}
                        </a>
                    </li>
                    <li>
                        <a href="{{route('settings')}}">
                            <i class="fa fa-cog" aria-hidden="true"></i>
                            {{trans('site.settings')}}
                        </a>
                    </li>
                </ul>
            </div>

        </div>
        <!-- /sidebar menu -->
    </div>
</div>