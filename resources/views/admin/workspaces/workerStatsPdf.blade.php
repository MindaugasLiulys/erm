<style>

    *{
        font-family: DejaVu Sans !important;
    }

    .invoice-box {
        max-width: 1100px;
        margin: auto;
        padding: 30px;
        border: 1px solid #eee;
        font-size: 10px;
        line-height: 15px;
        color: #555;
    }

    .invoice-box table{
        width:100%;
        border-collapse: collapse;
        text-align:left;
    }

    .invoice-box table td{
        padding:5px;
        vertical-align:top;
    }

    td.cred {
        text-align: left;
    }

    .invoice-box tr.tr-border td {
        padding: 8px 0;
    }

    .invoice-box table tr.tr-border td{
        border-bottom: 1px solid #eee;
    }

    .invoice-box table tr.top table td{
        padding-bottom:20px;
    }

    .invoice-box table{
        border-top: 1px solid #eeeeee;
    }

    .invoice-box .top-block{
        font-size: 14px;
    }

</style>
<div class="invoice-box">
    <div class="top-block">
        <div style="width: 49%; display: inline-block">
            <span class="company-name">{{$settings->company_name}}</span><br>
            <span class="company-address">
                <strong>{{trans('site.Address')}}:</strong>
                @foreach($settingsAddress as $address)
                    {{$address}}<br>
                @endforeach
            </span>
            <span class="company-email">
                <strong>{{trans('site.Email')}}:</strong>
                @foreach($emails as $email)
                    {{$email}}<br>
                @endforeach
            </span>
            @if(!empty($settings->company_phone))
                <span class="company-phone">
                    <strong>{{trans('site.Phone')}}:</strong>
                    {{$settings->company_phone}}
                </span><br>
            @endif
            @if(!empty($settings->company_fax))
                <span class="company-fax">
                    <strong>{{trans('site.Fax')}}:</strong>
                    {{$settings->company_fax}}
                </span><br>
            @endif
            @if(!empty($settings->company_website))
                <span class="company-website">
                    <strong>{{trans('site.Website')}}:</strong>
                    {{$settings->company_website}}
                </span>
            @endif
        </div>
        <div style="width: 50%; text-align: right; display: inline-block">
            <strong>{{trans('site.Full name')}}: </strong>{{$worker->name}} {{$worker->surname}}<br>
            <strong>{{trans('site.Position')}}: </strong>{{$worker->position->name}}
        </div>
    </div>

    <table cellpadding="0" cellspacing="0">
        <tr style="margin-top:20px; height: 60px;" class="tr-border">
            <th>{{trans('site.No.')}}</th>
            <th>{{trans('site.workspace')}}</th>
            <th>{{trans('site.Day start')}}</th>
            <th>{{trans('site.Day end')}}</th>
            <th>{{trans('site.Exported')}}</th>
            <th>{{trans('site.Produced')}}</th>
            <th>{{trans('site.Product')}}</th>
            <th>{{trans('site.Date')}}</th>
        </tr>
        <?php $serialNo = 1 ?>
        @foreach($productions as $production)
            @if(($production->worker_id == $worker->id) && ($production->updated_at->format('Y-m-d') >= $from) && ($production->updated_at->format('Y-m-d') <= $to))
                <tr class="tr-border">
                    <td>{{$serialNo ++}}</td>
                    <td>{{$production->workspace->name}}</td>
                    <td>{{$production->day_start}}</td>
                    <td>{{$production->day_end}}</td>
                    <td>{{$production->unloaded - $production->day_end + $production->day_start}}</td>
                    <td>{{$production->unloaded}}</td>
                    <td>{{$production->product->name}}</td>
                    <td>{{$production->updated_at->format('Y-m-d')}}</td>
                </tr>
            @endif
        @endforeach
    </table>
</div>

