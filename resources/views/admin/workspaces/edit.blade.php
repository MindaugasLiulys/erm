@extends('layout')

@section('main_container')

    <div class="right_col" role="main">
        <h1>{{trans('site.edit workspace')}}</h1><hr>
        <form action="{{url('admin/workspaces/edit/' . $workspace->id)}}" class="create-new-form validate-form" method="POST">
            {!! csrf_field() !!}
            <div class="form-group">
                <label class="control-label">{{trans('site.name')}}</label>
                <input name="name" class="form-control validate-input" value="{{$workspace->name}}">
                <div class="alert alert-danger"></div>
            </div>
            <div class="form-group">
                <label class="control-label">{{trans('site.distinctive color')}}</label>
                <input type="color" name="favcolor" class="form-control" value="{{$workspace->color}}">
                <div class="alert alert-danger"></div>
            </div>
            <div class="form-group">
                <a href="{{route('workspaces')}}" class="btn btn-default">{{trans('site.cancel')}}</a>
                <input class="btn btn-success" type="submit" value="{{trans('site.save')}}">
            </div>
        </form>
    </div>
@endsection