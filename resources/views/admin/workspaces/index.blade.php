@extends('layout')

@section('main_container')
    <div class="right_col" role="main">
        <h1>{{trans('site.workspaces')}}</h1>
        <a class="btn btn-primary" href="{{route('create-workspace')}}"><i class="fa fa-plus" aria-hidden="true"></i> {{trans('site.create_workspace')}}</a><hr>
        <div class="workspaces-body">
            @if(count($workspaces) >= 1)
                @foreach($workspaces->chunk(3) as $workspace1)
                    <div class="row">
                        @foreach($workspace1 as $workspace)
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <div class="x_panel tile overflow_hidden">
                                    <div class="x_title">
                                        <h2 class="block-title-fit" style="font-size: 16px">
                                            <span class="rand-color stroke" style="background-color: {{$workspace->color}}">
                                                {{$loop->iteration}}. {{$workspace->name}}
                                            </span>
                                        </h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                            </li>
                                            <li class="dropdown">
                                                <a href="javascript:;" class="workplace_drop dropdown-toggle" data-toggle="dropdown" role="button" data-id="{{$workspace->id}}" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                                <ul class="dropdown-menu dropdown_workplace_{{$workspace->id}}" role="menu">
                                                    <li>
                                                        <a class="workspaceModalCall" data-workspace="{{$workspace->id}}" data-name="{{$workspace->name}}"><i class="fa fa-area-chart" aria-hidden="true"></i> {{trans('site.Statistics')}}</a>
                                                    </li>
                                                    <li>
                                                        <a href="{{url('admin/workspaces/edit/' . $workspace->id)}}"><i class="fa fa-edit" aria-hidden="true"></i> {{trans('site.Edit')}}</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:;" class="add-worker-modal" data-action="{{url('admin/workspaces/add/' . $workspace->id)}}" data-id="{{$workspace->id}}"><i class="fa fa-plus" aria-hidden="true"></i> {{trans('site.add worker')}}</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:;" data-action="{{url('admin/workspaces/delete/' . $workspace->id)}}" data-method="delete"><i class="fa fa-trash" aria-hidden="true"></i> {{trans('site.Delete')}}</a>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                        <table class="workspace-table scroll first-column-width" style="width:100%">
                                            <tbody>
                                            <?php $workersNumber = 1?>
                                            @foreach($workers as $worker)
                                                @foreach($worker->workspaces as $work)
                                                    @if($work->id == $workspace->id)
                                                        @if($workersNumber == 1)
                                                            <tr>
                                                                <th>{{trans('site.No.')}}</th>
                                                                <th>{{trans('site.Full name')}}</th>
                                                                <th>{{trans('site.Action')}}</th>
                                                            </tr>
                                                        @endif
                                                        <tr>
                                                            <td><?=$workersNumber++?>.</td>
                                                            <td>{{$worker->name}} {{$worker->surname}}</td>

                                                            <td>
                                                                <a class="btn btn-info btn-xs btn-remove worker-stat" data-id="{{$worker->id}}" data-name="{{$worker->name}} {{$worker->surname}}"><i class="fa fa-bar-chart" aria-hidden="true"></i></a>
                                                                <a class="btn btn-danger btn-xs btn-remove" data-action="{{url('admin/workspaces/remove/' . $worker->id)}}" data-method="delete"><i class="fa fa-minus" aria-hidden="true"></i></a>
                                                            </td>
                                                        </tr>
                                                    @endif
                                                @endforeach
                                            @endforeach
                                            @if($workersNumber <= 1)
                                                <tr>
                                                    <td colspan="3" class="no-workers">{{trans('site.no workers in workspace')}}</td>
                                                </tr>
                                            @endif
                                            </tbody>
                                        </table>
                                        <div style="text-align: center" class="graphic show">
                                            <div class="clearfix"></div>
                                            <div id="graph_{{$workspace->id}}" class="graphic-div"
                                                 data-mon="{{weekDayData($monday, $workspace->id)}}"
                                                 data-tue="{{weekDayData($tuesday, $workspace->id)}}"
                                                 data-wed="{{weekDayData($wednesday, $workspace->id)}}"
                                                 data-thu="{{weekDayData($thursday, $workspace->id)}}"
                                                 data-fri="{{weekDayData($friday, $workspace->id)}}"
                                                 data-sat="{{weekDayData($saturday, $workspace->id)}}"
                                                 data-sun="{{weekDayData($sunday, $workspace->id)}}"
                                            >
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @include('admin.modals.workspaceStatModal')
                        @endforeach
                    </div>
                @endforeach
            @else
                <h4>{{trans('site.workspaces not found')}}</h4>
            @endif
        </div>
    </div>
    @include('admin.modals.workerStatModal')
    @include('admin.modals.addWorkerModal')
    @include('admin.modals.deleteModal')
@endsection
@section('footer_scripts')
    <script>
        $(document).ready(function() {

        })
    </script>
@endsection