@extends('layout')
@section('main_container')
    <div class="right_col" role="main">
        <h1>{{$worker->name}} {{$worker->surname}} {{trans('site.statistics')}}</h1><hr>
        <div class="clearfix"></div>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <fieldset class="col-md-9 col-sm-9 col-xs-12">
                <div class="control-group">
                    <div class="controls">
                        <div class="col-md-12  xdisplay_inputx form-group has-feedback" >
                            <div class="col-md-5 text-right">
                                <label style="padding-top: 6px">{{trans('site.Select date')}}:</label>
                            </div>
                            <div class="col-md-7">
                                <input type="text" class="form-control has-feedback-left single_cal1" placeholder="{{trans('site.Select date')}}" aria-describedby="inputSuccess2Status">
                                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>
            <div class="col-md-3 col-sm-3 col-xs-12 print-button">
                <a class="btn btn-primary" id="worker-stat-print" data-id="{{$worker->id}}" style="margin-bottom: 15px" disabled="disabled"><i class="fa fa-print" aria-hidden="true"></i> {{trans('site.Print statistics')}}
                </a>
            </div>
        </div>
        <div class="workspaces-body">
            <table id="example" class="display resp table-workerStats table table-striped table-bordered dataTable no-footer" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>{{trans('site.No.')}}</th>
                        <th>{{trans('site.Full name')}}</th>
                        <th>{{trans('site.Position')}}</th>
                        <th>{{trans('site.Day start')}}</th>
                        <th>{{trans('site.Day end')}}</th>
                        <th>{{trans('site.Exported')}}</th>
                        <th>{{trans('site.Produced')}}</th>
                        <th>{{trans('site.workspace')}}</th>
                        <th>{{trans('site.Product')}}</th>
                        <th>{{trans('site.Date')}}</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>{{trans('site.No.')}}</th>
                        <th>{{trans('site.Full name')}}</th>
                        <th>{{trans('site.Position')}}</th>
                        <th>{{trans('site.Day start')}}</th>
                        <th>{{trans('site.Day end')}}</th>
                        <th>{{trans('site.Exported')}}</th>
                        <th>{{trans('site.Produced')}}</th>
                        <th>{{trans('site.workspace')}}</th>
                        <th>{{trans('site.Product')}}</th>
                        <th>{{trans('site.Date')}}</th>
                    </tr>
                </tfoot>
                <tbody>
                <?php $serialNo = 1?>
                    @foreach($productions as $production)
                        @if($production->worker_id == $worker->id)
                            <tr>
                                <td>{{$serialNo++}}</td>
                                <td>{{$worker->name}} {{$worker->surname}}</td>
                                <td>{{$worker->position->name}}</td>
                                <td>{{$production->day_start}}</td>
                                <td>{{$production->day_end}}</td>
                                <td>{{$production->unloaded}}</td>
                                <td>{{$production->unloaded + $production->day_end - $production->day_start}}</td>
                                <td>{{$production->workspace->name}}</td>
                                <td>{{$production->product->name}}</td>
                                <td>{{$production->updated_at->format('Y-m-d')}}</td>
                            </tr>
                        @endif
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
