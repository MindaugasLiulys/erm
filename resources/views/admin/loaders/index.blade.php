@extends('layout')

@section('main_container')
    <div class="right_col" role="main">
        <h1>{{trans('site.loaders')}}</h1>
        <a class="btn btn-primary" href="{{route('create-loader')}}"><i class="fa fa-plus" aria-hidden="true"></i> {{trans('site.create loader')}}</a><hr>
        <div class="workspaces-body">
            @if(count($loaders) >= 1)
                @foreach($loaders->chunk(3) as $loaderChunk)
                    <div class="row">
                        @foreach($loaderChunk as $loader)
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <div class="x_panel tile">
                                    <div class="x_title">
                                        <h2 style="font-size: 18px">
                                            <span class="rand-color stroke" style="background-color: {{$loader->color}}">
                                                {{$loop->iteration}}. {{$loader->name}}
                                            </span>
                                        </h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                            <li class="dropdown">
                                                <a href="javascript:;" class="workplace_drop dropdown-toggle" data-toggle="dropdown" role="button" data-id="{{$loader->id}}" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                                <ul class="dropdown-menu dropdown_workplace_{{$loader->id}}" role="menu">
                                                    <li>
                                                        <a class="loaderModalCall" data-loader="{{$loader->id}}" data-name="{{$loader->name}}"><i class="fa fa-area-chart" aria-hidden="true"></i> {{trans('site.Statistics')}}</a>
                                                    </li>
                                                    <li>
                                                        <a href="{{url('admin/loaders/edit/' . $loader->id)}}"><i class="fa fa-edit" aria-hidden="true"></i> Edit</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:;" class="add-worker-modal" data-action="{{url('admin/loaders/add/' . $loader->id)}}" data-id="{{$loader->id}}"><i class="fa fa-plus" aria-hidden="true"></i> {{trans('site.add worker')}}</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:;" data-action="{{url('admin/loaders/delete/' . $loader->id)}}" data-method="delete"><i class="fa fa-trash" aria-hidden="true"></i> Delete</a>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                        <table class="workspace-table scroll first-column-width" style="width:100%">
                                            <tbody>
                                            <?php $workersNumber = 1?>
                                            @foreach($workers as $worker)
                                                @foreach($worker->loaderTo as $work)
                                                    @if($work->loader_id == $loader->id)
                                                        @if($workersNumber == 1)
                                                            <tr>
                                                                <th>{{trans('site.No.')}}</th>
                                                                <th>{{trans('site.Full name')}}</th>
                                                                <th>{{trans('site.Action')}}</th>
                                                            </tr>
                                                        @endif
                                                        <tr>
                                                            <tr>
                                                                <td><?=$workersNumber++?>.</td>
                                                                <td>{{$worker->name}} {{$worker->surname}}</td>
                                                                <td>
                                                                    <a class="btn btn-danger btn-xs btn-remove" data-action="{{url('admin/loaders/remove/' . $worker->id)}}" data-method="delete"><i class="fa fa-minus" aria-hidden="true"></i></a>
                                                                </td>
                                                            </tr>
                                                        @endif
                                                @endforeach
                                            @endforeach
                                            @if($workersNumber <= 1)
                                                <tr>
                                                    <td colspan="3" class="no-workers">{{trans('site.no workers in workspace')}}</td>
                                                </tr>
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            @include('admin.modals.loaderStatModal')
                        @endforeach
                    </div>
                @endforeach
            @else
                <h4>{{trans('site.loaders not found')}}</h4>
            @endif
        </div>
    </div>
    @include('admin.modals.addWorkerModal')
    @include('admin.modals.deleteModal')
@endsection