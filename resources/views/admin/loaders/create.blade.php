@extends('layout')

@section('main_container')
    <div class="right_col" role="main">
        <h1>{{trans('site.new loader')}}</h1><hr>
        <form action="{{route('create-loader')}}" class="create-new-form validate-form" method="POST">
            {!! csrf_field() !!}
            <div class="form-group">
                <label class="control-label">{{trans('site.name')}}</label>
                <input name="name" class="form-control validate-input">
                <div class="alert alert-danger"></div>
            </div>
            <div class="form-group">
                <label class="control-label">{{trans('site.distinctive color')}}</label>
                <input type="color" name="favcolor" class="form-control" value="#999999">
                <div class="alert alert-danger"></div>
            </div>
            <div class="form-group">
                <a href="{{route('loaders')}}" class="btn btn-default">{{trans('site.cancel')}}</a>
                <input class="btn btn-success" type="submit" value="{{trans('site.create')}}">
            </div>
        </form>
    </div>
@endsection