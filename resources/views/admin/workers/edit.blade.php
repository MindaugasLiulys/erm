@extends('layout')

@section('main_container')

    <div class="right_col" role="main">
        <h1>{{trans('site.edit_worker')}}</h1><hr>
        <form action="{{url('admin/worker/edit/' . $worker->id)}}" class="create-new-form validate-form" method="POST">
            {!! csrf_field() !!}
            <div class="form-group">
                <label class="control-label">{{trans('site.name')}}</label>
                <input name="name" class="form-control validate-input" value="{{$worker->name}}">
                <div class="alert alert-danger"></div>
            </div>
            <div class="form-group">
                <label class="control-label">{{trans('site.surname')}}</label>
                <input name="surname" class="form-control validate-input" value="{{$worker->surname}}">
                <div class="alert alert-danger"></div>
            </div>
            <div class="form-group">
                <label class="control-label">{{trans('site.password')}}</label>
                <input name="password" class="form-control validate-input" value="{{$worker->password}}">
                <div class="alert alert-danger"></div>
            </div>
            <div class="form-group">
                <label class="control-label">{{trans('site.position')}}</label>
                <select name="position_id" class="form-control">
                    @foreach($positions as $position)
                        @if($position->active != 0)
                        <option value="{{$position->id}}" {{($worker->position_id == $position->id)?"selected=selected" : '' }}>{{$position->name}}</option>
                        @else
                            <option value="0">{{trans('site.no positions found')}}</option>
                        @endif
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <a href="{{route('workers')}}" class="btn btn-default">{{trans('site.cancel')}}</a>
                <input class="btn btn-success" type="submit" value="{{trans('site.save')}}">
            </div>
        </form>
    </div>
@endsection