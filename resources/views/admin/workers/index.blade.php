@extends('layout')

@section('main_container')

<div class="right_col" role="main">
    <h1>{{trans('site.workers')}}</h1>
    <div class="col-md-2">
        <a class="btn btn-primary" href="{{route('create-worker')}}"><i class="fa fa-plus" aria-hidden="true"></i> {{trans('site.create_worker')}}</a>
    </div>
    <div class="col-md-2 col-md-offset-8">
        <a class="btn btn-primary" style="float: right;" href="{{route('workers-print-all')}}"><i class="fa fa-print" aria-hidden="true"></i> {{trans('site.print all workers')}}</a>
    </div>
    <div class="clearfix"></div>
    <hr>
    <table id="example" class="display table-worker resp table table-striped table-bordered dataTable no-footer" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>{{trans('site.No.')}}</th>
                <th>{{trans('site.username')}}</th>
                <th>{{trans('site.name')}}</th>
                <th>{{trans('site.surname')}}</th>
                <th>{{trans('site.position')}}</th>
                <th>{{trans('site.action')}}</th>
            </tr>
        </thead>
        <tbody>
            @foreach($workers as $worker)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$worker->user->username}}</td>
                    <td>{{$worker->name}}</td>
                    <td>{{$worker->surname}}</td>
                    <td>@if(!empty($worker->position->name))
                            {{$worker->position->name}}
                        @else
                            {{trans('site.no position found')}}
                        @endif
                    </td>
                    <td>
                        <a href="{{url('admin/worker/edit/' . $worker->id)}}" class="btn btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                        <a class="btn btn-danger" data-action="{{url('admin/worker/delete/' . $worker->id)}}" data-method="delete"><i class="fa fa-trash" aria-hidden="true"></i></a>
                        <a href="{{url('admin/workers/print/' . $worker->id)}}" class="btn btn-default"><i class="fa fa-print" aria-hidden="true"></i></a>
                    </td>
                </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <th>{{trans('site.No.')}}</th>
                <th>{{trans('site.username')}}</th>
                <th>{{trans('site.name')}}</th>
                <th>{{trans('site.surname')}}</th>
                <th>{{trans('site.position')}}</th>
                <th>{{trans('site.action')}}</th>
            </tr>
        </tfoot>
    </table>
</div>

@include('admin.modals.deleteModal')
@endsection