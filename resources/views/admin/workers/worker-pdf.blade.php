<style>
    *{
        font-family: DejaVu Sans !important;
    }

    .invoice-box {
        max-width: 800px;
        margin: auto;
        padding: 30px;
        font-size: 16px;
        line-height: 24px;
        color: #555;
    }

    .invoice-box table{
        width:100%;
        border-collapse: collapse;
        text-align:left;
    }

    .invoice-box table td{
        padding:5px;
        vertical-align:top;
    }

    .invoice-box tr.tr-border td {
        padding: 20px 5px;
    }

    .invoice-box table tr td:nth-child(2){
        text-align:right;
    }

    .invoice-box table tr.tr-border td{
        border-bottom: 1px solid #eee;
    }

    .invoice-box table tr.top table td{
        padding-bottom:20px;
    }

    .invoice-box table .company-name{
        font-size: 30px;
    }

</style>
<div class="invoice-box">
    <table cellpadding="0" cellspacing="0">
        <tr class="information">
            <td colspan="2">
                <table>
                    @if(!empty($workers))
                        @foreach($workers as $worker)
                            <tr class="tr-border">
                                <td>
                                    {{trans('site.Name')}}: {{$worker->name}}<br>
                                    {{trans('site.Surname')}}: {{$worker->surname}}<br>
                                    {{trans('site.Position')}}: {{$worker->position->name}}<br>
                                </td>
                                <td>
                                    {{trans('site.Login username')}}: {{$worker->user->username}}<br>
                                    {{trans('site.Password')}}: {{$worker->password}}<br>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td>
                                <span class="company-name">{{$settings->company_name}}</span><br>
                                <span class="company-address">
                                    @foreach($settingsAddress as $address)
                                        {{$address}}<br>
                                    @endforeach
                                </span>
                            </td>
                            <td>
                                {{trans('site.Name')}}: {{$worker->name}}<br>
                                {{trans('site.Surname')}}: {{$worker->surname}}<br>
                                {{trans('site.Position')}}: {{$worker->position->name}}<br><hr>
                                {{trans('site.Login username')}}: {{$worker->user->username}}<br>
                                {{trans('site.Password')}}: {{$worker->password}}<br>
                            </td>
                        </tr>
                    @endif
                </table>
            </td>
        </tr>
    </table>
</div>

