@extends('layout')

@section('main_container')
    <div class="right_col" role="main">
        <h1>{{trans('site.settings')}}</h1><hr>
        <div class="workspaces-body">
            <form method="post" class="text-center form-settings" action="{{route('settings-update')}}">
                <div class="modal-body">
                    {!! csrf_field() !!}
                    <div class="form-group">
                        <label for="company_name">{{trans('site.Company name')}}</label>
                        <input class="form-control" type="text" name="company_name" id="company_name" value="{{$settings->company_name}}">
                        <div class="alert alert-danger"></div>
                    </div>
                    <div class="form-group settings-address">
                        <label for="company_address">{{trans('site.Company address')}}</label>
                        @foreach($address as $key=>$value)
                            <input class="form-control" style="margin-top: 5px" type="text" name="company_address_{{$key}}" id="company_address_{{$key}}" placeholder="{{trans('site.' . $key)}}" value="{{$address[$key]}}">
                        @endforeach
                        <div class="alert alert-danger"></div>
                    </div>
                    <div class="form-group company-email-group">
                        <input type="hidden">
                        <label for="company_emails">{{trans('site.Company emails')}}</label>
                        @foreach($emails as $key=>$value)
                                <input class="form-control company-email" style="margin-top: 5px" type="text" name="company_email_{{$key}}" id="company_email_{{$key}}" data-key="{{$key}}" placeholder="{{trans('site.company email') . ($key + 1)}}" value="{{$emails[$key]}}">
                        @endforeach
                    </div>
                    <div class="form-group">
                        <label for="company_phone">{{trans('site.Company phone')}}</label>
                        <input class="form-control" type="text" name="company_phone" id="company_phone" value="{{$settings->company_phone}}">
                        <div class="alert alert-danger"></div>
                    </div>
                    <div class="form-group">
                        <label for="company_website">{{trans('site.Company website')}}</label>
                        <input class="form-control" type="text" name="company_website" id="company_website" value="{{$settings->company_website}}">
                        <div class="alert alert-danger"></div>
                    </div>
                    <div class="form-group">
                        <label for="company_fax">{{trans('site.Company fax')}}</label>
                        <input class="form-control" type="text" name="company_fax" id="company_fax" value="{{$settings->company_fax}}">
                        <div class="alert alert-danger"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('site.cancel')}}</button>
                    <input type="submit" class="btn btn-success" value="{{trans('site.save')}}">
                </div>
            </form>
        </div>
    </div>
@endsection
@section('footer_scripts')
    <script>


    </script>
@endsection