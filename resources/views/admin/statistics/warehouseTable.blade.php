<table id="example" class="display table-warehouseStats resp table table-striped table-bordered dataTable no-footer" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th>{{trans('site.No.')}}</th>
            <th>{{trans('site.name')}}</th>
            <th>{{trans('site.loader')}}</th>
            <th>{{trans('site.from')}}</th>
            <th>{{trans('site.to')}}</th>
            <th>{{trans('site.volume')}}</th>
            <th>{{trans('site.Date')}}</th>
        </tr>
    </thead>
    <tbody>
    @foreach($production as $product)
        <tr>
            <td>{{$loop->iteration}}</td>
            @if($product->worker_id != 0)
                <td>{{$product->worker->name}} {{$product->worker->surname}}</td>
            @else
                <td>{{trans('site.loader')}}</td>
            @endif
            <td>{{$product->loader->name}}</td>
            @if($product->type == 0)
                @if($product->workspace_id != 0)
                    <td>{{$product->workspace->name}}</td>
                @else
                    <td>{{trans('site.loader')}}</td>
                @endif
                <td>{{$product->warehouse->name}}</td>
            @else
                <td>{{$product->warehouse->name}}</td>
                @if($product->workspace_id != 0)
                    <td>{{$product->workspace->name}}</td>
                @else
                    <td>{{$product->ramp->name}}</td>
                @endif
            @endif
            <td>{{$product->volume}}</td>
            <td>{{$product->updated_at->format('Y-m-d')}}</td>
        </tr>
    @endforeach
    </tbody>
</table>
@section('footer_scripts')
    <script>
        $(document).ready(function() {
            var loadedProdText = "{{trans('site.Weeks loaders loaded production')}}";
            var unloadedProdText = "{{trans('site.Weeks loaders unloaded production')}}";
            var unloadedMatText = "{{trans('site.Weeks loaders unloaded materials')}}";
            var loadedMatText = "{{trans('site.Weeks loaders loaded materials')}}";
            var type1 = null;
            var type2 = null;
            var data1 = null;
            var data2 = null;
            var dateSelected = false;

            var data = [
                { m: "{{getWeek(11)}}", a: "{{productionWeek(11, $subject->id, $type)}}", b: "{{productionWeek(11, $subject->id, $type2)}}"},
                { m: "{{getWeek(10)}}", a: "{{productionWeek(10, $subject->id, $type)}}", b: "{{productionWeek(10, $subject->id, $type2)}}"},
                { m: "{{getWeek(9)}}", a: "{{productionWeek(9, $subject->id, $type)}}", b: "{{productionWeek(9, $subject->id, $type2)}}"},
                { m: "{{getWeek(8)}}", a: "{{productionWeek(8, $subject->id, $type)}}", b: "{{productionWeek(8, $subject->id, $type2)}}"},
                { m: "{{getWeek(7)}}", a: "{{productionWeek(7, $subject->id, $type)}}", b: "{{productionWeek(7, $subject->id, $type2)}}"},
                { m: "{{getWeek(6)}}", a: "{{productionWeek(6, $subject->id, $type)}}", b: "{{productionWeek(6, $subject->id, $type2)}}"},
                { m: "{{getWeek(5)}}", a: "{{productionWeek(5, $subject->id, $type)}}", b: "{{productionWeek(5, $subject->id, $type2)}}"},
                { m: "{{getWeek(4)}}", a: "{{productionWeek(4, $subject->id, $type)}}", b: "{{productionWeek(4, $subject->id, $type2)}}"},
                { m: "{{getWeek(3)}}", a: "{{productionWeek(3, $subject->id, $type)}}", b: "{{productionWeek(3, $subject->id, $type2)}}"},
                { m: "{{getWeek(2)}}", a: "{{productionWeek(2, $subject->id, $type)}}", b: "{{productionWeek(2, $subject->id, $type2)}}"},
                { m: "{{getWeek(1)}}", a: "{{productionWeek(1, $subject->id, $type)}}", b: "{{productionWeek(1, $subject->id, $type2)}}"},
                { m: "{{getWeek(0)}}", a: "{{productionWeek(0, $subject->id, $type)}}", b: "{{productionWeek(0, $subject->id, $type2)}}"}
            ];
            var config = {
                data: data,
                parseTime:false,
                xkey: 'm',
                ykeys: ['a', 'b'],
                labels: [loadedProdText, unloadedProdText],
                fillOpacity: 0.6,
                hideHover: 'auto',
                behaveLikeLine: true,
                resize: true,
                pointFillColors:['#ffffff'],
                pointStrokeColors: ['black'],
                lineColors:["rgba(38, 185, 154, 0.38)", "rgba(160, 20, 50, 0.38)"]
            };
            config.element = 'area-chart';
            var statIndex = Morris.Area(config);
            type1 = 3;
            type2 = 5;

            data = [
                { m: "{{getWeek(11)}}", a: "{{productionWeek(11, $subject->id, $type3)}}", b: "{{productionWeek(11, $subject->id, $type4)}}"},
                { m: "{{getWeek(10)}}", a: "{{productionWeek(10, $subject->id, $type3)}}", b: "{{productionWeek(10, $subject->id, $type4)}}"},
                { m: "{{getWeek(9)}}", a: "{{productionWeek(9, $subject->id, $type3)}}", b: "{{productionWeek(9, $subject->id, $type4)}}"},
                { m: "{{getWeek(8)}}", a: "{{productionWeek(8, $subject->id, $type3)}}", b: "{{productionWeek(8, $subject->id, $type4)}}"},
                { m: "{{getWeek(7)}}", a: "{{productionWeek(7, $subject->id, $type3)}}", b: "{{productionWeek(7, $subject->id, $type4)}}"},
                { m: "{{getWeek(6)}}", a: "{{productionWeek(6, $subject->id, $type3)}}", b: "{{productionWeek(6, $subject->id, $type4)}}"},
                { m: "{{getWeek(5)}}", a: "{{productionWeek(5, $subject->id, $type3)}}", b: "{{productionWeek(5, $subject->id, $type4)}}"},
                { m: "{{getWeek(4)}}", a: "{{productionWeek(4, $subject->id, $type3)}}", b: "{{productionWeek(4, $subject->id, $type4)}}"},
                { m: "{{getWeek(3)}}", a: "{{productionWeek(3, $subject->id, $type3)}}", b: "{{productionWeek(3, $subject->id, $type4)}}"},
                { m: "{{getWeek(2)}}", a: "{{productionWeek(2, $subject->id, $type3)}}", b: "{{productionWeek(2, $subject->id, $type4)}}"},
                { m: "{{getWeek(1)}}", a: "{{productionWeek(1, $subject->id, $type3)}}", b: "{{productionWeek(1, $subject->id, $type4)}}"},
                { m: "{{getWeek(0)}}", a: "{{productionWeek(0, $subject->id, $type3)}}", b: "{{productionWeek(0, $subject->id, $type4)}}"}
            ];
            config = {
                data: data,
                parseTime:false,
                xkey: 'm',
                ykeys: ['a', 'b'],
                labels: [unloadedMatText, loadedMatText],
                fillOpacity: 0.6,
                hideHover: 'auto',
                behaveLikeLine: true,
                resize: true,
                responsive: true,
                pointFillColors:['#ffffff'],
                pointStrokeColors: ['black'],
                lineColors:["rgba(38, 185, 154, 0.38)", "rgba(160, 20, 50, 0.38)"]
            };
            config.element = 'material-area-chart';
            var statIndex2 = Morris.Area(config);
            var type3 = 4;
            var type4 = 6;

            $('#tab-2').hide();

            $('#mat-tab').on('click', function(){
                $('#tab-1').hide();
                $('#tab-2').fadeIn(400);
                if(dateSelected == true){
                    statIndex.setData(data1);
                    statIndex2.setData(data2);
                }
            });

            $('#prod-tab').on('click', function(){
                $('#tab-2').hide();
                $('#tab-1').fadeIn(400);
                if(dateSelected == true){
                    statIndex.setData(data1);
                    statIndex2.setData(data2);
                }
            });

            //Statistics range picker
            $('.single_cal1-3').daterangepicker({
                singleDatePicker: false,
                singleClasses: "picker_1",
                startDate:"{{$weekDate}}",
                locale: {
                    format: 'YYYY-MM-DD',
                    applyLabel: "{{trans('site.Apply')}}",
                    cancelLabel: "{{trans('site.Cancel')}}"
                }
            }, function(start, end) {
                var table = $('#example').DataTable();
                var url = window.location.href;
                var startDate = start.format('WW');
                var endDate = end.format('WW');

                $.ajax({
                    type: "GET",
                    url: url,
                    data: {test:'true', start:startDate, end:endDate, type2:type2, type1:type1, type3:type3, type4:type4},
                    success: function(allData)
                    {
                        statIndex.setData(allData['data1']);
                        statIndex2.setData(allData['data2']);
                        dateSelected = true;
                        data1 = allData['data1'];
                        data2 = allData['data2'];

                        $.fn.dataTable.ext.search.push(function( settings, data){
                            var min = start.format('YYYY-MM-DD');
                            var max = end.format('YYYY-MM-DD');
                            var age = data.slice(-1)[0]; //Date column index

                            return ( min <= age && age <= max );
                        });

                        table.draw();
                        $.fn.dataTable.ext.search.pop();
                    }
                });

            });
        })
    </script>
@endsection