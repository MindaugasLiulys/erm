@extends('layout')

@section('main_container')
    <div class="right_col" role="main">
        <div class="row tile_count">
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count no-border1">
                <span class="count_top"><i class="fa fa-user"></i> {{trans('site.Total workers')}}</span>
                <div class="count">{{count($workers)}}</div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-clock-o"></i> {{trans('site.This months production')}}</span>
                <div class="count">{{productionMonth(0)}}m<sup>3</sup></div>
                <span class="count_bottom">
                    @if(percentDifference(productionMonth(0), productionMonth(1)) > 0)
                        <i class="green"><i class="fa fa-sort-asc"></i>
                    @else
                        <i class="red"><i class="fa fa-sort-desc"></i>
                    @endif
                        {{percentDifference(productionMonth(0), productionMonth(1))}}
                        %</i> {{trans('site.From last month')}}
                </span>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count no-border3">
                <span class="count_top"><i class="fa fa-user"></i> {{trans('site.Total warehouse space')}}</span>
                <div class="count">{{$warehouseUsed}}m<sup>3</sup></div>
                <span class="count_bottom">{{$warehouseOccup}}% {{trans('site.Occupied')}}</span>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count no-border4">
                <span class="count_top"><i class="fa fa-user"></i> {{trans('site.Most efficient workspace')}}</span>
                <div class="count">
                    @if(!empty($items))
                        {{$items[$highest]['produced']}}m<sup>3</sup>
                    @else
                       0m<sup>3</sup>
                    @endif
                </div>
                <span>
                     @if(!empty($items))
                        {{$items[$highest]['name']}}
                    @else
                        {{trans('site.Not enough data')}}
                    @endif
                </span>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count no-border5">
                <span class="count_top"><i class="fa fa-user"></i> {{trans('site.Least efficient workspace')}}</span>
                <div class="count">
                    @if(!empty($items))
                        {{$items[$lowest]['produced']}}m<sup>3</sup>
                    @else
                        0m<sup>3</sup>
                    @endif
                </div>
                <span>
                    @if(!empty($items))
                        {{$items[$lowest]['name']}}
                    @else
                        {{trans('site.Not enough data')}}
                    @endif
                </span>
            </div>
        </div>
        <div class="col-md-8 col-sm-8 col-xs-12 pull-right text-right">
            <fieldset class="col-md-12 col-sm-12 col-xs-12">
                <div class="control-group">
                    <div class="controls">
                        <div class="col-md-12  xdisplay_inputx form-group has-feedback" >
                            <div class="col-md-5 text-right">
                                <label style="padding-top: 6px">{{trans('site.Select date')}}:</label>
                            </div>
                            <div class="col-md-7">
                                <input type="text" class="form-control has-feedback-left single_cal1-2" placeholder="{{trans('site.Select date')}}" aria-describedby="inputSuccess2Status">
                                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="dashboard_graph">
                    <div class="row x_title">
                        <div class="col-md-6">
                            <h3>{{trans('site.Production statistics')}} <small>({{trans('site.12-month')}})</small></h3>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div id="area-chart"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <h1>{{trans('site.workspaces')}}</h1>
        @if(count($workspacesAll) >= 1)
            @foreach($workspacesAll->chunk(3) as $workspace1)
                <div class="row">
                    @foreach($workspace1 as $workspace)
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="x_panel tile overflow_hidden">
                                <div class="x_title">
                                    <h3 class="block-title-fit" style="font-size: 14px">
                                        <span class="rand-color stroke" style="background-color: {{$workspace->color}}">
                                            {{$workspace->name}}
                                        </span>
                                    </h3>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <a href="{{route('workspace-statistics', $workspace->id)}}" class="btn btn-info btn-block">{{trans('site.Statistics')}}</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            @endforeach
        @else
            <h4>{{trans('site.workspaces not found')}}</h4>
        @endif<hr>
        <h1>{{trans('site.loaders')}}</h1>
        @if(count($loaders) >= 1)
            @foreach($loaders->chunk(3) as $loaderChunk)
                <div class="row">
                    @foreach($loaderChunk as $loader)
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="x_panel tile">
                                <div class="x_title">
                                    <h3 style="font-size: 14px">
                                        <span class="rand-color stroke" style="background-color: {{$loader->color}}">
                                            {{$loader->name}}
                                        </span>
                                    </h3>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <a href="{{route('loader-statistics', $loader->id)}}" class="btn btn-info btn-block">{{trans('site.Statistics')}}</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            @endforeach
        @else
            <h4>{{trans('site.loaders not found')}}</h4>
        @endif<hr>


        <h1>{{trans('site.warehouses')}}</h1>
        @if(count($warehouses) >= 1)
            @foreach($warehouses->chunk(3) as $warehouseChunk)
                <div class="row">
                    @foreach($warehouseChunk as $warehouse)
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="x_panel tile">
                                <div class="x_title">
                                    <h3 style="font-size: 14px">
                                        <span class="rand-color stroke" style="background-color: {{$warehouse->color}}">
                                            {{$warehouse->name}}
                                        </span>
                                    </h3>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <a href="{{route('warehouse-statistics', $warehouse->id)}}" class="btn btn-info btn-block">{{trans('site.Statistics')}}</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            @endforeach
        @else
            <h4>{{trans('site.loaders not found')}}</h4>
        @endif<hr>


        <h1>{{trans('site.workers')}}</h1>
        <table id="example" class="display table-worker resp table table-striped table-bordered dataTable no-footer" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th>{{trans('site.No.')}}</th>
                    <th>{{trans('site.username')}}</th>
                    <th>{{trans('site.name')}}</th>
                    <th>{{trans('site.surname')}}</th>
                    <th>{{trans('site.position')}}</th>
                    <th>{{trans('site.workspace')}}</th>
                    <th>{{trans('site.action')}}</th>
                </tr>
            </thead>
            <tbody>
            @foreach($workers as $worker)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$worker->worker->user->username}}</td>
                    <td>{{$worker->worker->name}}</td>
                    <td>{{$worker->worker->surname}}</td>
                    <td>@if(!empty($worker->worker->position->name))
                            {{$worker->worker->position->name}}
                        @else
                            {{trans('site.no position found')}}
                        @endif
                    </td>
                    <td>{{$worker->workspace->name}}</td>
                    <td>
                        <a href="{{route('worker-statistics', $worker->id)}}" class="btn btn-info worker-stat"><i class="fa fa-bar-chart" aria-hidden="true"></i></a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
@section('footer_scripts')
    <script>
        $(document).ready(function() {
            var productionText = "{{trans('site.Months production')}}";
            var data = [
                        { m: "{{getMonth(0)}}", a: "{{productionMonth(0)}}"},
                        { m: "{{getMonth(1)}}", a: "{{productionMonth(1)}}"},
                        { m: "{{getMonth(2)}}", a: "{{productionMonth(2)}}"},
                        { m: "{{getMonth(3)}}", a: "{{productionMonth(3)}}"},
                        { m: "{{getMonth(4)}}", a: "{{productionMonth(4)}}"},
                        { m: "{{getMonth(5)}}", a: "{{productionMonth(5)}}"},
                        { m: "{{getMonth(6)}}", a: "{{productionMonth(6)}}"},
                        { m: "{{getMonth(7)}}", a: "{{productionMonth(7)}}"},
                        { m: "{{getMonth(8)}}", a: "{{productionMonth(8)}}"},
                        { m: "{{getMonth(9)}}", a: "{{productionMonth(9)}}"},
                        { m: "{{getMonth(10)}}", a: "{{productionMonth(10)}}"},
                        { m: "{{getMonth(11)}}", a: "{{productionMonth(11)}}"}
                    ]
                    ,
                    config = {
                        data: data,
                        xkey: 'm',
                        ykeys: ['a'],
                        labels: [productionText],
                        fillOpacity: 0.6,
                        hideHover: 'auto',
                        behaveLikeLine: true,
                        resize: true,
                        pointFillColors:['#ffffff'],
                        pointStrokeColors: ['black'],
                        lineColors:["rgba(38, 185, 154, 0.38)"]
                    };

            config.element = 'area-chart';
            var statIndex = Morris.Area(config);
            //Statistics range picker
            $('.single_cal1-2').daterangepicker({
                singleDatePicker: false,
                singleClasses: "picker_1",
                startDate:"{{getMonth(11)}}",
                locale: {
                    format: 'YYYY-MM-DD',
                    applyLabel: "{{trans('site.Apply')}}",
                    cancelLabel: "{{trans('site.Cancel')}}"
                }
            }, function(start, end) {
                var url = window.location.href;
                var startDate = start.format('YYYY-MM-DD');
                var endDate = end.format('YYYY-MM-DD');

                $.ajax({
                    type: "GET",
                    url: url,
                    data: {test:'true', start:startDate, end:endDate},
                    success: function(data)
                    {
                        console.log(data);
                        statIndex.setData(data)
                    }
                });
            });
        })
    </script>
@endsection
