<table id="example" class="display table-workspStats resp table table-striped table-bordered dataTable no-footer" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th>{{trans('site.No.')}}</th>
            <th>{{trans('site.name')}}</th>
            <th>{{trans('site.surname')}}</th>
            <th>{{trans('site.Position')}}</th>
            <th>{{trans('site.Day start')}}</th>
            <th>{{trans('site.Day end')}}</th>
            <th>{{trans('site.Produced')}}</th>
            <th>{{trans('site.Date')}}</th>
        </tr>
    </thead>
    <tbody>
    @foreach($production as $product)
        <tr>
            <td>{{$loop->iteration}}</td>
            <td>{{$product->worker->name}}</td>
            <td>{{$product->worker->surname}}</td>
            <td>{{$product->worker->position->name}}</td>
            <td>{{$product->day_start}}</td>
            <td>{{$product->day_end}}</td>
            <td>{{$product->unloaded-$product->day_start+$product->day_end}}</td>
            <td>{{$product->updated_at->format('Y-m-d')}}</td>
        </tr>
    @endforeach
    </tbody>
</table>
@section('footer_scripts')
    <script>
        $(document).ready(function() {
            var productionText = "{{trans('site.Weeks production')}}";

            var data = [
                        { m: "{{getWeek(11)}}", a: "{{productionWeek(11, $subject->id, $type)}}"},
                        { m: "{{getWeek(10)}}", a: "{{productionWeek(10, $subject->id, $type)}}"},
                        { m: "{{getWeek(9)}}", a: "{{productionWeek(9, $subject->id, $type)}}"},
                        { m: "{{getWeek(8)}}", a: "{{productionWeek(8, $subject->id, $type)}}"},
                        { m: "{{getWeek(7)}}", a: "{{productionWeek(7, $subject->id, $type)}}"},
                        { m: "{{getWeek(6)}}", a: "{{productionWeek(6, $subject->id, $type)}}"},
                        { m: "{{getWeek(5)}}", a: "{{productionWeek(5, $subject->id, $type)}}"},
                        { m: "{{getWeek(4)}}", a: "{{productionWeek(4, $subject->id, $type)}}"},
                        { m: "{{getWeek(3)}}", a: "{{productionWeek(3, $subject->id, $type)}}"},
                        { m: "{{getWeek(2)}}", a: "{{productionWeek(2, $subject->id, $type)}}"},
                        { m: "{{getWeek(1)}}", a: "{{productionWeek(1, $subject->id, $type)}}"},
                        { m: "{{getWeek(0)}}", a: "{{productionWeek(0, $subject->id, $type)}}"}
                    ],
                    config = {
                        data: data,
                        parseTime:false,
                        xkey: 'm',
                        ykeys: ['a'],
                        labels: [productionText],
                        fillOpacity: 0.6,
                        hideHover: 'auto',
                        behaveLikeLine: true,
                        resize: true,
                        pointFillColors:['#ffffff'],
                        pointStrokeColors: ['black'],
                        lineColors:["rgba(38, 185, 154, 0.38)"]
                    };
            config.element = 'area-chart';
            var statIndex = Morris.Area(config);

            //Statistics range picker
            $('.single_cal1-3').daterangepicker({
                singleDatePicker: false,
                singleClasses: "picker_1",
                startDate:"{{$weekDate}}",
                locale: {
                    format: 'YYYY-MM-DD',
                    applyLabel: "{{trans('site.Apply')}}",
                    cancelLabel: "{{trans('site.Cancel')}}"
                }
            }, function(start, end) {
                var table = $('#example').DataTable();
                var url = window.location.href;
                var startDate = start.format('WW');
                var endDate = end.format('WW');

                $.ajax({
                    type: "GET",
                    url: url,
                    data: {test:'true', start:startDate, end:endDate},
                    success: function(data)
                    {
                        statIndex.setData(data);

                        $.fn.dataTable.ext.search.push(function(settings, data){
                            var min = start.format('YYYY-MM-DD');
                            var max = end.format('YYYY-MM-DD');
                            var age = data.slice(-1)[0]; //Date column index

                            return ( min <= age && age <= max );
                        });
                        table.draw();
                        $.fn.dataTable.ext.search.pop();
                    }
                });

            });
        })
    </script>
@endsection