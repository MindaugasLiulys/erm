@extends('layout')

@section('main_container')
    <div class="right_col" role="main">
        <h1>{{$subject->name}} @if(!empty($subject->surname)){{$subject->surname}} @endif{{trans('site.statistics')}}</h1>
        <div class="row tile_count">
            @if(!empty($workers))
                <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count no-border1">
                    <span class="count_top"><i class="fa fa-user"></i> {{trans('site.Total workers')}}</span>
                    <div class="count">{{count($workers)}}</div>
                </div>
            @endif()
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count no-border1">
                <span class="count_top"><i class="fa fa-clock-o"></i> {{trans('site.This months production')}}</span>
                <div class="count">{{workspaceMonthsProd(0, $subject->id, $type)}}m<sup>3</sup></div>
                <span class="count_bottom">
                    @if(percentDifference(workspaceMonthsProd(0, $subject->id, $type), workspaceMonthsProd(1, $subject->id, $type)) > 0)
                        <i class="green"><i class="fa fa-sort-asc"></i>
                    @else
                        <i class="red"><i class="fa fa-sort-desc"></i>
                    @endif
                    {{percentDifference(workspaceMonthsProd(0, $subject->id, $type), workspaceMonthsProd(1, $subject->id, $type))}}%
                        </i> {{trans('site.From last month')}}
                </span>
            </div>
        </div>
        <fieldset class="col-md-12 col-sm-12 col-xs-12">
            <div class="control-group">
                <div class="controls">
                    <div class="col-md-12  xdisplay_inputx form-group has-feedback" >
                        <div class="col-md-5 text-right">
                            <label style="padding-top: 6px">{{trans('site.Select date')}}:</label>
                        </div>
                        <div class="col-md-7">
                            <input type="text" class="form-control has-feedback-left single_cal1-3" placeholder="{{trans('site.Select date')}}" aria-describedby="inputSuccess2Status">
                            <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                        </div>
                    </div>
                </div>
            </div>
        </fieldset>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="dashboard_graph">
                    <div class="row x_title">
                        <div class="col-md-6">
                            <h3>
                                @if($type == 3 || $type == 1)
                                    {{trans('site.Statistics')}}
                                @else
                                    {{trans('site.Production statistics')}}
                                @endif
                                <small>({{trans('site.12-weeks')}})</small>
                            </h3>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        @if($type == 3 || $type == 1)
                            <div class="x_content">
                                <div class="" role="tabpanel" data-example-id="togglable-tabs">
                                    <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                        <li role="presentation" class="active">
                                            <a href="#tab-1" id="prod-tab" role="tab" data-toggle="tab" aria-expanded="true">{{trans('site.production')}}</a>
                                        </li>
                                        <li role="presentation" class="">
                                            <a href="#tab-2" role="tab" id="mat-tab" data-toggle="tab" aria-expanded="false">{{trans('site.materials')}}</a>
                                        </li>
                                    </ul>
                                    <div id="myTabContent" class="tab-content">
                                        <div class="stat-tab" id="tab-1">
                                            <div id="area-chart"></div>
                                        </div>
                                        <div class="stat-tab" id="tab-2">
                                            <div id="material-area-chart"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @else
                            <div id="area-chart"></div>
                        @endif
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="row x_title">
                    <div class="col-md-6">
                        <h2>
                            @if($type == 0)
                                {{trans('site.Workspace production')}}
                            @elseif($type == 1)
                                {{trans('site.Loaders unloaded volume')}}
                            @elseif($type == 2)
                                {{trans('site.Worker production')}}
                            @else
                                {{trans('site.warehouse')}}
                            @endif
                        </h2>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    @if($type == 0)
                        @include('admin.statistics.workspaceTable')
                    @elseif($type == 1)
                        @include('admin.statistics.forkliftTable')
                    @elseif($type == 2)
                        @include('admin.statistics.workerTable')
                    @elseif($type == 3)
                        @include('admin.statistics.warehouseTable')
                    @endif
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@endsection