@extends('layout')

@section('main_container')

    <div class="right_col" role="main">
        <h1>{{trans('site.edit_position')}}</h1><hr>
        <form action="{{url('admin/positions/edit/' . $position->id)}}" class="create-new-form validate-form" method="POST">
            {!! csrf_field() !!}
            <div class="form-group">
                <label class="control-label" for="position">{{trans('site.position')}}</label>
                <input type="text" id="position" name="position" class="form-control validate-input" value="{{$position->name}}">
                <div class="alert alert-danger"></div>
            </div>
            <div class="form-group">
                <input type="checkbox" name="active" value="1" {{($position->active == 1)?"checked=checked" : '' }}> <span style="font-weight: 700">{{trans('site.active')}}?</span>
            </div>
            <div class="form-group">
                <a href="{{route('positions')}}" class="btn btn-default">{{trans('site.cancel')}}</a>
                <input class="btn btn-success" type="submit" value="{{trans('site.save')}}">
            </div>
        </form>
    </div>
@endsection