@extends('layout')

@section('main_container')

<div class="right_col" role="main">
    <h1>{{trans('site.positions')}}</h1>
    <a class="btn btn-primary" href="{{route('create-position')}}"><i class="fa fa-plus" aria-hidden="true"></i> {{trans('site.create_position')}}</a><hr>
    <table id="example" class="display resp table-position table table-striped table-bordered dataTable no-footer" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>{{trans('site.No.')}}</th>
                <th>{{trans('site.positions')}}</th>
                <th>{{trans('site.active')}}</th>
                <th>{{trans('site.action')}}</th>
            </tr>
        </thead>
        <tbody>
            @foreach($positions as $position)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$position->name}}</td>
                    <td>@if($position->active != 0)
                            <span class="green">{{trans('site.YES')}}</span>
                        @else
                            <span class="red">{{trans('site.NO')}}</span>
                        @endif
                    </td>
                    <td>
                        <a href="{{url('admin/positions/edit/' . $position->id)}}" class="btn btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                        <a class="btn btn-danger" data-action="{{url('admin/position/delete/' . $position->id)}}" data-method="delete"><i class="fa fa-trash" aria-hidden="true"></i></a>
                    </td>
                </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <th>{{trans('site.No.')}}</th>
                <th>{{trans('site.positions')}}</th>
                <th>{{trans('site.active')}}</th>
                <th>{{trans('site.action')}}</th>
            </tr>
        </tfoot>
    </table>
</div>

@include('admin.modals.deleteModal')
@endsection