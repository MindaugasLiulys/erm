@extends('layout')

@section('main_container')
    <div class="right_col" role="main">
        <h1>{{trans('site.Orders')}}</h1>
        <div class="col-md-2">
            <a class="btn btn-primary order-modal"><i class="fa fa-plus" aria-hidden="true"></i> {{trans('site.create new order')}}</a>
        </div>
        <div class="clearfix"></div><hr>
        <div class="workspaces-body">
            @if(count($workspaces) >= 1)
                @foreach($workspaces->chunk(3) as $workspace1)
                    <div class="row">
                        @foreach($workspace1 as $workspace)
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <div class="x_panel tile overflow_hidden">
                                    <div class="x_title">
                                        <h2 class="block-title-fit" style="font-size: 16px">
                                            <span class="rand-color stroke" style="background-color: {{$workspace->color}}">
                                                {{$loop->iteration}}. {{$workspace->name}}
                                            </span>
                                        </h2>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                        <ul class="sortable">
                                            <?php $serial = 0 ?>
                                            @foreach($orders as $order)
                                                @if($order->workspace_id == $workspace->id && $serial < 5)
                                                    <li id="item-{{$order->id}}" class="item-sort" data-position="{{$loop->iteration}}">
                                                        {{displayStatusIcon($order->status)}} {{trans('site.Order ID')}}: {{$order->order_number}}
                                                        <span class="pull-right">
                                                            <span class="red" style="padding-right: 10px">{{trans('site.Deadline')}}: {{convertDateFormat($order->deadline)->format('Y-m-d H:i')}}</span>
                                                            <span style="padding-right: 5px">
                                                                <a class="order-modal-edit" style="cursor: pointer"
                                                                   data-action="{{route('order-edit', $order->id)}}"
                                                                   data-order="{{$order->order_number}}"
                                                                   data-workspace="{{$order->workspace->id}}"
                                                                   data-product="{{$order->product_id}}"
                                                                   data-dimensions="{{$order->dimensions}}"
                                                                   data-quantity="{{$order->target_quantity}}"
                                                                   data-heat="{{$order->heat_no}}"
                                                                   data-deadline="{{convertDateFormat($order->deadline)->format('Y-m-d H:i')}}"
                                                                   data-notes="{{$order->order_notes}}"
                                                                   data-status="{{$order->status}}">
                                                                    <i class="fa fa-edit" aria-hidden="true"></i>
                                                                </a>
                                                            </span>
                                                            <span style="padding-right: 5px">
                                                                <a class="red delete-order" style="cursor: pointer" data-method="delete" data-action="{{route('order-delete', $order->id)}}"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                                            </span>
                                                            <span style="padding-right: 5px">
                                                                <a href="{{route('order-pdf', $order->id)}}" style="cursor: pointer"><i class="fa fa-print" aria-hidden="true"></i></a>
                                                            </span>
                                                            <span><i class="fa fa-arrows-v" aria-hidden="true"></i></span>
                                                        </span>
                                                    </li>
                                                    <?php $serial++ ?>
                                                @endif
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endforeach
            @else
                <h4>{{trans('site.workspaces not found')}}</h4>
            @endif
            <table id="example" class="display table-worker resp table table-striped table-bordered dataTable no-footer" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>{{trans('site.No.')}}</th>
                        <th>{{trans('site.Order ID')}}</th>
                        <th>{{trans('site.workspace')}}</th>
                        <th>{{trans('site.Product')}}</th>
                        <th>{{trans('site.Dimensions')}}</th>
                        <th>{{trans('site.Quantity made')}}</th>
                        <th>{{trans('site.Target quantity')}}</th>
                        <th>{{trans('site.Heat No.')}}</th>
                        <th>{{trans('site.Order notes')}}</th>
                        <th>{{trans('site.Deadline')}}</th>
                        <th>{{trans('site.Created')}}</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($ordersTable as $order)
                    <tr>
                        <td>{{$loop->iteration}}.</td>
                        <td>{{$order->order_number}}</td>
                        <td>{{$order->workspace->name}}</td>
                        <td>{{$order->product->name}}</td>
                        <td>{{$order->dimensions}}</td>
                        <td>{{$order->quantity_made}}</td>
                        <td>{{$order->target_quantity}}</td>
                        <td>{{$order->heat_no}}</td>
                        <td>{{$order->order_notes}}</td>
                        <td style="background: rgba(255, 12, 0, 0.04)">{{$order->deadline}}</td>
                        <td>{{$order->created_at->format('Y-m-d H:i')}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @include('admin.modals.deleteModal')
    @include('admin.modals.editOrderModal')
    @include('admin.modals.createOrderModal')
@endsection
@section('footer_scripts')
    <script>
        $(function() {
            $('.sortable').sortable({
                axis: 'y',
                update: function (event, ui) {
                    var data = $(this).sortable('serialize');
                    var workspace = $(this).find('a').data('workspace');

                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': "{{ csrf_token() }}"
                        },
                        data: data + '&workspace=' + workspace,
                        type: 'POST',
                        url: "{{url()->current()}}",
                        success: function(){
                            init_PNotify_fade("Position updated succesfully", '', 'success');
                        }
                    });
                }
            });
        });

    </script>
@endsection