<style>
    *{
        font-family: DejaVu Sans !important;
    }

    .invoice-box {
        max-width: 800px;
        margin: auto;
        padding: 30px;
        font-size: 16px;
        line-height: 24px;
        color: #555;
    }

    .invoice-box table{
        width:100%;
        border-collapse: collapse;
        text-align:left;
    }

    .invoice-box table td{
        padding:5px;
        vertical-align:top;
    }

    .invoice-box tr.tr-border td {
        padding: 20px 5px;
    }

    .invoice-box table tr td:nth-child(2){
        text-align:right;
    }

    .invoice-box table tr.tr-border td{
        border-bottom: 1px solid #eee;
    }

    .invoice-box table tr.top table td{
        padding-bottom:20px;
    }

    .invoice-box table.order-data th{
        border-bottom: 1px solid #eee;
        padding: 10px 10px;
    }

    .invoice-box table.order-data thead tr{
        background: #eee;
    }

    .invoice-box table.order-data tbody td{
        border-bottom: 1px solid #eee;
        padding: 7px 10px;
    }

    .invoice-box table.order-data th:last-child{
        text-align: right;
    }

    .invoice-box table .tr-border .company-name{
        font-size: 30px;
    }

    .invoice-box table .tr-border .company-address,
    .invoice-box table .tr-border .company-email,
    .invoice-box table .tr-border .company-phone,
    .invoice-box table .tr-border .company-fax,
    .invoice-box table .tr-border .company-website
    {
        font-size: 14px;
    }

</style>
<div class="invoice-box">
    <table cellpadding="0" cellspacing="0">
        <tr class="information">
            <td colspan="2">
                <table>
                    <tr class="tr-border">
                        <td>
                            <span class="company-name">{{$settings->company_name}}</span><br>
                            <span class="company-address">
                                <strong>{{trans('site.Address')}}:</strong>
                                @foreach($settingsAddress as $address)
                                    {{$address}}<br>
                                @endforeach
                            </span>
                            <span class="company-email">
                                <strong>{{trans('site.Email')}}:</strong>
                                @foreach($emails as $email)
                                    {{$email}}<br>
                                @endforeach
                            </span>
                            @if(!empty($settings->company_phone))
                                <span class="company-phone">
                                    <strong>{{trans('site.Phone')}}:</strong>
                                    {{$settings->company_phone}}
                                </span><br>
                            @endif
                            @if(!empty($settings->company_fax))
                                <span class="company-fax">
                                    <strong>{{trans('site.Fax')}}:</strong>
                                    {{$settings->company_fax}}
                                </span><br>
                            @endif
                            @if(!empty($settings->company_website))
                                <span class="company-website">
                                    <strong>{{trans('site.Website')}}:</strong>
                                    {{$settings->company_website}}
                                </span>
                            @endif
                        </td>
                        <td>
                            <strong>{{trans('site.Order ID')}}:</strong> {{$order->order_number}}<br>
                            <strong>{{trans('site.Date')}}:</strong> {{\Carbon\Carbon::now()->format('Y-m-d')}}<br>
                        </td>
                    </tr>
                </table>
                <table class="order-data">
                    <thead>
                        <tr>
                            <th>{{trans('site.Description')}}</th>
                            <th>{{trans('site.Order data')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{trans('site.workspace')}}</td>
                            <td>{{$order->workspace->name}}</td>
                        </tr>
                        <tr>
                            <td>{{trans('site.Product')}}</td>
                            <td>{{$order->product->name}}</td>
                        </tr>

                        <tr>
                            <td>{{trans('site.Dimensions')}}</td>
                            <td>{{$order->dimensions}}</td>
                        </tr>
                        <tr>
                            <td>{{trans('site.Quantity made')}}</td>
                            <td>{{$order->quantity_made}}</td>
                        </tr>
                        <tr>
                            <td>{{trans('site.Target quantity')}}</td>
                            <td>{{$order->target_quantity}}</td>
                        </tr>
                        <tr>
                            <td>{{trans('site.Heat No.')}}</td>
                            <td>{{$order->heat_no}}</td>
                        </tr>
                        <tr>
                            <td>{{trans('site.Deadline')}}</td>
                            <td>{{convertDateFormat($order->deadline)->format('Y-m-d H:i')}}</td>
                        </tr>
                        <tr>
                            <td>{{trans('site.Order notes')}}</td>
                            <td>{{$order->order_notes}}</td>
                        </tr>
                    </tbody>
                {{--{{trans('site.PDF error')}}--}}
                {{-- Support company email / website, can assign in settings --}}
                </table>
            </td>
        </tr>
    </table>
</div>

