@extends('layout')

@section('main_container')
    <div class="right_col">
        <div class="row">
            <form action="{{url('admin/products/edit/' . $product->id)}}" class="validate-form"
                  method="POST">
                {!! csrf_field() !!}
                <div class="col-xs-12 col-md-5">
                    <h1>{{trans('site.Edit product')}}</h1>
                    <hr>
                    <div class="form-group">
                        <label class="control-label" for="name">{{trans('site.Product')}}</label>
                        <input type="text" id="name" name="name" class="form-control validate-input"
                               value="{{$product->name}}">
                        <div class="alert alert-danger"></div>
                    </div>
                    @if(!empty($materialNotAdded))
                        <br>
                        <h1>{{trans('site.add_materials_to_product')}}</h1>
                        <hr>
                        <div class="table-responsive" style="max-height: 360px; overflow-y: auto;">
                            <table class="table table-striped table-bordered table-hover">
                                <tr>
                                    <th class="text-center">{{trans('site.material')}}</th>
                                    <th class="text-center">{{trans('site.add')}}</th>
                                </tr>
                                @foreach ($materialNotAdded as $material)
                                    <tr>
                                        <td>
                                            {{ Html::link(route('admin.materials.edit', $material['id']), $material['name']) }}
                                        </td>
                                        <td class="text-center">
                                            {{ Form::checkbox('materials[]', $material['id'], in_array($material['id'], $product->materials->pluck('id')->toArray())) }}
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    @endif
                    <div class="form-group">
                        <a href="{{route('products')}}" class="btn btn-default">{{trans('site.cancel')}}</a>
                        <input class="btn btn-success" type="submit" value="{{trans('site.save')}}">
                    </div>
                </div>
                <div class="col-xs-12 col-md-7">
                    <h1>{{trans('site.product_materials')}}</h1>
                    <hr>
                    <div class="workspaces-body">
                        <table class="table table-striped table-bordered table-hover" width="100%" cellspacing="0">
                            <tr>
                                <th class="text-center">{{trans('site.material')}}</th>
                                <th class="text-center">{{trans('site.add')}}</th>
                            </tr>
                            @foreach($materials as $material)
                                <tr>
                                    <td>
                                        {{ Html::link(route('admin.materials.edit', $material['id']), $material['name']) }}
                                    </td>
                                    <td class="text-center">
                                        {{ Form::checkbox('materials[]', $material['id'], in_array($material['id'], $product->materials->pluck('id')->toArray())) }}
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </form>
        </div>
    </div>


@endsection