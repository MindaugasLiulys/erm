@extends('layout')

@section('main_container')
    <div class="right_col" role="main">
        <h1>{{trans('site.products')}}</h1>
        <a class="btn btn-primary" href="{{route('products-create')}}"><i class="fa fa-plus" aria-hidden="true"></i> {{trans('site.create product')}}</a><hr>
        <div class="workspaces-body">
            <table id="example" class="display resp table-products table table-striped table-bordered dataTable no-footer" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>{{trans('site.No.')}}</th>
                        <th>{{trans('site.products')}}</th>
                        <th>{{trans('site.action')}}</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($products as $product)
                    <tr>
                        <td>{{$number++}}</td>
                        <td>{{$product->name}}</td>

                        <td>
                            <a href="{{url('admin/products/edit/' . $product->id)}}" class="btn btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                            <a class="btn btn-danger" data-action="{{url('admin/products/delete/' . $product->id)}}" data-method="delete"><i class="fa fa-trash" aria-hidden="true"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>{{trans('site.No.')}}</th>
                        <th>{{trans('site.products')}}</th>
                        <th>{{trans('site.action')}}</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
@include('admin.modals.deleteModal')
@endsection