@extends('layout')

@section('main_container')
    <div class="right_col" role="main">
        <h1>{{trans('site.products')}}</h1>
        <hr>
        <div class="workspaces-body">
            <form action="{{route('products-create-new')}}" class="create-new-form validate-form" method="POST">
                {!! csrf_field() !!}
                <div class="form-group">
                    <label class="control-label" for="name">{{trans('site.Product')}}</label>
                    <input type="text" id="name" name="name" class="form-control validate-input">
                    <div class="alert alert-danger"></div>
                </div><br>
                <h1>{{trans('site.add_materials_to_product')}}</h1><hr>
                <div class="table-responsive" style="max-height: 360px; overflow-y: auto;">
                    <table class="table table-striped table-bordered table-hover">
                        <tr>
                            <th class="text-center">{{trans('site.material')}}</th>
                            <th class="text-center">{{trans('site.add')}}</th>
                        </tr>
                        @foreach ($allMaterials as $material)
                            <tr>
                                <td>
                                    <a href="{{route('admin.materials.edit', $material['id']), $material['name']}}">{{$material->name}}</a>
                                </td>
                                <td class="text-center">
                                    {{ Form::checkbox('materials[]', $material->id) }}
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
                <div class="form-group">
                    <a href="{{route('products')}}" class="btn btn-default">{{trans('site.cancel')}}</a>
                    <input class="btn btn-success" type="submit" value="{{trans('site.create')}}">
                </div>
            </form>
        </div>
    </div>
@endsection