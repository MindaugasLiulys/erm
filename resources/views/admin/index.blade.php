@extends('layout')

@section('main_container')
    <div class="right_col" role="main">
        <div class="row tile_count">
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count no-border1">
                <span class="count_top"><i class="fa fa-user"></i> {{trans('site.Total workers')}}</span>
                <div class="count">{{count($workers)}}</div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-clock-o"></i> {{trans('site.This months production')}}</span>
                <div class="count">{{productionMonth(0)}}m<sup>3</sup></div>
                <span class="count_bottom">
                    @if(percentDifference(productionMonth(0), productionMonth(1)) > 0)
                        <i class="green"><i class="fa fa-sort-asc"></i>
                    @else
                        <i class="red"><i class="fa fa-sort-desc"></i>
                    @endif
                        {{percentDifference(productionMonth(0), productionMonth(1))}}
                        %</i> {{trans('site.From last month')}}</span>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count no-border3">
                <span class="count_top"><i class="fa fa-user"></i> {{trans('site.Total warehouse space')}}</span>
                <div class="count">{{$warehouseUsed}}m<sup>3</sup></div>
                <span class="count_bottom">{{$warehouseOccup}}% {{trans('site.Occupied')}}</span>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count no-border4">
                <span class="count_top"><i class="fa fa-user"></i> {{trans('site.Most efficient workspace')}}</span>
                <div class="count">
                    @if(!empty($items))
                        {{$items[$highest]['produced']}}m<sup>3</sup>
                    @else
                       0m<sup>3</sup>
                    @endif
                </div>
                <span>
                     @if(!empty($items))
                        {{$items[$highest]['name']}}
                    @else
                        {{trans('site.Not enough data')}}
                    @endif
                </span>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count no-border5">
                <span class="count_top"><i class="fa fa-user"></i> {{trans('site.Least efficient workspace')}}</span>
                <div class="count">
                    @if(!empty($items))
                        {{$items[$lowest]['produced']}}m<sup>3</sup>
                    @else
                        0m<sup>3</sup>
                    @endif
                </div>
                <span>
                    @if(!empty($items))
                        {{$items[$lowest]['name']}}
                    @else
                        {{trans('site.Not enough data')}}
                    @endif
                </span>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="dashboard_graph">
                    <div class="row x_title">
                        <div class="col-md-6">
                            <h3>{{trans('site.Production statistics')}} <small>({{trans('site.12-month')}})</small></h3>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div id="area-chart"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="row second-row">
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="x_panel tile fixed_height_320">
                    <div class="x_title">
                        <h3 style="font-size: 16px">{{trans('site.Total warehouse space')}}</h3>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        @if(count($warehouses) != 0)
                            @foreach($warehouses as $warehouse)
                                <div class="widget_summary">
                                    <div class="w_left w_25">
                                        <span>{{$warehouse->name}}</span>
                                    </div>
                                    <div class="w_center w_55">
                                        <div class="progress">
                                            <div class="progress-bar bg-green" role="progressbar"
                                                 aria-valuenow="{{getPercent($warehouse->space, $warehouse->used)}}"
                                                 aria-valuemin="0"
                                                 aria-valuemax="100"
                                                 style="width:{{getPercent($warehouse->space, $warehouse->used)}}%">
                                                {{$warehouse->used}}m<sup>3</sup>
                                                <span class="sr-only">{{getPercent($warehouse->space, $warehouse->used)}}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="w_right w_20">
                                        <span>{{getPercent($warehouse->space, $warehouse->used)}}%</span>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            @endforeach
                        @else
                            {{trans('site.No warehouses found')}}
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="x_panel tile fixed_height_320">
                    <div class="x_title">
                        <h3 style="font-size: 16px">{{trans('site.Most efficient workers')}}</h3>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        @if(count($dataProd) != 0)
                            @foreach($dataProd as $workerProd)
                                <div class="widget_summary">
                                    <div class="w_left w_25">
                                        <span>{{$workerProd['name']}}</span>
                                    </div>
                                    <div class="w_center w_55">
                                        <div class="progress">
                                            <div class="progress-bar bg-green" role="progressbar"
                                                 aria-valuenow="0"
                                                 aria-valuemin="0"
                                                 aria-valuemax="100"
                                                 @if($loop->index == 0 )
                                                    <?php $maxLimit = $workerProd['produced'] ?>
                                                    style="width:100%">
                                                 @else
                                                    style="width:{{getPercent($maxLimit, $workerProd['produced'])}}%">
                                                 @endif
                                                {{$workerProd['workspace']}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="w_right w_20">
                                        <span>{{$workerProd['produced']}}m<sup>3</sup></span>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            @endforeach
                        @else
                            {{trans('site.No workers found')}}
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="x_panel tile fixed_height_320">
                    <div class="x_title">
                        <h3 style="font-size: 16px">{{trans('site.Most efficient loaders')}}</h3>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        @if(count($loadDataProd) != 0)
                            @foreach($loadDataProd as $loaderProd)
                                <div class="widget_summary">
                                    <div class="w_left w_25">
                                        <span>{{$loaderProd['name']}}</span>
                                    </div>
                                    <div class="w_center w_55">
                                        <div class="progress">
                                            <div class="progress-bar bg-green" role="progressbar"
                                                 aria-valuenow="{{getPercent($warehouse->space, $warehouse->used)}}"
                                                 aria-valuemin="0"
                                                 aria-valuemax="100"
                                                 @if($loop->index == 0)
                                                     <?php $maxLoaderLimit = $loaderProd['unloaded']?>
                                                     style="width:100%">
                                                @else
                                                    style="width:{{getPercent($maxLoaderLimit, $loaderProd['unloaded'])}}%">
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="w_right w_20">
                                        <span>{{$loaderProd['unloaded']}}m<sup>3</sup></span>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            @endforeach
                        @else
                            {{trans('site.No loaders found')}}
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer_scripts')
    <script>
        $(document).ready(function() {
            var productionText = "{{trans('site.Months production')}}";
            var data = [
                        { m: "{{getMonth(0)}}", a: "{{productionMonth(0)}}"},
                        { m: "{{getMonth(1)}}", a: "{{productionMonth(1)}}"},
                        { m: "{{getMonth(2)}}", a: "{{productionMonth(2)}}"},
                        { m: "{{getMonth(3)}}", a: "{{productionMonth(3)}}"},
                        { m: "{{getMonth(4)}}", a: "{{productionMonth(4)}}"},
                        { m: "{{getMonth(5)}}", a: "{{productionMonth(5)}}"},
                        { m: "{{getMonth(6)}}", a: "{{productionMonth(6)}}"},
                        { m: "{{getMonth(7)}}", a: "{{productionMonth(7)}}"},
                        { m: "{{getMonth(8)}}", a: "{{productionMonth(8)}}"},
                        { m: "{{getMonth(9)}}", a: "{{productionMonth(9)}}"},
                        { m: "{{getMonth(10)}}", a: "{{productionMonth(10)}}"},
                        { m: "{{getMonth(11)}}", a: "{{productionMonth(11)}}"}
                    ],
                    config = {
                        data: data,
                        xkey: 'm',
                        ykeys: ['a'],
                        labels: [productionText],
                        fillOpacity: 0.6,
                        hideHover: 'auto',
                        behaveLikeLine: true,
                        resize: true,
                        pointFillColors:['#ffffff'],
                        pointStrokeColors: ['black'],
                        lineColors:["rgba(38, 185, 154, 0.38)"]
                    };
            config.element = 'area-chart';
            Morris.Area(config);
            })
    </script>
@endsection
