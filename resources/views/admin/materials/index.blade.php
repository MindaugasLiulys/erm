@extends('layout')

@section('main_container')
    <div class="right_col" role="main">
        <h1>{{trans('site.materials')}}</h1>
        <a class="btn btn-primary" href="{{route('admin.materials.create')}}"><i class="fa fa-plus" aria-hidden="true"></i> {{trans('site.create_material')}}</a><hr>
        <div class="workspaces-body">
            <table id="example" class="display resp table-materials table table-striped table-bordered dataTable no-footer" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>{{trans('site.No.')}}</th>
                        <th>{{trans('site.materials')}}</th>
                        <th>{{trans('site.quantity')}}</th>
                        <th>{{trans('site.warehouse')}}</th>
                        <th>{{trans('site.action')}}</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($materials as $material)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$material->name}}</td>
                        <td>{{$material->quantity}}</td>
                        <td>{{$material->warehouse->name}}</td>
                        <td>
                            <a href="{{route('admin.materials.edit', $material->id)}}" class="btn btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                            <a class="btn btn-danger" data-action="{{route('delete-material', $material->id)}}" data-method="delete"><i class="fa fa-trash" aria-hidden="true"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>{{trans('site.No.')}}</th>
                        <th>{{trans('site.materials')}}</th>
                        <th>{{trans('site.quantity')}}</th>
                        <th>{{trans('site.warehouse')}}</th>
                        <th>{{trans('site.action')}}</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    @include('admin.modals.deleteModal')
@endsection