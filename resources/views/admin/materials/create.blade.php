@extends('layout')

@section('main_container')
    <div class="right_col" role="main">
        <h1>{{trans('site.materials')}}</h1>
        <hr>
        <div class="workspaces-body">
            <form action="{{route('admin.materials.store')}}" class="create-new-form validate-form" method="POST">
                {!! csrf_field() !!}
                <div class="form-group">
                    <label class="control-label" for="name">{{trans('site.material_name')}}</label>
                    <input type="text" id="name" name="name" class="form-control validate-input">
                    <div class="alert alert-danger"></div>
                </div>
                <div class="form-group">
                    <label class="control-label" for="quantity">{{trans('site.quantity')}}</label>
                    <input type="text" id="quantity" name="quantity" class="form-control validate-input">
                    <div class="alert alert-danger"></div>
                </div>
                <div class="form-group">
                    <label for="warehouseId">{{trans('site.select warehouse')}}</label>
                    <select name="warehouseId" id="warehouseId" class="form-control">
                        @foreach($warehouses as $warehouse)
                            <option value="{{$warehouse->id}}">{{$warehouse->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <a href="{{route('admin.materials.index')}}" class="btn btn-default">{{trans('site.cancel')}}</a>
                    <input class="btn btn-success" type="submit" value="{{trans('site.create')}}">
                </div>
            </form>
        </div>
    </div>
@endsection