@extends('layout')

@section('main_container')

    <div class="right_col">
        <h1>{{trans('site.Edit material')}}</h1>
        <hr>
        <form action="{{route('admin.materials.update', $material->id)}}" class="create-new-form validate-form"
              method="POST">
            {!! csrf_field() !!}
            {{ method_field('PUT') }}
            <div class="form-group">
                <label class="control-label" for="name">{{trans('site.material')}}</label>
                <input type="text" id="name" name="name" class="form-control validate-input" value="{{$material->name}}">
                <div class="alert alert-danger"></div>
            </div>
            <div class="form-group">
                <label class="control-label" for="quantity">{{trans('site.quantity')}}</label>
                <input type="text" id="quantity" name="quantity" value="{{$material->quantity}}" class="form-control validate-input">
                <div class="alert alert-danger"></div>
            </div>
            <div class="form-group">
                <label class="control-label">{{trans('site.warehouse')}}</label>
                <select name="warehouseId" class="form-control">
                    @foreach($warehouses as $warehouse)
                        @if($warehouse->id == $material->warehouse_id)
                            <option value="{{$warehouse->id}}" selected="selected">{{$warehouse->name}}</option>
                        @else
                            <option value="{{$warehouse->id}}">{{$warehouse->name}}</option>
                        @endif
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <a href="{{route('admin.materials.index')}}" class="btn btn-default">{{trans('site.cancel')}}</a>
                <input class="btn btn-success" type="submit" value="{{trans('site.save')}}">
            </div>
        </form>
    </div>

@endsection