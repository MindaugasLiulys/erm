@extends('layout')

@section('main_container')
    <div class="right_col" role="main">
        <h1>{{trans('site.new breakdown')}}</h1><hr>
        <form action="{{route('breakdowns-create')}}" class="create-new-form validate-form" method="POST">
            {!! csrf_field() !!}
            <div class="form-group">
                <label class="control-label">{{trans('site.select alerted object')}}</label>
                <select name="type" class="form-control type-option">
                    <option value="0">{{trans('site.workspace')}}</option>
                    <option value="1">{{trans('site.loader')}}</option>
                </select>
            </div>
            <div class="form-group workspace-option">
                <label class="control-label">{{trans('site.Called')}}</label>
                <select name="breakdown_id" class="form-control">
                    @foreach($workspaces as $workspace)
                        <option value="{{$workspace->id}}">{{$workspace->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group loader-option" style="display: none">
                <label class="control-label">{{trans('site.Called')}}</label>
                <select name="breakdown_id" class="form-control">
                    @foreach($loaders as $loader)
                        <option value="{{$loader->id}}">{{$loader->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label class="control-label">{{trans('site.Description')}}</label>
                <textarea name="text" rows="3" class="form-control"></textarea>
                <div class="alert alert-danger"></div>
            </div>
            <div class="form-group">
                <label class="control-label">{{trans('site.duration')}} <small>({{trans('site.minutes')}})</small></label>
                <input type="number" name="time" class="form-control">
                <div class="alert alert-danger"></div>
            </div>
            <div class="form-group">
                <label class="control-label">{{trans('site.Date')}}</label>
                <input type="date" name="created_at" class="form-control">
                <div class="alert alert-danger"></div>
            </div>
            <div class="form-group">
                <input type="checkbox" name="status" value="1"> <span style="font-weight: 700">{{trans('site.active')}}?</span>
            </div>
            <div class="form-group">
                <a href="{{route('breakdowns')}}" class="btn btn-default">{{trans('site.cancel')}}</a>
                <input class="btn btn-success" type="submit" value="{{trans('site.create')}}">
            </div>
        </form>
    </div>
@endsection
@section('footer_scripts')
    <script>
        $(document).ready(function() {
            var selectedVal2 = $('select.type-option').val();

            if(selectedVal2 == 0){
                $('.workspace-option'). show();
                $('.loader-option'). hide();
            }else if(selectedVal2 == 1){
                $('.workspace-option'). hide();
                $('.loader-option'). show();
            }

            $('select.type-option').change(function(){
                var selectedVal = $('select.type-option').val();
                if(selectedVal == 0){
                    $('.workspace-option'). slideDown(400);
                    $('.loader-option'). slideUp(400);
                }else if(selectedVal == 1){
                    $('.loader-option'). slideDown(400);
                    $('.workspace-option'). slideUp(400);
                }
            });

        })
    </script>
@endsection