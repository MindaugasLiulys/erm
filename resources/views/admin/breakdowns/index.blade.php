@extends('layout')

@section('main_container')
    <div class="right_col" role="main">
        <h1>{{trans('site.breakdowns')}}</h1>
        <div class="workspaces-body">
            <div class="col-md-12">
                <h4>{{trans('site.Alerts')}} <small>(5 {{trans('site.Newest')}})</small></h4>
                <ul class="messages">
                    @foreach($breakdowns5 as $breakdown)
                        <li>
                            <div class="message_date">
                                <p class="month">{{trans('site.duration')}}:</p>
                                <h3 class="date text-info">
                                    @if($breakdown->status == 0)
                                        {{$breakdown->time}} {{convert_plural_tes('Minu', $breakdown->time)}}
                                    @else
                                        {{$activeNow->diffInMinutes($breakdown->created_at)}} {{convert_plural_tes('Minu', $activeNow->diffInMinutes($breakdown->created_at))}}
                                    @endif
                                </h3>
                            </div>
                            <div class="message_wrapper">
                                <h4 class="heading" style="cursor: default">{{$breakdown->breakdownObject->name}}
                                    <small>
                                        @if($breakdown->status == 0)
                                            (<span class="green">{{trans('site.fixed')}}</span>)
                                        @else
                                            (<span class="red">{{trans('site.Active')}}</span>)
                                        @endif
                                        {{$breakdown->created_at->format('m-d H:m')}}</small>
                                </h4>
                                <blockquote class="message">
                                    @if(!empty($breakdown->text))
                                        {{$breakdown->text}}
                                    @else
                                        {{trans('site.text field is not filled')}}
                                    @endif
                                </blockquote>
                                <br>
                            </div>
                        </li>
                    @endforeach
                    @if(count($breakdowns5) < 1)
                        {{trans('site.No data')}}
                    @endif
                </ul>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <h1>{{trans('site.Statistics')}}</h1>
                        <div class="row tile_count">
                            <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count no-border1">
                                <span class="count_top"><i class="fa fa-user"></i> {{trans('site.Total breakdowns')}}</span>
                                <div class="count">{{count($breakdowns)}}</div>
                            </div>
                            <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count no-border2">
                                <span class="count_top"><i class="fa fa-user"></i> {{trans('site.Total breakdowns this month')}}</span>
                                <div class="count">{{count($monthsBreakdown)}}</div>
                            </div>
                        </div>
                        <div class="dashboard_graph">
                            <div class="row x_title">
                                <div class="col-md-6">
                                    <h3>{{trans('site.Breakdowns statistics')}} <small>({{trans('site.12-month')}})</small></h3>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div id="area-chart"></div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <h1>{{trans('site.All breakdowns')}}</h1>
                <a class="btn btn-primary" href="{{route('breakdowns-create')}}"><i class="fa fa-plus" aria-hidden="true"></i> {{trans('site.create breakdown')}}</a>
                <table id="example" class="display table-worker resp table table-striped table-bordered dataTable no-footer" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>{{trans('site.No.')}}</th>
                            <th>{{trans('site.Called')}}</th>
                            <th>{{trans('site.Description')}}</th>
                            <th>{{trans('site.duration')}} <small>({{trans('site.minutes')}})</small></th>
                            <th>{{trans('site.Date')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($breakdowns as $breakdown)
                            <tr>
                                <td>{{$loop->iteration}}.</td>
                                <td>{{$breakdown->breakdownObject->name}}</td>
                                <td>{{$breakdown->text}}</td>
                                <td>{{$breakdown->time}}</td>
                                <td>{{$breakdown->created_at->format('m-d H:m')}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@section('footer_scripts')
    <script>
        $(document).ready(function() {
            var breakdownText = "{{trans('site.Months breakdowns')}}";
            var data = [
                        { m: "{{getMonth(0)}}", a: "{{monthsBreakdown(0)}}"},
                        { m: "{{getMonth(1)}}", a: "{{monthsBreakdown(1)}}"},
                        { m: "{{getMonth(2)}}", a: "{{monthsBreakdown(2)}}"},
                        { m: "{{getMonth(3)}}", a: "{{monthsBreakdown(3)}}"},
                        { m: "{{getMonth(4)}}", a: "{{monthsBreakdown(4)}}"},
                        { m: "{{getMonth(5)}}", a: "{{monthsBreakdown(5)}}"},
                        { m: "{{getMonth(6)}}", a: "{{monthsBreakdown(6)}}"},
                        { m: "{{getMonth(7)}}", a: "{{monthsBreakdown(7)}}"},
                        { m: "{{getMonth(8)}}", a: "{{monthsBreakdown(8)}}"},
                        { m: "{{getMonth(9)}}", a: "{{monthsBreakdown(9)}}"},
                        { m: "{{getMonth(10)}}", a: "{{monthsBreakdown(10)}}"},
                        { m: "{{getMonth(11)}}", a: "{{monthsBreakdown(11)}}"}
                    ]
                    ,
                    config = {
                        data: data,
                        xkey: 'm',
                        ykeys: ['a'],
                        labels: [breakdownText],
                        fillOpacity: 0.6,
                        hideHover: 'auto',
                        behaveLikeLine: true,
                        resize: true,
                        pointFillColors:['#ffffff'],
                        pointStrokeColors: ['black'],
                        lineColors:["rgba(38, 185, 154, 0.38)"]
                    };

            config.element = 'area-chart';
            Morris.Area(config);
        })
    </script>
@endsection