@extends('layout')

@section('main_container')
    <div class="right_col" role="main">
        <h1>{{trans('site.factory')}}</h1>
        <hr>
        <div class="col-md-12 col-sm-12 col-xs-12 factory_subtitle padding-right-0">
            <div class="col-md-11 col-sm-11 col-xs-9">
                <h2>{{trans('site.workspaces')}}</h2>
            </div>
            <div class="col-md-1 col-sm-1 col-xs-3 collapse-block padding-right-0">
                <a class="collapse-link block-slideUp" data-block="1"><i class="fa fa-chevron-up"></i></a>
            </div>
        </div>
        <div class="workspaces-body block_1">
            @if(count($workspaces) >= 1)
                @foreach($workspaces->chunk(3) as $workspace1)
                    <div class="row">
                        @foreach($workspace1 as $workspace)
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <div class="x_panel tile">
                                    <div class="x_title">
                                        <h2 class="block-title-fit h2-font">
                                            <span class="rand-color"
                                                  style="background-color: {{$workspace->color}}">
                                                {{$serialNo++}}. {{$workspace->name}}
                                            </span>
                                        </h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                        <table class="scroll first-column-width" style="width:100%">
                                            <tbody>
                                            <?php $workersNumber = 1?>
                                            @foreach($workspaceWorkers as $worker)
                                                @foreach($worker->workspaces as $work)
                                                    @if($work->id == $workspace->id)
                                                        @if($workersNumber == 1)
                                                            <tr>
                                                                <th>{{trans('site.No.')}}</th>
                                                                <th>{{trans('site.Full name')}}</th>
                                                                <th style="text-align: right">{{trans('site.status')}}</th>
                                                            </tr>
                                                        @endif
                                                        <tr class="border-bottom">
                                                            <td><?=$workersNumber++?>.</td>
                                                            <td>{{$worker->name}} {{$worker->surname}}
                                                                <span style="color:#52A7E0">{{$worker->position->name}}</span>
                                                            </td>
                                                            <td class="loader-worker-{{$worker->user->id}}"
                                                                style="text-align: right">
                                                                @if($worker->user->isOnline() || $worker->status)
                                                                    <span class="green">{{trans('site.Active')}}</span>
                                                                @else
                                                                    <span class="red">{{trans('site.Not active')}}</span>
                                                                @endif
                                                            </td>
                                                        </tr>
                                                    @endif
                                                @endforeach
                                            @endforeach
                                            @if($workersNumber <= 1)
                                                <tr>
                                                    <td style="padding: 5px 20px; font-size: 14px">{{trans('site.no workers in workspace')}}</td>
                                                </tr>
                                            @endif
                                            </tbody>
                                        </table>
                                        <div id="workspaceSup-{{$workspace->id}}"
                                             class="alert alert-danger supervisor {{ $workspace->supervisor()->called()->first() ? '' : 'hidden' }}">
                                            {{trans('site.Supervisor needed')}}
                                            <button type="button" class="close supClose" data-id="{{ $workspace->id }}" aria-label="Close">
                                                {!! csrf_field() !!}
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <a class="graph-slideUp">
                                            <div>
                                                <span class="block-title-fit">
                                                    {{trans('site.Statistics')}}<i class="fa fa-info-circle"
                                                                                   aria-hidden="true"></i>

                                                </span><i class="fa fa-chevron-down changeI"></i>
                                            </div>
                                        </a>
                                        <div style="text-align: center" class="graphic">
                                            <div class="clearfix"></div>
                                            <div id="graph_{{$workspace->id}}" class="graphic-div"
                                                 data-mon="{{weekDayData($monday, $workspace->id)}}"
                                                 data-tue="{{weekDayData($tuesday, $workspace->id)}}"
                                                 data-wed="{{weekDayData($wednesday, $workspace->id)}}"
                                                 data-thu="{{weekDayData($thursday, $workspace->id)}}"
                                                 data-fri="{{weekDayData($friday, $workspace->id)}}"
                                                 data-sat="{{weekDayData($saturday, $workspace->id)}}"
                                                 data-sun="{{weekDayData($sunday, $workspace->id)}}">
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endforeach
                <?php $serialNo = 1?>
            @else
                <h4>{{trans('site.workspaces not found')}}</h4>
            @endif
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12 factory_subtitle padding-right-0">
            <div class="col-md-11 col-sm-11 col-xs-9">
                <h2>{{trans('site.loaders')}}</h2>
            </div>
            <div class="col-md-1 col-sm-1 col-xs-3 collapse-block padding-right-0">
                <a class="collapse-link block-slideUp" data-block="2"><i class="fa fa-chevron-up"></i></a>
            </div>
        </div>
        <div class="workspaces-body block_2">
            @if(count($loaders) >= 1)
                @foreach($loaders->chunk(3) as $loaderChunk)
                    <div class="row">
                        @foreach($loaderChunk as $loader)
                            <div class="col-md-4 col-sm-4 col-xs-12 loaderBlock-{{$loader->id}}">
                                <div class="x_panel tile fixed_height_loader">
                                    <div class="x_title loader-socket">
                                        <h2 class="h2-font">
                                             <span class="rand-color"
                                                   style="background-color: {{$loader->color}}">
                                                {{$serialNo++}}. {{$loader->name}}
                                            </span>
                                            @if(count($loader->action) >= 1 && $loader->action->actions_status == 1)
                                                @if(!is_null($loader->jobLater->last()))
                                                    <span class="loader_{{$loader->id}}"><span
                                                                class="orange">&nbsp;{{trans('site.working_later',
                                                                ['time' => $loader->jobLater->last()->getStartAtTime()])}}</span><i></i></span>
                                                @else
                                                    <span class="loader_{{$loader->id}}"><span
                                                                class="{{ getActionByStatus($loader->action->actions_status)['span'] }}">
                                                            &nbsp;{{ getActionByStatus($loader->action->actions_status)['message'] }}</span>
                                                        <i class="{{ getActionByStatus($loader->action->actions_status)['i'] }}"></i></span>
                                                @endif
                                            @else
                                                <span class="loader_{{$loader->id}}"><span
                                                            class="{{ getActionByStatus($loader->action->actions_status)['span'] }}">
                                                            &nbsp;{{ getActionByStatus($loader->action->actions_status)['message'] }}</span>
                                                        <i class="{{ getActionByStatus($loader->action->actions_status)['i'] }}"></i></span>
                                            @endif
                                        </h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                            @if($loader->status == 1 && $loader->action->actio_status == 0)
                                                <li class="dropdown callLoad callLoad-{{$loader->id}}">
                                            @else
                                                <li class="dropdown callLoad callLoad-{{$loader->id}} disabled">
                                                    @endif
                                                    <a href="javascript:;" class="workplace_drop dropdown-toggle green"
                                                       data-toggle="dropdown" role="button" data-id="{{$loader->id}}"
                                                       aria-expanded="false"><i class="fa fa-lightbulb-o"></i>
                                                    </a>
                                                    <ul class="dropdown-menu dropdown_workplace_{{$loader->id}}"
                                                        role="menu">
                                                        <li>
                                                            <a class="unloadModalCall" data-id="{{$loader->id}}"><i
                                                                        class="fa fa-arrow-circle-o-right"></i>
                                                                {{trans('site.Unload materials')}}
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a class="loadModalCall" data-id="{{$loader->id}}"><i
                                                                        class="fa fa-arrow-circle-o-left"></i>
                                                                {{trans('site.Load products')}}
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a class="additionalModalCall" data-id="{{$loader->id}}"><i
                                                                        class="fa fa-arrow-circle-o-left"></i>
                                                                {{trans('site.Additional work')}}
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                        <table style="width:100%">
                                            <tbody>
                                            <?php $workersNumber = 1?>
                                            @foreach($loaderWorkers as $worker)
                                                @foreach($worker->loaderTo as $work)
                                                    @if($work->loader_id == $loader->id)
                                                        @if($workersNumber == 1)
                                                            <tr>
                                                                <th>{{trans('site.No.')}}</th>
                                                                <th>{{trans('site.Full name')}}</th>
                                                                <th style="text-align: right">{{trans('site.status')}}</th>
                                                            </tr>
                                                        @endif
                                                        <tr class="border-bottom">
                                                            <td><?=$workersNumber++?>.</td>
                                                            <td>{{$worker->name}} {{$worker->surname}}</td>
                                                            <td class="loader-worker-{{$worker->user->id}} loaderStat"
                                                                style="text-align: right">
                                                                @if($worker->loader()->first()->status)
                                                                    <span class="green loaderStatus-{{$loop->index}}"
                                                                          data-status="1">{{trans('site.Active')}}</span>
                                                                @else
                                                                    <span class="red loaderStatus-{{$loop->index}}"
                                                                          data-status="0">{{trans('site.Not active')}}</span>
                                                                @endif
                                                            </td>
                                                        </tr>
                                                    @endif
                                                @endforeach
                                            @endforeach
                                            @if($workersNumber <= 1)
                                                <tr>
                                                    <td colspan="3"
                                                        class="no-workers">{{trans('site.no workers in workspace')}}</td>
                                                </tr>
                                            @endif
                                            </tbody>
                                        </table>
                                        <div id="loaderSup-{{$loader->id}}" class="alert alert-danger supervisor
                                                    {{ $loader->supervisor()->called()->first() ? '' : ' hidden' }}">
                                            {{trans('site.Supervisor needed')}}
                                            <button type="button" class="close supClose" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <a class="graph-slideUpLoader">
                                            <div>
                                                <span class="block-title-fit">
                                                    {{trans('site.Statistics')}}<i class="fa fa-info-circle"
                                                                                   aria-hidden="true"></i>

                                                </span><i class="fa fa-chevron-down changeI"></i>
                                            </div>
                                        </a>
                                        <div style="text-align: center" class="loader-graph">
                                            <div class="clearfix"></div>
                                            <div id="graphLoader_{{$loader->id}}" class="loader-div"
                                                 data-mon="{{weekDayDataLoaded($mondayL, $loader->id)}}"
                                                 data-monUn="{{weekDayDataUnloaded($mondayL, $loader->id)}}"
                                                 data-tue="{{weekDayDataLoaded($tuesdayL, $loader->id)}}"
                                                 data-tueUn="{{weekDayDataUnloaded($tuesdayL, $loader->id)}}"
                                                 data-wed="{{weekDayDataLoaded($wednesdayL, $loader->id)}}"
                                                 data-wedUn="{{weekDayDataUnloaded($wednesdayL, $loader->id)}}"
                                                 data-thu="{{weekDayDataLoaded($thursdayL, $loader->id)}}"
                                                 data-thuUn="{{weekDayDataUnloaded($thursdayL, $loader->id)}}"
                                                 data-fri="{{weekDayDataLoaded($fridayL, $loader->id)}}"
                                                 data-friUn="{{weekDayDataUnloaded($fridayL, $loader->id)}}"
                                                 data-sat="{{weekDayDataLoaded($saturdayL, $loader->id)}}"
                                                 data-satUn="{{weekDayDataUnloaded($saturdayL, $loader->id)}}"
                                                 data-sun="{{weekDayDataLoaded($sundayL, $loader->id)}}"
                                                 data-sunUn="{{weekDayDataUnloaded($sundayL, $loader->id)}}">
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endforeach
                <?php $serialNo = 1?>
            @else
                <h4>{{trans('site.loaders not found')}}</h4>
            @endif
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12 factory_subtitle padding-right-0">
            <div class="col-md-11 col-sm-11 col-xs-9">
                <h2>{{trans('site.warehouses')}}</h2>
            </div>
            <div class="col-md-1 col-sm-1 col-xs-3 collapse-block padding-right-0">
                <a class="collapse-link block-slideUp" data-block="3"><i class="fa fa-chevron-up"></i></a>
            </div>
        </div>
        <div class="workspaces-body block_3">
            @if(count($warehouses) >= 1)
                @foreach($warehouses->chunk(3) as $warehouseChunk)
                    <div class="row">
                        @foreach($warehouseChunk as $warehouse)
                            <div class="col-md-4 col-sm-4 col-xs-12 warehouse-socket">
                                <div class="x_panel tile">
                                    <div class="x_title">
                                        <h2 class="block-title-fit h2-font">
                                            <span class="rand-color"
                                                  style="background-color: {{$warehouse->color}}">
                                                {{$serialNo++}}. {{$warehouse->name}}
                                            </span>
                                        </h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                        <table class="workspace-table warehouse_{{$warehouse->id}}" style="width:100%">
                                            <tbody>
                                            <tr>
                                                <th>{{trans('site.Volume')}}</th>
                                                <th>{{trans('site.Value')}}</th>
                                            </tr>
                                            <tr>
                                                <td>{{trans('site.Total')}}</td>
                                                <td>{{$warehouse->space}}m<sup>3</sup></td>
                                            </tr>
                                            <tr>
                                                <td>{{trans('site.Used')}}</td>
                                                <td class="volume_used"><span>{{$warehouse->used}}</span>m<sup>3</sup>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>{{trans('site.Reserved')}}</td>
                                                <td class="volume_reserved">
                                                    <span>{{$warehouse->reserved}}</span>m<sup>3</sup>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>{{trans('site.Free')}}</td>
                                                <td class="volume_free">
                                                    <span>{{$warehouse->space - $warehouse->used - $warehouse->reserved}}</span>m<sup>3</sup>
                                                </td>
                                            </tr>
                                            <tr style="border-top: 1px solid #e6e9ed"></tr>
                                            <tr>
                                                <td colspan="2"
                                                    class="space-in-text">{{trans('site.Space in warehouse')}}</td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" style="text-align: center">
                                                    <div class="progress">
                                                        <div class="progress-bar prog_{{$warehouse->id}} progress-bar-info progress-bar-striped active"
                                                             role="progressbar"
                                                             aria-valuenow="{{getPercent($warehouse->space, $warehouse->used)}}"
                                                             aria-valuemin="0" aria-valuemax="100"
                                                             style="width:{{getPercent($warehouse->space, $warehouse->used)}}%">
                                                            @if($warehouse->used != 0 && getPercent($warehouse->space, $warehouse->used) > 7)
                                                                <span>&nbsp;{{getPercent($warehouse->space, $warehouse->used)}}</span>
                                                                % {{trans('site.Occupied')}}
                                                            @endif
                                                        </div>
                                                        <div class="progress-bar progress-bar-warning progress-bar-striped active"
                                                             role="progressbar"
                                                             aria-valuenow="{{getPercent($warehouse->space, $warehouse->reserved)}}"
                                                             aria-valuemin="0" aria-valuemax="100"
                                                             style="width:{{getPercent($warehouse->space, $warehouse->reserved)}}%">
                                                            @if($warehouse->reserved != 0 && getPercent($warehouse->space, $warehouse->reserved) > 7)
                                                                <span>&nbsp;{{getPercent($warehouse->space, $warehouse->reserved)}}</span>
                                                                % {{trans('site.Reserved')}}
                                                            @endif
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endforeach
                <?php $serialNo = 1?>
            @else
                <h4>{{trans('site.warehouses not found')}}</h4>
            @endif
        </div>
    </div>
    @include('admin.modals.unloadModal')
    @include('admin.modals.loadModal')
    @include('admin.modals.additionalModal')
    @include('admin.modals.brokenModal')
@endsection

@section('footer_scripts')
    <script>
        $('.supClose').on('click', function (e) {
            e.preventDefault();
            $.ajax({
                type: "GET",
                url: "{{route('call_manager_finish')}}" + '?workspace_id=' + $(this).data('id'),
                headers: {
                    'X-CSRF-TOKEN': $(this).find('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    console.log('success');
                },
                error: function (data) {
                    console.log('error');
                }
            });
        })

        $('#select-order').on('change', '', function (e) {
            e.preventDefault();
            var url = "{{route('get_available_warehouses')}}";
            $.ajax({
                type: "POST",
                url: url,
                data: $("#loadSubmit").serialize(),
                headers: {
                    'X-CSRF-TOKEN': $("#loadSubmit").find('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    $('#select-warehouse').find('option').remove();
                    $('#select-warehouse').closest('div').removeClass('hidden');
                    $("#loadSubmit").find('input[type="submit"]').removeClass('disabled');
                    var listItems= "";
                    $.each(data.warehouses,function(key, value)
                    {
                        listItems += '<option value=' + value + '>' + key + '</option>';
                    });
                    $('#select-warehouse').html(listItems);
                },
                error: function (data) {
                    console.log('error');
                }
            });
        });

        $('#unloadSubmit').submit(function (e) {
            var url = "{{route('call_material')}}";
            var modal = $("#unloadModal");
            $.ajax({
                type: "POST",
                url: url,
                data: $("#unloadSubmit").serialize(),
                headers: {
                    'X-CSRF-TOKEN': $("#unloadSubmit").find('meta[name="csrf-token"]').attr('content')
                },
                success: function () {
                    modal.modal('hide');
                },
                error: function (data) {
                    errorsOutput('#unloadSubmit', data);
                }
            });
            e.preventDefault();
        });

        $('#loadSubmit').submit(function (e) {
            var url = "{{route('call_order')}}";
            var modal = $("#loadModal");
            $.ajax({
                type: "POST",
                url: url,
                headers: {
                    'X-CSRF-TOKEN': $("#loadSubmit").find('meta[name="csrf-token"]').attr('content')
                },
                data: $("#loadSubmit").serialize(),
                success: function () {
                    modal.modal('hide');
                },
                error: function (data) {
                    errorsOutput('#loadModal', data);
                }
            });
            e.preventDefault();
        });

        $('#additionalSubmit').submit(function (e) {
            var url = "{{route('call_additional')}}";
            var modal = $("#additionalModal");
            $.ajax({
                type: "POST",
                url: url,
                headers: {
                    'X-CSRF-TOKEN': $("#additionalSubmit").find('meta[name="csrf-token"]').attr('content')
                },
                data: $("#additionalSubmit").serialize(),
                success: function () {
                    modal.modal('hide');
                },
                error: function (data) {
                    errorsOutput('#additionalModal', data);
                }
            });
            e.preventDefault();
        });

        function errorsOutput(errorPlace, data) {
            var errors = data.responseJSON.errors;

            $.each(errors, function (key, value) {
                $(errorPlace + ' [name=' + key + ']').closest('div')
                        .addClass('has-error')
                        .append('<div class="text-danger">' + value + '</div>');
            })
        };
    </script>

    @if(env("APP_ENV") == 'local')
        <script src="http://localhost:3000/socket.io/socket.io.js"></script>
    @else
        <script src="http://erm.procreo.eu:3000/socket.io/socket.io.js"></script>
    @endif
    <script>
        var socket = null;

        if ('{{ env('APP_ENV') }}' != 'local') {
            socket = io('http://erm.procreo.eu:3000');
        } else {
            socket = io('http://localhost:3000');
        }

        socket.on("call-loader:App\\Events\\Factory", function (message) {

            if (message.data.action_status_type == 1) {
                $('.loader-socket').find('.loader_' + message.data.loader_id + ' span').removeClass().addClass(message.data.loader_class).html(message.data.action_name)
                        .siblings('i').removeClass().addClass(message.data.iconClass + ' red');
            } else {
                $('.loader-socket').find('.loader_' + message.data.loader_id + ' span').removeClass().addClass(message.data.loader_class).html(message.data.action_name)
                        .siblings('i').removeClass();
            }

            //Supervisor call
            if (message.data.supervisorStatus == 1) {
                $('#workspaceSup-' + message.data.workspace_id).removeClass('hidden').css('display', 'block').slideDown(200);
            } else if (message.data.supervisorStatus == 2) {
                $('#loaderSup-' + message.data.loader_id).removeClass('hidden').css('display', 'block').slideDown(200);
            }

            $('.warehouse-socket').find('.warehouse_' + message.data.warehouse_id + ' .volume_used span').html(message.data.warehouse_used);
            $('.warehouse-socket').find('.warehouse_' + message.data.warehouse_id + ' .volume_reserved span').html(message.data.warehouseReserved);
            $('.warehouse-socket').find('.warehouse_' + message.data.warehouse_id + ' .volume_free span').html(message.data.warehouse_free);

            if (message.data.warehouse_perc != 'undefined') {
                $('.prog_' + message.data.warehouse_id).attr('aria-valuenow', message.data.warehouse_perc).attr('style', 'width: ' + message.data.warehouse_perc + '%;').find('span').html(message.data.warehouse_perc);
                $('.progr_' + message.data.warehouse_id).attr('aria-valuenow', message.data.warehouse_perc_res).attr('style', 'width: ' + message.data.warehouse_perc_res + '%;').find('span').html(message.data.warehouse_perc_res);
            }

            if (message.data.graphStatus == 1) {
                var graph = $('#graph_' + message.data.workspaceId);

                var mon = graph.data('mon');
                var tue = graph.data('tue');
                var wed = graph.data('wed');
                var thu = graph.data('thu');
                var fri = graph.data('fri');
                var sat = graph.data('sat');
                var sun = graph.data('sun');

                var production = message.data.produced;

                if (message.data.todayDay == 0) {
                    sun = production;
                } else if (message.data.todayDay == 1) {
                    mon = production;
                } else if (message.data.todayDay == 2) {
                    tue = production;
                } else if (message.data.todayDay == 3) {
                    wed = production;
                } else if (message.data.todayDay == 4) {
                    thu = production;
                } else if (message.data.todayDay == 5) {
                    fri = production;
                } else if (message.data.todayDay == 6) {
                    sat = production;
                }

                window['graph_' + message.data.workspaceId].setData([
                    {device: "{{trans('site.Monday')}}", Produkcija: mon},
                    {device: "{{trans('site.Tuesday')}}", Produkcija: tue},
                    {device: "{{trans('site.Wednesday')}}", Produkcija: wed},
                    {device: "{{trans('site.Thursday')}}", Produkcija: thu},
                    {device: "{{trans('site.Friday')}}", Produkcija: fri},
                    {device: "{{trans('site.Saturday')}}", Produkcija: sat},
                    {device: "{{trans('site.Sunday')}}", Produkcija: sun}
                ])
            }

            if (message.data.graphLoader == 1) {
                var graphLoader = $('#graphLoader_' + message.data.loader_id);

                var monL = graphLoader.data('mon');
                var monUn = graphLoader.data('monun');
                var tueL = graphLoader.data('tue');
                var tueUn = graphLoader.data('tueun');
                var wedL = graphLoader.data('wed');
                var wedUn = graphLoader.data('wedun');
                var thuL = graphLoader.data('thu');
                var thuUn = graphLoader.data('thuun');
                var friL = graphLoader.data('fri');
                var friUn = graphLoader.data('friun');
                var satL = graphLoader.data('sat');
                var satUn = graphLoader.data('satun');
                var sunL = graphLoader.data('sun');
                var sunUn = graphLoader.data('sunun');

                var loaded = message.data.loaded;
                var unloaded = message.data.unloaded;

                if (message.data.todayDay == 0) {
                    sunL = loaded;
                    sunUn = unloaded;
                } else if (message.data.todayDay == 1) {
                    monL = loaded;
                    monUn = unloaded;
                } else if (message.data.todayDay == 2) {
                    tueL = loaded;
                    tueUn = unloaded;
                } else if (message.data.todayDay == 3) {
                    wedL = loaded;
                    wedUn = unloaded;
                } else if (message.data.todayDay == 4) {
                    thuL = loaded;
                    thuUn = unloaded;
                } else if (message.data.todayDay == 5) {
                    friL = loaded;
                    friUn = unloaded;
                } else if (message.data.todayDay == 6) {
                    satL = loaded;
                    satUn = unloaded;
                }

                window['graphLoaders_' + message.data.loaderId].setData([
                    {device: "{{trans('site.Monday')}}", LoadedProduction: monL, UnloadedProduction: monUn},
                    {device: "{{trans('site.Tuesday')}}", LoadedProduction: tueL, UnloadedProduction: tueUn},
                    {device: "{{trans('site.Wednesday')}}", LoadedProduction: wedL, UnloadedProduction: wedUn},
                    {device: "{{trans('site.Thursday')}}", LoadedProduction: thuL, UnloadedProduction: thuUn},
                    {device: "{{trans('site.Friday')}}", LoadedProduction: friL, UnloadedProduction: friUn},
                    {device: "{{trans('site.Saturday')}}", LoadedProduction: satL, UnloadedProduction: satUn},
                    {device: "{{trans('site.Sunday')}}", LoadedProduction: sunL, UnloadedProduction: sunUn}
                ])

            }

            //Brokendown modal
            if (message.data.actions_status == 5) {
                $('#brokenModal').modal('show').find('.modal-title').html("{{trans('site.Loader broke')}}");
                $('#brokenModal').modal('show').find('.modal-body p').html(message.data.loader_name + ' is broken, Mechanic needed');
            } else if (message.data.actions_status == 9) {
                $('#brokenModal').modal('show').find('.modal-title').html("{{trans('site.Workspace broke')}}");
                $('#brokenModal').modal('show').find('.modal-body p').html(message.data.workspace + ' is broken, Mechanic needed');
            }

            {{--//Brokendown modal--}}
            {{--if (message.data.actions_status == 5) {--}}
            {{--init_PNotify("{{trans('site.Loader broke')}}" + " (" + message.data.loader_name + ")", '', 'error');--}}
            {{--}else if(message.data.actions_status == 9){--}}
            {{--init_PNotify("{{trans('site.Workspace broke')}}" + " (" + message.data.workplace + ")", '', 'error');--}}
            {{--}--}}

            {{--//Loader rejected--}}
            {{--if(message.data.rejected){--}}
            {{--init_PNotify("{{trans('site.call rejected')}}", message.data.loader_name + " {{trans('site.loader rejected call')}}", 'warning');--}}
            {{--}--}}

        });

        socket.on("worker-login:App\\Events\\Login", function (message) {
            if (message.data.status != 'logout') {
                $('.loader-socket').find('.callLoad-' + message.data.loaderId).removeClass('disabled');
                $('.loader-worker-' + message.data.workerUserId).find('span').removeClass().addClass('green').html('{{trans('site.Active')}}');
                $('.loaderBlock-' + message.data.loaderId).find('.callLoad').removeClass('disabled-by-user');
            } else {
                $('.loader-worker-' + message.data.workerUserId).find('span').removeClass().addClass('red').html('{{trans('site.Not active')}}');
                $('.loaderBlock-' + message.data.loaderId).find('.callLoad').addClass('disabled-by-user');
                $('.loader-socket').find('.callLoad-' + message.data.loaderId).addClass('disabled');
            }

        });

    </script>

@endsection