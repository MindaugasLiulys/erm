@extends('layout')

@section('main_container')
    <div class="right_col" role="main">
        <h1>{{trans('site.map')}}</h1>
        <div class="workspaces-body">
            <div id="map"></div>
        </div>
        <div class="row">
            <form action="{{ route('map.update', $settings->id) }}" class="validate-form"
                  method="POST">
                {{ method_field('PUT') }}
                {!! csrf_field() !!}
                <div class="col-xs-12">
                    <h1>{{trans('site.Edit map settings')}}</h1>
                    <hr>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="control-label" for="lang">{{trans('site.lang')}}</label>
                            <input type="number" step="0.0000001" id="lang" name="lang" class="form-control validate-input"
                                   value="{{$settings->lang}}">
                            <div class="alert alert-danger"></div>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="control-label" for="long">{{trans('site.long')}}</label>
                            <input type="number" step="0.0000001" id="long" name="long" class="form-control validate-input"
                                   value="{{$settings->long}}">
                            <div class="alert alert-danger"></div>
                        </div>
                        <div class="form-group col-md-3">
                            <label class="control-label" for="bound_east">{{trans('site.east')}}</label>
                            <input type="number" step="0.0000001" id="bound_east" name="bound_east" class="form-control validate-input"
                                   value="{{$settings->bound_east}}">
                            <div class="alert alert-danger"></div>
                        </div>
                        <div class="form-group col-md-3">
                            <label class="control-label" for="bound_west">{{trans('site.west')}}</label>
                            <input type="number" step="0.0000001" id="bound_west" name="bound_west" class="form-control validate-input"
                                   value="{{$settings->bound_west}}">
                            <div class="alert alert-danger"></div>
                        </div>
                        <div class="form-group col-md-3">
                            <label class="control-label" for="bound_north">{{trans('site.north')}}</label>
                            <input type="number" step="0.0000001" id="bound_north" name="bound_north" class="form-control validate-input"
                                   value="{{$settings->bound_north}}">
                            <div class="alert alert-danger"></div>
                        </div>
                        <div class="form-group col-md-3">
                            <label class="control-label" for="bound_south">{{trans('site.south')}}</label>
                            <input type="number" step="0.0000001" id="bound_south" name="bound_south" class="form-control validate-input"
                                   value="{{$settings->bound_south}}">
                            <div class="alert alert-danger"></div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group col-xs-12">
                            <input class="btn btn-success" type="submit" value="{{trans('site.save')}}">
                        </div>
                    </div>

                </div>
            </form>
        </div>
    </div>
@endsection
@section('footer_scripts')
    @if(env("APP_ENV") == 'local')
        <script src="http://localhost:3000/socket.io/socket.io.js"></script>
    @else
        <script src="http://erm.procreo.eu:3000/socket.io/socket.io.js"></script>
    @endif
    <script>
        var forklift;

        if ('{{ env('APP_ENV') }}' != 'local') {
            forklift = 'http://erm.procreo.eu/images/forklift.png';
        }else{
            forklift = 'http://erm.dev/images/forklift.png';
        }
        var googleMapApp = {
            markers: [],
            bounds: "",
            map: "",
            historicalOverlay: "",
            setAllMap: function (map) {
                for (var i = 0; i < this.markers.length; i++) {
                    this.markers[i].setMap(map);
                }
            },
            clearMarkers: function () {
                this.setAllMap(null);
            },
            centerMap: function (map, latitude, longtitude) {

                var center = new google.maps.LatLng(latitude, longtitude);
                map.setCenter(center);
            },
            setMarkers: function (map, locations, center_lat, center_lng) {
                var that = this;

                var image = {
                    url: forklift,
                    size: new google.maps.Size(35, 35),// 20 pixels wide by 32 pixels tall.
                    origin: new google.maps.Point(0, 0),// The origin for this image is 0,0.
                    anchor: new google.maps.Point(0, 35)// The anchor -  base of the flagpole at 0,32.
                }

                for (var i = 0; i < locations.length; i++) {
                    var myLatLng = {lat: parseFloat(locations[i]['lang']), lng: parseFloat(locations[i]['long'])};

                    var marker = new google.maps.Marker({
                        position: myLatLng,
                        map: map,
                        icon: image
                    });

                    that.markers.push(marker);
                }

                this.centerMap(map, center_lat, center_lng);
            },
            reload_map: function (points, lat, lng) {
                var that = this;

                that.clearMarkers();
                points = JSON.parse(points);

                if (points.length > 0) {
                    that.setMarkers(that.map, points, lat, lng);
                } else {
                    that.centerMap(that.map, lat, lng);
                }
            },
            invoke: function () {
                var that = this;

                this.map = null;
                var mapDefaults = {
                    zoom: 19,
                    scrollwheel: true,
                    navigationControl: false,
                    mapTypeControl: false,
                    scaleControl: true,
                    draggable: true,
                    zoomControl: true,
                    fullscreenControl: false,
                    center: {lat: {{ $settings->lang }}, lng: {{ $settings->long }}},
                };

                $(function () {
                    that.map = new google.maps.Map(document.getElementById("map"), mapDefaults);

                    var imageBounds = {
                        north: {{ $settings->bound_north }},
                        south: {{ $settings->bound_south }},
                        west: {{ $settings->bound_west }},
                        east: {{ $settings->bound_east }}
                    };

                    var image;

                    if ('{{ env('APP_ENV') }}' != 'local') {
                        image = 'http://erm.procreo.eu/images/plan.png';
                    }else{
                        image = 'http://erm.dev/images/plan.png';
                    }

                    that.historicalOverlay = new google.maps.GroundOverlay(
                            image,
                            imageBounds
                    );
                    that.historicalOverlay.setMap(that.map);

                    var points = <?php echo $loaders ?>;

                    if (points.length > 0) {
                        that.setMarkers(that.map, points, {{ $settings->lang }}, {{ $settings->long }});
                    }
                });
            }
        }

        var socket = null;

        if ('{{ env('APP_ENV') }}' != 'local') {
            socket = io('http://erm.procreo.eu:3000');
        }else{
            socket = io('http://localhost:3000');
        }

        function initMap() {
            googleMapApp.invoke();
        }

        socket.on("get-map:App\\Events\\Map", function (message) {
            googleMapApp.reload_map(message.data.loaders, {{ $settings->lang }}, {{ $settings->long }});
        });

    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCfVBWntLZWX0gH5-DBWDRjJtQuu1rsnRs&callback=initMap">
    </script>
@endsection