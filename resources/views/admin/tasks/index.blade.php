@extends('layout')

@section('main_container')
    <div class="right_col" role="main">
        <h1>{{trans('site.tasks')}}</h1><hr>
        <div class="workspaces-body">
            @if(count($workspaces) >= 1)
                @foreach($workspaces->chunk(3) as $workspace1)
                    <div class="row">
                        @foreach($workspace1 as $workspace)
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <div class="x_panel tile overflow_hidden">
                                    <div class="x_title">
                                        <h2 class="block-title-fit" style="font-size: 16px">
                                            <span class="rand-color stroke" style="background-color: {{$workspace->color}}">
                                                {{$loop->iteration}}. {{$workspace->name}}
                                            </span>
                                        </h2>
                                        <a class="btn btn-info pull-right btn-sm create-task" data-toggle="tooltip" data-placement="left" title="{{trans('site.new task for workspace')}}" data-object="workspace_id" data-task="{{$workspace->id}}"><i class="fa fa-tag" aria-hidden="true"></i></a>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                        <table class="workspace-table scroll first-column-width" style="width:100%">
                                            <tbody>
                                            <?php $workersNumber = 1?>
                                            @foreach($workers as $worker)
                                                @foreach($worker->workspaces as $work)
                                                    @if($work->id == $workspace->id)
                                                        @if($workersNumber == 1)
                                                            <tr>
                                                                <th>{{trans('site.No.')}}</th>
                                                                <th>{{trans('site.Full name')}}</th>
                                                                <th>{{trans('site.Action')}}</th>
                                                            </tr>
                                                        @endif
                                                        <tr>
                                                            <td><?=$workersNumber++?>.</td>
                                                            <td>{{$worker->name}} {{$worker->surname}}</td>
                                                            <td>
                                                                <a class="btn btn-info btn-xs create-task" data-toggle="tooltip" data-placement="left" title="{{trans('site.new task for worker')}}" data-object="worker_id" data-task="{{$worker->id}}"><i class="fa fa-tag" aria-hidden="true"></i>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    @endif
                                                @endforeach
                                            @endforeach
                                            @if($workersNumber <= 1)
                                                <tr>
                                                    <td colspan="3" class="no-workers">{{trans('site.no workers in workspace')}}</td>
                                                </tr>
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endforeach
            @else
                <h4>{{trans('site.workspaces not found')}}</h4>
            @endif
            <h1>{{trans('site.loaders')}}</h1><hr>
            @if(count($loaders) >= 1)
                @foreach($loaders->chunk(3) as $loaderChunk)
                    <div class="row">
                        @foreach($loaderChunk as $loader)
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <div class="x_panel tile">
                                    <div class="x_title">
                                        <h2 style="font-size: 18px">
                                            <span class="rand-color stroke" style="background-color: {{$loader->color}}">
                                                {{$loop->iteration}}. {{$loader->name}}
                                            </span>
                                        </h2>
                                        <a class="btn btn-info pull-right btn-sm create-task" data-toggle="tooltip" data-placement="left" title="{{trans('site.new task for loader')}}" data-object="loader_id" data-task="{{$loader->id}}"><i class="fa fa-tag" aria-hidden="true"></i></a>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endforeach
            @else
                <h4>{{trans('site.loaders not found')}}</h4>
            @endif
            <hr><h3>{{trans('site.All tasks')}}</h3>
            <table id="example" class="display table-worker resp table table-striped table-bordered dataTable no-footer" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>{{trans('site.No.')}}</th>
                        <th>{{trans('site.Task for')}}</th>
                        <th>{{trans('site.Object')}}</th>
                        <th>{{trans('site.Task description')}}</th>
                        <th>{{trans('site.Date')}}</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($tasks as $task)
                    <tr>
                        <td>{{$loop->iteration}}.</td>
                        @if($task->workspace_id != 0)
                            <td>{{trans('site.for workspace')}}</td>
                            <td>{{$task->workspace->name}}</td>
                        @elseif($task->worker_id != 0)
                            <td>{{trans('site.for worker')}}</td>
                            <td>{{$task->worker->name}} {{$task->worker->surname}}</td>
                        @elseif($task->loader_id != 0)
                            <td>{{trans('site.for loader')}}</td>
                            <td>{{$task->loader->name}}</td>
                        @endif
                        <td>{{$task->text}}</td>
                        <td>{{$task->created_at->format('Y-m-d H:i')}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @include('admin.modals.createTaskModal')
@endsection