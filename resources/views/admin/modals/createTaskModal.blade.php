<div class="modal createTaskModal fade" id="createTaskModal" tabindex="-1" role="dialog" aria-labelledby="createTaskModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h2 class="modal-title text-center">{{trans('site.create new task')}}</h2>
            </div>
            <form action="{{route('tasks-create')}}" method="post" class="text-center validate-form">
                <div class="modal-body">
                    {!! csrf_field() !!}
                    <input type="hidden" class="task-object">
                    <div class="form-group">
                        <label class="control-label">{{trans('site.task description')}}</label>
                        <textarea name="text" rows="3" class="form-control validate-input"></textarea>
                        <div class="alert alert-danger"></div>
                    </div>
                </div>
                <div class="form-group">
                    <input type="checkbox" name="status" value="1"> <span style="font-weight: 700">{{trans('site.active')}}?</span>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('site.cancel')}}</button>
                    <input type="submit" class="btn btn-success" value="{{trans('site.create task')}}">
                </div>
            </form>
        </div>
    </div>
</div>