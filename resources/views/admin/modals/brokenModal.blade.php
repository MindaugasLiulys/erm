<div class="modal brokenModal fade" id="brokenModal" tabindex="-1" role="dialog" aria-labelledby="brokenModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h2 class="modal-title text-center"></h2>
            </div>
            <div class="modal-body">
                <p></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('site.Close')}}</button>
            </div>
        </div>
    </div>
</div>