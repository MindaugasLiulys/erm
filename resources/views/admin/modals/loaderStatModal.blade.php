<div class="modal loaderStatModal_{{$loader->id}} fade" id="loaderStatModal" tabindex="-1" role="dialog" aria-labelledby="loaderStatModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h2 class="modal-title text-center modalHeading">{{$loader->name}} {{trans('site.statistics')}}</h2>
            </div>
            <form action="" method="post" class="text-center">
                <div class="modal-body">
                    <div class="x_content">
                        <div class="col-xs-3">
                            <ul class="nav nav-tabs tabs-left">
                                <li class="active"><a href="#today1_{{$loader->id}}" data-toggle="tab">{{trans('site.Today')}}</a></li>
                                <li><a href="#yesterday1_{{$loader->id}}" data-toggle="tab">{{trans('site.Yesterday')}}</a></li>
                                <li><a href="#thisMonth1_{{$loader->id}}" data-toggle="tab">{{trans('site.This month')}}</a></li>
                                <li><a href="#lastMonth1_{{$loader->id}}" data-toggle="tab">{{trans('site.Last month')}}</a></li>
                            </ul>
                        </div>
                        <div class="col-xs-9">
                            <div class="tab-content">
                                <div class="tab-pane active" id="today1_{{$loader->id}}">
                                    <h2>{{trans('site.Today')}}:</h2>
                                    <p class="lead">
                                        {{trans('site.Products loaded')}}: {{workspaceStat($todaysLoaded, $loader->id, 1)}}m<sup>3</sup><br>
                                        {{trans('site.Materials unloaded')}}: {{workspaceStat($todaysUnloaded, $loader->id, 1)}}m<sup>3</sup>
                                    </p>
                                </div>
                                <div class="tab-pane" id="yesterday1_{{$loader->id}}">
                                    <h2>{{trans('site.Yesterday')}}:</h2>
                                    <p class="lead">
                                        {{trans('site.Products loaded')}}: {{workspaceStat($yesterdaysLoaded, $loader->id, 1)}}m<sup>3</sup><br>
                                        {{trans('site.Materials unloaded')}}: {{workspaceStat($yesterdaysUnloaded, $loader->id, 1)}}m<sup>3</sup>
                                    </p>
                                </div>
                                <div class="tab-pane" id="thisMonth1_{{$loader->id}}">
                                    <h2>{{trans('site.This month')}}:</h2>
                                    <p class="lead">
                                        {{trans('site.Products loaded')}}: {{workspaceStat($thisMonthLoaded, $loader->id, 1)}}m<sup>3</sup><br>
                                        {{trans('site.Materials unloaded')}}: {{workspaceStat($thisMonthUnloaded, $loader->id, 1)}}m<sup>3</sup>
                                    </p>
                                </div>
                                <div class="tab-pane" id="lastMonth1_{{$loader->id}}">
                                    <h2>{{trans('site.Last month')}}:</h2>
                                    <p class="lead">
                                        {{trans('site.Products loaded')}}: {{workspaceStat($lastMonthLoaded, $loader->id, 1)}}m<sup>3</sup><br>
                                        {{trans('site.Materials unloaded')}}: {{workspaceStat($lastMonthUnloaded, $loader->id, 1)}}m<sup>3</sup>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>