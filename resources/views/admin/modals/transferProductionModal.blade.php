<div class="modal transferProductionModal fade" id="transferProductionModal" tabindex="-1" role="dialog" aria-labelledby="transferProductionModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h2 class="modal-title text-center">{{trans('site.transfer')}}</h2>
            </div>
            <form method="post" class="text-center validate-form" action="{{route('warehouse-transfer')}}" id="transferProductionModalForm">
                <div class="modal-body">
                    {!! csrf_field() !!}
                    <input type="hidden" name="warehouse_from" class="warehouse_from">
                    <div class="form-group">
                        <label for="warehouse_id">{{trans('site.warehouse to transfer')}}</label>
                        <select name="warehouse_id" id="warehouse_id" class="form-control">
                            @foreach($warehouses as $warehouse)
                                <option value="{{$warehouse->id}}">{{$warehouse->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="order_id">{{trans('site.product to transfer')}}</label>
                        <select name="order_id" id="order_id" class="form-control">
                            @foreach($orders as $order)
                                <option value="{{$order->order->id}}" data-warehouse="{{$order->warehouse_id}}" style="display: none">{{trans('site.order_number') . ': ' . $order->order->order_number}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="loader_id">{{trans('site.Select loader')}}</label>
                        <select name="loader_id" id="loader_id" class="form-control">
                            @foreach($loaders as $loader)
                                <option value="{{$loader->loader_id}}">{{$loader->loader->name}} - {{getActionByStatus($loader->actions_status)['message']}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="quantity">{{trans('site.Product quantity')}}</label>
                        <input class="form-control validate-input" type="text" name="quantity" id="quantity">
                        <div class="alert alert-danger"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('site.cancel')}}</button>
                    <input type="submit" class="btn btn-success" value="{{trans('site.transfer')}}">
                </div>
            </form>
        </div>
    </div>
</div>

