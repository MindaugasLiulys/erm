<div class="modal createOrderModal fade" id="createOrderModal" tabindex="-1" role="dialog" aria-labelledby="createOrderModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h2 class="modal-title text-center">{{trans('site.create new order')}}</h2>
            </div>
            <form method="post" class="text-center validate-form" action="{{route('order-create')}}">
                <div class="modal-body">
                    {!! csrf_field() !!}
                    <div class="form-group">
                        <label for="order_number">{{trans('site.Order ID')}}</label>
                        <input class="form-control validate-input" type="text" name="order_number" id="order_number">
                        <div class="alert alert-danger"></div>
                    </div>
                    <div class="form-group">
                        <label for="workspace_id">{{trans('site.Select workspace to assign order to')}}</label>
                        <select name="workspace_id" id="workspace_id" class="form-control">
                            @foreach($workspaces as $workspace)
                                <option value="{{$workspace->id}}">{{$workspace->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="product_id">{{trans('site.Select product that needs to be produced')}}</label>
                        <select name="product_id" id="product_id" class="form-control">
                            @foreach($products as $product)
                                <option value="{{$product->id}}">{{$product->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="dimensions">{{trans('site.Product dimensions')}}</label>
                        <input class="form-control validate-input" type="text" name="dimensions" id="dimensions">
                        <div class="alert alert-danger"></div>
                    </div>
                    <div class="form-group">
                        <label for="target_quantity">{{trans('site.Target quantity')}}</label>
                        <input class="form-control validate-input" type="text" name="target_quantity" id="target_quantity">
                        <div class="alert alert-danger"></div>
                    </div>
                    <div class="form-group">
                        <label for="heat_no">{{trans('site.Heat No.')}}</label>
                        <input class="form-control validate-input" type="text" name="heat_no" id="heat_no">
                        <div class="alert alert-danger"></div>
                    </div>
                    <div class="form-group">
                        <label for="deadline">{{trans('site.Deadline')}}</label>
                        <div class="input-group deadline">
                            <input type="text" class="form-control deadline" name="deadline" id="deadline">
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                        <div class="alert alert-danger"></div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="order_notes">{{trans('site.Order notes')}}</label>
                        <textarea name="order_notes" rows="3" class="form-control" id="order_notes"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="status_id">{{trans('site.Select order status')}}</label>
                        <select name="status" id="status_id" class="form-control">
                            <option value="0">{{trans('site.Not active')}}</option>
                            <option value="1">{{trans('site.Active')}}</option>
                            <option value="2">{{trans('site.Paused')}}</option>
                            <option value="3">{{trans('site.Finished')}}</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('site.cancel')}}</button>
                    <input type="submit" class="btn btn-success" value="{{trans('site.create order')}}">
                </div>
            </form>
        </div>
    </div>
</div>