<div class="modal warehouseModal_{{$warehouse->id}} fade" id="warehouseModal" tabindex="-1" role="dialog" aria-labelledby="warehouseModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h2 class="modal-title text-center modalHeading">{{$warehouse->name}} {{trans('site.statistics')}}</h2>
            </div>
            <form action="" method="post" class="text-center">
                <div class="modal-body">
                    <div class="x_content">
                        <div class="col-xs-3">
                            <ul class="nav nav-tabs tabs-left">
                                <li class="active"><a href="#today_{{$warehouse->id}}" data-toggle="tab">{{trans('site.Today')}}</a></li>
                                <li><a href="#yesterday_{{$warehouse->id}}" data-toggle="tab">{{trans('site.Yesterday')}}</a></li>
                                <li><a href="#thisMonth_{{$warehouse->id}}" data-toggle="tab">{{trans('site.This month')}}</a></li>
                                <li><a href="#lastMonth_{{$warehouse->id}}" data-toggle="tab">{{trans('site.Last month')}}</a></li>
                            </ul>
                        </div>
                        <div class="col-xs-9">
                            <div class="tab-content">
                                <div class="tab-pane active" id="today_{{$warehouse->id}}">
                                    <h2>{{trans('site.Today')}}:</h2>
                                    <p class="lead">
                                        <?php $lodedValue = 0; ?>
                                        @foreach($todays as $today)
                                            @if($today->warehouse_id == $warehouse->id)
                                                <?php $lodedValue += $today->volume ?>
                                            @endif
                                        @endforeach
                                        {{trans('site.Unloaded volume')}}: {{$lodedValue}}m<sup>3</sup>
                                    </p>
                                </div>
                                <div class="tab-pane" id="yesterday_{{$warehouse->id}}">
                                    <h2>{{trans('site.Yesterday')}}:</h2>
                                    <p class="lead">
                                        <?php $lodedValue = 0; ?>
                                        @foreach($yesterdays as $yesterday)
                                            @if($yesterday->warehouse_id == $warehouse->id)
                                                <?php $lodedValue += $yesterday->volume ?>
                                            @endif
                                        @endforeach
                                        {{trans('site.Unloaded volume')}}: {{$lodedValue}}m<sup>3</sup>
                                    </p>
                                </div>
                                <div class="tab-pane" id="thisMonth_{{$warehouse->id}}">
                                    <h2>{{trans('site.This month')}}:</h2>
                                    <p class="lead">
                                        <?php $lodedValue = 0; ?>
                                        @foreach($thisMonth as $thisMonthData)
                                            @if($thisMonthData->warehouse_id == $warehouse->id)
                                                <?php $lodedValue += $thisMonthData->volume ?>
                                            @endif
                                        @endforeach
                                        {{trans('site.Unloaded volume')}}: {{$lodedValue}}m<sup>3</sup>
                                    </p>
                                </div>
                                <div class="tab-pane" id="lastMonth_{{$warehouse->id}}">
                                    <h2>{{trans('site.Last month')}}:</h2>
                                    <p class="lead">
                                        <?php $lodedValue = 0; ?>
                                        @foreach($lastMonth as $lastMonthData)
                                            @if($lastMonthData->warehouse_id == $warehouse->id)
                                                <?php $lodedValue += $lastMonthData->volume ?>
                                            @endif
                                        @endforeach
                                        {{trans('site.Unloaded volume')}}: {{$lodedValue}}m<sup>3</sup>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>