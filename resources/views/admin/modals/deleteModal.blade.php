<div class="modal deleteModal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h2 class="modal-title text-center">{{trans('site.are_you_sure')}}</h2>
            </div>
            <div class="modal-footer">
                <form action="" method="post" class="text-center">
                    {!! csrf_field() !!}
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('site.cancel')}}</button>
                    <input type="submit" class="btn btn-danger" value="{{trans('site.delete')}}">
                </form>
            </div>
        </div>
    </div>
</div>