<div class="modal loadModal fade" id="loadModal" tabindex="-1" role="dialog" aria-labelledby="loadModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h2 class="modal-title text-center loadModalHeading">{{trans('site.Load products')}}</h2>
            </div>
            <form action="" method="post" id="loadSubmit" class="text-center">
                {!! csrf_field() !!}
                <div class="modal-body">
                    <div class="x_content">
                        <div class="col-md-12 col-sm-12 col-xs-12 worker-stats">
                            <input type="hidden" id="loaderId" name="loaderId">
                            <div class="form-group">
                                <label for="order_id">{{trans('site.select order to load')}}</label>
                                <select name="order_id" id="select-order" class="form-control">
                                    <option selected disabled>Choose here</option>
                                    @foreach($orders as $order)
                                        <option value="{{$order->id}}">{{trans('site.order_number') . ': ' . $order->order_number}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="ramp">{{trans('site.select ramp to load your product to')}}</label>
                                <select name="ramp" id="ramp" class="form-control">
                                    @foreach($ramps as $ramp)
                                        <option value="{{$ramp->id}}">{{$ramp->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group hidden">
                                <label for="warehouse">{{trans('site.select warehouse to load product from')}}</label>
                                <select name="warehouse" id="select-warehouse" class="form-control">
                                    {{--@foreach($warehouses as $warehouse)--}}
                                        {{--<option value="{{$warehouse->id}}">{{$warehouse->name}}</option>--}}
                                    {{--@endforeach--}}
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="quantity">{{trans('site.quantity to load')}}</label>
                                <input class="form-control" type="number" name="quantity" id="quantity" min="1" required>
                            </div>
                            <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('site.cancel')}}</button>
                            <input type="submit" class="btn btn-success disabled" value="{{trans('site.Send')}}">
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>