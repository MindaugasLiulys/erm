<div class="modal unloadModal fade" id="unloadModal" tabindex="-1" role="dialog" aria-labelledby="unloadModalModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h2 class="modal-title text-center unloadModalHeading">{{trans('site.Unload materials')}}</h2>
            </div>
            <form action="" method="post" id="unloadSubmit" class="text-center">
                {!! csrf_field() !!}
                <div class="modal-body">
                    <div class="x_content">
                        <div class="col-md-12 col-sm-12 col-xs-12 worker-stats">
                            <input type="hidden" id="loaderId" name="loaderId">
                            <div class="form-group">
                                <label for="material">{{trans('site.select material to unload')}}</label>
                                <select name="material" id="material" class="form-control">
                                    @foreach($materials as $material)
                                        <option value="{{$material->id}}">{{$material->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="ramp">{{trans('site.select ramp to unload from')}}</label>
                                <select name="ramp" id="ramp" class="form-control">
                                    @foreach($ramps as $ramp)
                                        <option value="{{$ramp->id}}">{{$ramp->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="quantity">{{trans('site.quantity to unload')}}</label>
                                <input class="form-control" type="text" name="quantity" id="quantity">
                            </div>
                            <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('site.cancel')}}</button>
                            <input type="submit" class="btn btn-success" value="{{trans('site.Send')}}">
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>