<div class="modal additionalModal fade" id="additionalModal" tabindex="-1" role="dialog" aria-labelledby="additionalModalModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h2 class="modal-title text-center additionalModalHeading">{{trans('site.Additional work')}}</h2>
            </div>
            <form action="{{route('call_additional')}}" method="post" id="additionalSubmit" class="text-center">
                <div class="modal-body">
                    {!! csrf_field() !!}
                    <input type="hidden" name="loader_id" id="loaderId">
                    <div class="form-group">
                        <label class="control-label">{{trans('site.work description')}}</label>
                        <textarea name="text" rows="3" class="form-control validate-input"></textarea>
                        <div class="alert alert-danger"></div>
                    </div>
                </div>
                <div class="form-group">
                    <label><span class="checkbox-text">{{trans('site.active')}}?</span>
                        <input type="checkbox" name="status" value="1">
                    </label>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('site.cancel')}}</button>
                    <input type="submit" class="btn btn-success" value="{{trans('site.create')}}">
                </div>
            </form>
        </div>
    </div>
</div>