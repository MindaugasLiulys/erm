<div class="modal workerStatModal fade" id="workerStatModal" tabindex="-1" role="dialog" aria-labelledby="workerStatModalLabel">
    <div class="modal-dialog" role="document" style="width: 57%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h2 class="modal-title text-center workerModalHeading"></h2>
            </div>
            <form action="" method="post" class="text-center">
                <div class="modal-body">
                    <div class="x_content">
                        <div class="col-md-12 col-sm-12 col-xs-12 worker-stats">
                            <table class="table resp table-workerModalStats" id="workspace-production">
                                <thead>
                                    <tr>
                                        <th>{{trans('site.No.')}}</th>
                                        <th>{{trans('site.Full name')}}</th>
                                        <th>{{trans('site.Day start')}}</th>
                                        <th>{{trans('site.Day end')}}</th>
                                        <th>{{trans('site.Exported')}}</th>
                                        <th>{{trans('site.Produced')}}</th>
                                        <th>{{trans('site.Date')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($productions as $production)
                                    <tr class="workspace-production" data-workerid="{{ $production->worker_id }}">
                                        <td class="serial"></td>
                                        <td>{{$production->worker->name}} {{$production->worker->surname}}</td>
                                        <td>{{$production->day_start}}</td>
                                        <td>{{$production->day_end}}</td>
                                        <td>{{$production->unloaded}}</td>
                                        <td>{{$production->unloaded + $production->day_end - $production->day_start}}</td>
                                        <td>{{$production->updated_at->format('Y-m-d')}}</td>
                                    </tr>
                                @endforeach
                                <tr class="dont-show">
                                    <td colspan="7">
                                        <a class="btn btn-info btn-block worker-stat-id">{{trans('site.Show extended statistics')}}</a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>