<div class="modal addWorkerModal fade" id="addWorkerModal" tabindex="-1" role="dialog" aria-labelledby="addWorkerModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h2 class="modal-title text-center">{{trans('site.add worker')}}</h2>
            </div>
            <form action="" method="post" class="text-center">
                <div class="modal-body">
                    {!! csrf_field() !!}
                    <div class="form-group"></div>
                    <label class="control-label">{{trans('site.select worker')}}</label>
                    <select name="workspace_id" class="select-worker form-control">
                        @if(count($availWorkers) >= 1)
                            @foreach($availWorkers as $availWorker)
                                <option value="{{$availWorker->id}}">{{$availWorker->name}} {{$availWorker->surname}}</option>
                            @endforeach
                        @else
                            <option>{{trans('site.no free workers')}}</option>
                        @endif
                    </select>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('site.cancel')}}</button>
                    <input type="submit" class="btn btn-success" value="{{trans('site.add worker')}}">
                </div>
            </form>
        </div>
    </div>
</div>