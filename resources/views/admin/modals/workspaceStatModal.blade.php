<div class="modal workspaceStatModal_{{$workspace->id}} fade" id="workspaceStatModal" tabindex="-1" role="dialog" aria-labelledby="workspaceStatModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h2 class="modal-title text-center modalHeading">{{$workspace->name}} {{trans('site.statistics')}}</h2>
            </div>
            <form action="" method="post" class="text-center">
                <div class="modal-body">
                    <div class="x_content">
                        <div class="col-xs-3">
                            <ul class="nav nav-tabs tabs-left">
                                <li class="active"><a href="#today1_{{$workspace->id}}" data-toggle="tab">{{trans('site.Today')}}</a></li>
                                <li><a href="#yesterday1_{{$workspace->id}}" data-toggle="tab">{{trans('site.Yesterday')}}</a></li>
                                <li><a href="#thisMonth1_{{$workspace->id}}" data-toggle="tab">{{trans('site.This month')}}</a></li>
                                <li><a href="#lastMonth1_{{$workspace->id}}" data-toggle="tab">{{trans('site.Last month')}}</a></li>
                            </ul>
                        </div>
                        <div class="col-xs-9">
                            <div class="tab-content">
                                <div class="tab-pane active" id="today1_{{$workspace->id}}">
                                    <h2>{{trans('site.Today')}}:</h2>
                                    <p class="lead">
                                        {{trans('site.Produced')}}: {{workspaceStat($todays, $workspace->id, 0)}}m<sup>3</sup>
                                    </p>
                                </div>
                                <div class="tab-pane" id="yesterday1_{{$workspace->id}}">
                                    <h2>{{trans('site.Yesterday')}}:</h2>
                                    <p class="lead">
                                        {{trans('site.Produced')}}: {{workspaceStat($yesterdays, $workspace->id, 0)}}m<sup>3</sup>
                                    </p>
                                </div>
                                <div class="tab-pane" id="thisMonth1_{{$workspace->id}}">
                                    <h2>{{trans('site.This month')}}:</h2>
                                    <p class="lead">
                                        {{trans('site.Produced')}}: {{workspaceStat($thisMonth, $workspace->id, 0)}}m<sup>3</sup>
                                    </p>
                                </div>
                                <div class="tab-pane" id="lastMonth1_{{$workspace->id}}">
                                    <h2>{{trans('site.Last month')}}:</h2>
                                    <p class="lead">
                                        {{trans('site.Produced')}}: {{workspaceStat($lastMonth, $workspace->id, 0)}}m<sup>3</sup>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>