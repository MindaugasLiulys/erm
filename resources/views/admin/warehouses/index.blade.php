@extends('layout')

@section('main_container')
    <div class="right_col" role="main">
        <h1>{{trans('site.warehouses')}}</h1>
        <a class="btn btn-primary" href="{{route('create-warehouse')}}"><i class="fa fa-plus"
                                                                           aria-hidden="true"></i> {{trans('site.create warehouse')}}
        </a>
        <hr>
        <div class="workspaces-body">
            @if(count($warehouses) >= 1)
                @foreach($warehouses->chunk(3) as $warehouseChunk)
                    <div class="row">
                        @foreach($warehouseChunk as $warehouse)
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <div class="x_panel tile overflow_hidden">
                                    <div class="x_title">
                                        <h2 style="font-size: 18px">
                                            <span class="rand-color stroke"
                                                  style="background-color: {{$warehouse->color}}">
                                                {{$loop->iteration}}. {{$warehouse->name}}
                                            </span>
                                        </h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li>
                                                <a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                            </li>
                                            <li class="dropdown">
                                                <a href="javascript:;" class="workplace_drop dropdown-toggle"
                                                   data-toggle="dropdown" role="button" data-id="{{$warehouse->id}}"
                                                   aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                                <ul class="dropdown-menu dropdown_workplace_{{$warehouse->id}}"
                                                    role="menu">
                                                    <li>
                                                        <a class="warehouseModalCall"
                                                           data-warehouse="{{$warehouse->id}}"
                                                           data-name="{{$warehouse->name}}"><i class="fa fa-area-chart"
                                                                                               aria-hidden="true"></i> {{trans('site.Statistics')}}
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="{{url('admin/warehouses/edit/' . $warehouse->id)}}"><i
                                                                    class="fa fa-edit" aria-hidden="true"></i> Edit</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:;"
                                                           data-action="{{url('admin/warehouses/delete/' . $warehouse->id)}}"
                                                           data-method="delete"><i class="fa fa-trash"
                                                                                   aria-hidden="true"></i> {{trans('site.Delete')}}
                                                        </a>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                        <table class="workspace-table" style="width:100%">
                                            <tbody>
                                            <tr>
                                                <th>{{trans('site.Volume')}}</th>
                                                <th>{{trans('site.Value')}}</th>
                                            </tr>
                                            <tr>
                                                <td>{{trans('site.Total')}}</td>
                                                <td>{{$warehouse->space}}m<sup>3</sup></td>
                                            </tr>
                                            <tr>
                                                <td>{{trans('site.Used')}}</td>
                                                <td>{{$warehouse->used}}m<sup>3</sup></td>
                                            </tr>
                                            <tr>
                                                <td>{{trans('site.Reserved')}}</td>
                                                <td>{{$warehouse->reserved}}m<sup>3</sup></td>
                                            </tr>
                                            <tr>
                                                <td>{{trans('site.Free')}}</td>
                                                <td>{{$warehouse->space - $warehouse->used - $warehouse->reserved}}
                                                    m<sup>3</sup></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2"
                                                    class="space-in-text">{{trans('site.Space in warehouse')}}</td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" style="text-align: center">
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-info progress-bar-striped active"
                                                             role="progressbar"
                                                             aria-valuenow="{{getPercent($warehouse->space, $warehouse->used)}}"
                                                             aria-valuemin="0" aria-valuemax="100"
                                                             style="width:{{getPercent($warehouse->space, $warehouse->used)}}%">
                                                            @if($warehouse->used != 0 && getPercent($warehouse->space, $warehouse->used) > 7)
                                                                <span>&nbsp;{{getPercent($warehouse->space, $warehouse->used)}}</span>
                                                                % {{trans('site.Occupied')}}
                                                            @endif
                                                        </div>
                                                        <div class="progress-bar progress-bar-warning progress-bar-striped active"
                                                             role="progressbar"
                                                             aria-valuenow="{{getPercent($warehouse->space, $warehouse->reserved)}}"
                                                             aria-valuemin="0" aria-valuemax="100"
                                                             style="width:{{getPercent($warehouse->space, $warehouse->reserved)}}%">
                                                            @if($warehouse->reserved != 0 && getPercent($warehouse->space, $warehouse->reserved) > 7)
                                                                <span>&nbsp;{{getPercent($warehouse->space, $warehouse->reserved)}}</span>
                                                                % {{trans('site.Reserved')}}
                                                            @endif
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <div style="text-align: center">
                                            {{trans('site.Orders')}}
                                        </div>
                                        <div class="row orders-list">
                                            <div class="col-md-6"><b>{{ trans('site.order_number') }}</b></div>
                                            <div class="col-md-6 text-right"><b>{{ trans('site.quantity') }}</b></div>
                                            @foreach($warehouse->orders as $order)
                                                <div class="col-md-6">{{ $order->order->order_number }}</div>
                                                <div class="col-md-6 text-right">{{ $order->quantity }}</div>
                                            @endforeach
                                        </div>
                                        <div style="text-align: center">
                                            <a class="btn btn-info transfer-modal"
                                               data-warehouse="{{$warehouse->id}}">{{trans('site.Transfer orders')}}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @include('admin.modals.warehouseModal')
                        @endforeach
                    </div>
                @endforeach
            @else
                <h4>{{trans('site.warehouses not found')}}</h4>
            @endif
            <hr>
            <h1>{{trans('site.ramps')}}</h1>
            <a class="btn btn-primary" href="{{route('create-ramp')}}"><i class="fa fa-plus"
                                                                          aria-hidden="true"></i> {{trans('site.create ramp')}}
            </a>
            <hr>
            <table id="example" class="display resp table table-striped table-bordered dataTable no-footer" width="100%"
                   cellspacing="0">
                <thead>
                <tr>
                    <th>{{trans('site.No.')}}</th>
                    <th>{{trans('site.ramp')}}</th>
                    <th>{{trans('site.active')}}</th>
                    <th>{{trans('site.action')}}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($ramps as $ramp)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$ramp->name}}</td>
                        <td>@if($ramp->status != 0)
                                <span class="green">{{trans('site.YES')}}</span>
                            @else
                                <span class="red">{{trans('site.NO')}}</span>
                            @endif
                        </td>
                        <td>
                            <a href="{{route('edit-ramp', $ramp->id)}}" class="btn btn-primary"><i
                                        class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                            <a class="btn btn-danger" data-action="{{route('delete-ramp', $ramp->id)}}"
                               data-method="delete"><i class="fa fa-trash" aria-hidden="true"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                    <th>{{trans('site.No.')}}</th>
                    <th>{{trans('site.ramp')}}</th>
                    <th>{{trans('site.status')}}</th>
                    <th>{{trans('site.action')}}</th>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
    @include('admin.modals.transferProductionModal')
    @include('admin.modals.deleteModal')
@endsection
@section('footer_scripts')
    <script>
        $('#transferProductionModalForm').submit(function (e) {
            var url = "{{route('warehouse-transfer')}}";
            $.ajax({
                type: "POST",
                url: url,
                data: $("#transferProductionModalForm").serialize(),
                headers: {
                    'X-CSRF-TOKEN': $("#transferProductionModalForm").find('meta[name="csrf-token"]').attr('content')
                },
                success: function () {
                    location.reload();
                },
                error: function (data) {
                    errorsOutput('#transferProductionModalForm', data);
                }
            });
            e.preventDefault();
        });

        function errorsOutput(errorPlace, data) {
            var errors = data.responseJSON.errors;

            $.each(errors, function (key, value) {
                $(errorPlace + ' [name=' + key + ']').closest('div')
                        .addClass('has-error')
                        .append('<div class="text-danger">' + value + '</div>');
            })
        }
        ;

    </script>
@endsection