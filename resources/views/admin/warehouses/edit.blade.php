@extends('layout')

@section('main_container')

    <div class="right_col" role="main">
        <h1>{{trans('site.edit warehouse')}}</h1><hr>
        <form action="{{url('admin/warehouses/edit/' . $warehouse->id)}}" class="create-new-form validate-form" method="POST">
            {!! csrf_field() !!}
            <div class="form-group">
                <label class="control-label">{{trans('site.name')}}</label>
                <input name="name" class="form-control validate-input" value="{{$warehouse->name}}">
                <div class="alert alert-danger"></div>
            </div>
            <div class="form-group">
                <label class="control-label">{{trans('site.volume')}} (m<sup>3</sup>)</label>
                <input name="space" class="form-control validate-input" value="{{$warehouse->space}}">
                <div class="alert alert-danger"></div>
            </div>
            <div class="form-group">
                <label class="control-label">{{trans('site.already used')}} (m<sup>3</sup>)</label>
                <input name="used" class="form-control validate-input" value="{{$warehouse->used}}">
                <div class="alert alert-danger"></div>
            </div>
            <div class="form-group">
                <label class="control-label">{{trans('site.Reserved')}} (m<sup>3</sup>)</label>
                <input name="reserved" class="form-control validate-input" value="{{$warehouse->reserved}}">
                <div class="alert alert-danger"></div>
            </div>
            <div class="form-group">
                <label class="control-label">{{trans('site.distinctive color')}}</label>
                <input type="color" name="favcolor" class="form-control" value="{{$warehouse->color}}">
                <div class="alert alert-danger"></div>
            </div>
            <div class="form-group">
                <a href="{{route('warehouses')}}" class="btn btn-default">{{trans('site.cancel')}}</a>
                <input class="btn btn-success" type="submit" value="{{trans('site.save')}}">
            </div>
        </form>
    </div>
@endsection