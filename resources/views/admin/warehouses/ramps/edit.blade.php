@extends('layout')

@section('main_container')

    <div class="right_col" role="main">
        <h1>{{trans('site.edit ramp')}}</h1><hr>
        <form action="{{route('edit-ramp-post', $ramp->id)}}" class="create-new-form validate-form" method="POST">
            {!! csrf_field() !!}
            <div class="form-group">
                <label class="control-label" for="name">{{trans('site.ramp')}}</label>
                <input type="text" id="name" name="name" class="form-control validate-input" value="{{$ramp->name}}">
                <div class="alert alert-danger"></div>
            </div>
            <div class="form-group">
                <input type="checkbox" name="status" value="1" {{($ramp->status == 1)?"checked=checked" : '' }}> <span style="font-weight: 700">{{trans('site.active')}}?</span>
            </div>
            <div class="form-group">
                <a href="{{route('warehouses')}}" class="btn btn-default">{{trans('site.cancel')}}</a>
                <input class="btn btn-success" type="submit" value="{{trans('site.save')}}">
            </div>
        </form>
    </div>
@endsection