@extends('layout')

@section('main_container')

    <div class="right_col" role="main">
        <h1>{{trans('site.new ramp')}}</h1><hr>
        <form action="{{route('create-ramp-post')}}" class="create-new-form validate-form" method="POST">
            {!! csrf_field() !!}
            <div class="form-group">
                <label class="control-label">{{trans('site.name')}}</label>
                <input name="name" class="form-control validate-input">
                <div class="alert alert-danger"></div>
            </div>
            <div class="form-group">
                <input type="checkbox" name="status" value="1"> <span style="font-weight: 700">{{trans('site.active')}}?</span>
            </div>
            <div class="form-group">
                <a href="{{route('warehouses')}}" class="btn btn-default">{{trans('site.cancel')}}</a>
                <input class="btn btn-success" type="submit" value="{{trans('site.create')}}">
            </div>
        </form>
    </div>
@endsection