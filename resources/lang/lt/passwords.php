<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Slaptažodis turi būti bent 6 skaitmenų ir atitikti sistemos duomenis.',
    'reset' => 'Jūsų slaptažodis atstatytas!',
    'sent' => 'Slaptažodžio atstatymo nuoroda buvo nusiųsta į jūsų el.paštą!',
    'token' => 'Slaptažodžio atsatymo tokenas neteisingas.',
    'worker' => "Darbuotojas su tokiu el.paštu nerastas.",

];
