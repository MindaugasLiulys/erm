<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the worker. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Neteisingi duomenys',
    'throttle' => 'Viršytas galimų prisijungimų skaičius. Prašome mėginti dar kartą po :seconds sekundžių.',

];
